/** *
 * \file	Command.cpp
 * \date	09.5.2016
 * \author	brr
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 */


#include "Command.h"

Command::Command(
    int cmdId,
    void* pParam /*= nullptr*/,
    Sync* pSync /*= nullptr*/,
    Result* pRes /*= nullptr*/)
    : m_CmdId(cmdId), m_pParam(pParam), m_pSync(pSync), m_pRes(pRes) {
}

Command::~Command() {
}

int Command::getCmdId() {
    return m_CmdId;
}


void* Command::getParam() {
    return m_pParam;
}

Sync* Command::getSync() {
    return m_pSync;
}

Result* Command::getResult() {
    return m_pRes;
}
