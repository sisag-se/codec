/** *
 * \file	SisThread.cpp
 * \date	09.5.2016
 * \author	brr
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 */

#include "SisThread.h"

#include <pthread.h>
#include <sched.h>
#include <stddef.h>

#include <queue>

#include "Sync.h"

SisThread::SisThread(const EThreadName threadName /*= "NoName"*/)
    : m_threadName(threadName),
      m_thread(0),
      m_mutex(),
      m_condVar(),
      m_cmdQueue() {
    // initialize mutex and condition variable
    pthread_mutex_init(&m_mutex, NULL);
    pthread_cond_init(&m_condVar, NULL);
}

SisThread::~SisThread() {
    // we should resume the thread and exit ThreadFunc before calling of
    // blocking pthread_join function also it prevents the mutex staying locked
    stop();

    pthread_cond_destroy(&m_condVar);
    pthread_mutex_destroy(&m_mutex);
}

int SisThread::start(int policy /*= SCHED_OTHER*/, int prio /*= 0*/) {
    int ret = -1;
    // create thread in suspended state
    ret = pthread_create(&m_thread, NULL, ThreadFunc, this);
    if(ret != 0) {
        return -1;
    }
    // set the schedule policy and the priority of the thread
    ret = setScheduleAndPrio(m_thread, policy, prio);
    if(ret != 0) {
        return -1;
    }
    int policyThread = 0;
    int prioThread   = 0;
    ret              = getScheduleAndPrio(m_thread, policyThread, prioThread);
    // compare the parameters
    if(ret != 0 || policyThread != policy || prioThread != prio) {
        return -1;
    }

    return 0;
}

int SisThread::stop() {
    Command* pCmd = new Command(0); // command id 0 = stop
    post(pCmd);
    pthread_join(m_thread, NULL); // wait until the ThreadFunc is terminated
    return 0;
}

pthread_t SisThread::getThread() {
    return m_thread;
}

void* SisThread::ThreadFunc(void* pParam) {
    int ret          = 0;
    SisThread* pThis = (SisThread*) pParam;
    if(pThis == NULL) {
        return NULL; // thread class not correct
    }

    while(1) {
        Command* pCmd = pThis->getCmd();

        ret = pThis->dispatch(pCmd); // call the dispatcher

        // std::cout << "SisThread" << pCmd->getCmdId() << "\n";

        if(pCmd->getSync()) {
            pCmd->getSync()->signal(); // release the calling task
        }

        pThis->removeCmd(pCmd); // delete the command

        if(pThis->m_cmdQueue.size() > 1) {
        }

        if(ret == CMD_THREAD_STOP) {
            return NULL; // stop thread and exit
        }
        else if(ret == CMD_NOT_HANDLED) {
        }
        else if(ret == CMD_HANDLED_FAILED) {
        }
    }
    pthread_exit(NULL);
}


int SisThread::dispatch(Command* pCmd) {
    int ret = CMD_NOT_HANDLED;

    if(!pCmd) {
        return ret;
    }

    if(pCmd->getCmdId() == 0) { // commands to stop the thread
        ret = stopCmd();
    }

    return ret;
}

int SisThread::post(Command* pCmd) {
    pthread_mutex_lock(&m_mutex);
    m_cmdQueue.push(pCmd);
    //    if(m_threadName != "LoggerThread") {
    //    	DTRACE3("%i Cmd is %i \n", m_threadName, pCmd->getCmdId())
    //    }
    pthread_mutex_unlock(&m_mutex);
    resume();
    return 0;
}


int SisThread::getScheduleAndPrio(pthread_t id, int& policy, int& prio) {
    int* pPolicy = &policy;
    struct sched_param param;
    if((pthread_getschedparam(id, pPolicy, &param)) == 0) {
        prio = param.__sched_priority;
    }
    else {
        return -1;
    }
    return 0;
}


int SisThread::setScheduleAndPrio(pthread_t id, int policy, int prio) {
    struct sched_param param;
    param.sched_priority = prio;
    if((pthread_setschedparam(id, policy, &param)) != 0) {
        return -1;
    }
    return 0;
}


void SisThread::pause() {
    pthread_cond_wait(&m_condVar, &m_mutex);
    // in pause command we don't need to signal m_condVar because we are not in
    // wait state now
}

void SisThread::resume() {
    pthread_cond_signal(&m_condVar);
}

Command* SisThread::getCmd() {
    pthread_mutex_lock(&m_mutex);
    while(m_cmdQueue.empty()) {
        pause();
    }
    Command* pCmd = m_cmdQueue.front();
    pthread_mutex_unlock(&m_mutex);
    return pCmd;
}

void SisThread::removeCmd(Command* pCmd) {
    pthread_mutex_lock(&m_mutex);
    m_cmdQueue.pop(); // remove the command from the queue
    pthread_mutex_unlock(&m_mutex);

    if(pCmd) {
        delete pCmd;
        pCmd = NULL;
    }
}

int SisThread::stopCmd() {
    return CMD_THREAD_STOP;
}
