/** *
 * \file	Sync.cpp
 * \date	09.5.2016
 * \author	brr
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 */

#include "Sync.h"

#include <stddef.h>

Sync::Sync(): m_enableSuspend(true) {
    // initialize mutex and condition variable
    pthread_mutex_init(&m_mutex, NULL);
    pthread_cond_init(&m_condVar, NULL);
}

Sync::~Sync() {
    pthread_cond_destroy(&m_condVar);
    pthread_mutex_destroy(&m_mutex);
}


void Sync::wait() {
    pthread_mutex_lock(&m_mutex);
    if(m_enableSuspend) {
        pthread_cond_wait(&m_condVar, &m_mutex);
        m_enableSuspend = false;
    }
    pthread_mutex_unlock(&m_mutex);
}

void Sync::signal() {
    pthread_mutex_lock(&m_mutex);
    m_enableSuspend = false;
    pthread_cond_signal(&m_condVar);
    pthread_mutex_unlock(&m_mutex);
}

void Sync::reset() {
    pthread_mutex_lock(&m_mutex);
    m_enableSuspend = true; // enable the sync object again
    pthread_mutex_unlock(&m_mutex);
}

pthread_mutex_t& Sync::getMutex() {
    return m_mutex;
}

pthread_cond_t& Sync::getCond() {
    return m_condVar;
}
