/** *
 * \file	Encoder.cpp
 * \date	15.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief 	implementation of the encoder class
 */

#include "Encoder.h"

#include <opus.h>

#include <chrono>
#include <iostream>
#include <string>
#include <vector>

#include "CodecCtl.h"
#include "Command.h"
#include "ICorTel.h"
#include "Result.h"
#include "SipConf.h"
#include "Sync.h"
#include "TelStruct.h"

Encoder::Encoder(ICorTel* pItf, AudioRouter* rt, SisComSip* sip)
    : SisThread(eEncoder),
      m_TelItf(pItf),
      m_encoder(),
      m_Router(rt),
      m_sip(sip) {
}

Encoder::~Encoder() {
    // destroy the encoder
    opus_encoder_destroy(m_encoder);
}

int Encoder::init(Sync* pSync /*= NULL*/, Result* pRes /*= NULL*/) {
    if(pSync) {
        pSync->reset();
    }
    Command* pCmd = new Command(eCmd_init, NULL, pSync, pRes);
    post(pCmd);
    return 0;
}

int Encoder::Encode(Sync* pSync /*= NULL*/, Result* pRes /*= NULL*/) {
    if(pSync) {
        pSync->reset();
    }
    Command* pCmd = new Command(eCmd_Encode, NULL, pSync, pRes);
    post(pCmd);
    return 0;
}

int Encoder::dispatch(Command* pCmd) {
    Result res;                // result with error message
    Sync sync;                 // pthread sync parameter
    int ret = CMD_NOT_HANDLED; // return value

    if(!pCmd) {
        return ret;
    }

    if(pCmd->getCmdId() >= 0) { // get commands to run

        Result* pRes = NULL;

        if(pCmd->getResult()) {
            pRes = pCmd->getResult(); // get result from command
        }
        else {
            pRes = &res; // result internal
        }

        // normal running process..
        switch(pCmd->getCmdId()) {
            case eCmd_init: // execute the init command
                *pRes += initCmd();
                ret = CMD_HANDLED_SUCCESSFUL;
                break;

            case eCmd_Encode: // execute the encode command
                *pRes += EncodeCmd();
                ret = CMD_HANDLED_SUCCESSFUL;
                break;

            default: // call in any case the base dispatcher
                ret = SisThread::dispatch(pCmd);
                break;
        }
    }
    return ret;
}

Result Encoder::initCmd() {
    Result res;               // Result with error message
    int err;                  // variable for the opus error message
    std::string ErrorMessage; // Error message

    /* Create a new encoder state */
    m_encoder = opus_encoder_create(SAMPLE_RATE, CHANNELS, APPLICATION, &err);
    if(err < 0) {
        ErrorMessage = "Error while creating the Encoder: ";
        ErrorMessage += opus_strerror(err);
        res.setResult(
            ERR_ENC_CREATE, ErrorMessage, __FILE__, __func__, __LINE__);
    }

    /* Set bit-rate */
    err = opus_encoder_ctl(m_encoder, OPUS_SET_BITRATE(BITRATE));
    if(err < 0) {
        ErrorMessage = "Error while setting the Encoder Bit rate: ";
        ErrorMessage += opus_strerror(err);
        res.setResult(
            ERR_ENC_CREATE, ErrorMessage, __FILE__, __func__, __LINE__);
    }

    /* Disable variable bit-rate */
    err = opus_encoder_ctl(m_encoder, OPUS_SET_VBR(0));
    if(err < 0) {
        ErrorMessage = "Error while setting the Encoder to Fixed bit rate: ";
        ErrorMessage += opus_strerror(err);
        res.setResult(
            ERR_ENC_CREATE, ErrorMessage, __FILE__, __func__, __LINE__);
    }

    /* Set input Signal type to voice */
    err = opus_encoder_ctl(m_encoder, OPUS_SET_SIGNAL(OPUS_AUTO));
    if(err < 0) {
        ErrorMessage = "Error while setting the Encoder signal type to voice: ";
        ErrorMessage += opus_strerror(err);
        res.setResult(
            ERR_ENC_CREATE, ErrorMessage, __FILE__, __func__, __LINE__);
    }
    return res;
}

Result Encoder::EncodeCmd() {
    Result res;                 // Result with error message
    std::vector<int> SamplesIn; // Vector of input Samples
    std::vector<int>
        SamplesInPC316;       // Vector of input Samples of the analog telephone
    std::vector<int> CodeOut; // Vector of Coded Data
    unsigned char cbits[MAX_PACKET_SIZE]; // Array for coded Data
    opus_int16
        in[FRAME_SIZE * CHANNELS]; // Array for one Frame of input samples
    int nbBytes; // Number of Coded Bytes. If the Value is negative the encoder
                 // has made an Error
    std::string ErrorMessage; // Error message

    // Get the audio from PC316 first and push it to the router cause it may be
    // a pure analog phone
    if(m_Router->getRoute(eSourcePC316) != eNoSink) {
        res += m_TelItf->getSamplesIn(SamplesInPC316);
        m_Router->pushFrame(eSourcePC316, SamplesInPC316);
    }

    m_Router->getFrame(eSinkEncoder, SamplesIn);

    /* set the acknowledgment of reading received data */
    res += m_TelItf->setSamplesInAck(true);

    /* Convert Samples into 16 bit Local Tel Samples for the encoder */
    for(unsigned int i = 0; i < SamplesIn.size(); ++i) {
        in[i] = (short) SamplesIn.at(i);
    }

    /* Encode the frame */
    nbBytes = opus_encode(m_encoder, in, FRAME_SIZE, cbits, MAX_PACKET_SIZE);
    if(nbBytes < 0) {
        // if the encoding fails generate Error message and abort the writing of
        // the data to the Cor
        ErrorMessage = "Error while encoding: ";
        ErrorMessage += opus_strerror(nbBytes);
        res.setResult(ERR_ENCODING, ErrorMessage, __FILE__, __func__, __LINE__);
        return res;
    }

    /* convert encoded output to a vector */
    CodeOut.resize(nbBytes);
    CodeOut.assign(cbits, cbits + nbBytes);

    /* write Data Out */
    res += m_TelItf->setCodeTx(CodeOut);

    /* reset the acknowledge flag */
    res += m_TelItf->setSamplesInAck(false);

#ifdef DEBUG_TIMING
    static std::chrono::high_resolution_clock::time_point start;
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::microseconds>(
                     stop - start)
                     .count()
              << "us encode" << std::endl;
    start = std::chrono::high_resolution_clock::now();
#endif

    return res;
}
