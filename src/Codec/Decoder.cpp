/** *
 * \file	Decoder.cpp
 * \date	15.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief 	implementation of the decoder class
 */

#include "Decoder.h"

#include <Command.h>
#include <opus.h>

#include <chrono>
#include <iostream>
#include <string>
#include <vector>

#include "CodecCtl.h"
#include "ErrorId.h"
#include "SipConf.h"
#include "Sync.h"

Decoder::Decoder(ICorTel* pItf, AudioRouter* rt)
    : SisThread(eDecoder), m_TelItf(pItf), m_decoder(), m_router(rt) {
}

Decoder::~Decoder() {
    // destroy the opus decoder
    opus_decoder_destroy(m_decoder);
}

int Decoder::init(Sync* pSync /*= NULL*/, Result* pRes /*= NULL*/) {
    if(pSync) {
        pSync->reset();
    }
    Command* pCmd = new Command(eCmd_init, NULL, pSync, pRes);
    post(pCmd);
    return 0;
}

int Decoder::Decode(Sync* pSync /*= NULL*/, Result* pRes /*= NULL*/) {
    if(pSync) {
        pSync->reset();
    }
    Command* pCmd = new Command(eCmd_Decode, NULL, pSync, pRes);
    post(pCmd);
    return 0;
}

int Decoder::dispatch(Command* pCmd) {
    Result res;                // result with error message
    Sync sync;                 // pthread sync parameter
    int ret = CMD_NOT_HANDLED; // return value

    if(!pCmd) {
        return ret;
    }

    if(pCmd->getCmdId() >= 0) { // get commands to run
        Result* pRes = NULL;

        if(pCmd->getResult()) {
            pRes = pCmd->getResult(); // get result from the command
        }
        else {
            pRes = &res; // result internal
        }

        // normal running process..
        switch(pCmd->getCmdId()) {
            case eCmd_init: // execute the init command
                *pRes += initCmd();
                ret = CMD_HANDLED_SUCCESSFUL;
                break;

            case eCmd_Decode: // execute the decode command
                *pRes += DecodeCmd();
                ret = CMD_HANDLED_SUCCESSFUL;
                break;

            default: // call the base dispatcher in any case
                ret = SisThread::dispatch(pCmd);
                break;
        }
    }
    return ret;
}

Result Decoder::initCmd() {
    Result res; // Result with the error message
    int err;    // variable for the opus error message

    /* Create a new decoder state. */
    m_decoder = opus_decoder_create(SAMPLE_RATE, CHANNELS, &err);
    if(err < 0) {
        // create Error message
        std::string ErrorMessage = "Error while creating the Decoder: ";
        ErrorMessage += opus_strerror(err);
        res.setResult(
            ERR_DEC_CREATE, ErrorMessage, __FILE__, __func__, __LINE__);
    }
    return res;
}

Result Decoder::DecodeCmd() {
    Result res;                                // Result for error messages
    std::vector<int> CodeIn;                   // vector for the coded input
    unsigned char cbits[MAX_PACKET_SIZE];      // Array for coded input frame
    opus_int16 out[MAX_FRAME_SIZE * CHANNELS]; // Array for decoded output frame
    int nbBytes;    // exact number of Coded Bytes to decode in this Frame
    int frame_size; // size of the decoded frame, If the Value is negative the
                    // decoder has made an Error

    /* Read a coded audio frame form the Cor */
    res += m_TelItf->getCodeRx(CodeIn);

    /* set the acknowledgment of reading received data */
    res += m_TelItf->setCodeRxAck(true);

    /* get the number of coded Bytes in this frame */
    nbBytes = CodeIn.size();

    /* Convert coded vector into coded input array */
    for(int i = 0; i < nbBytes; ++i) {
        cbits[i] = (short) CodeIn.at(i);
    }

    /* Decode the data */
    frame_size = opus_decode(m_decoder, cbits, nbBytes, out, MAX_FRAME_SIZE, 0);
    if(frame_size < 0) {
        // if the decoding fails generate Error message and abort the writing of
        // the data to the Cor
        std::string ErrorMessage = "Error while decoding: ";
        ErrorMessage += opus_strerror(frame_size);
        res.setResult(ERR_DECODING, ErrorMessage, __FILE__, __func__, __LINE__);
    }
    else {
        std::vector<int> SamplesOut(
            std::begin(out),
            std::begin(out) + frame_size); // vector for the decoded Samples

        /* Forward the decoded audio to the AudioRouter*/
        m_router->pushFrame(eSourceDecoder, SamplesOut);
    }

#ifdef DEBUG_TIMING
    static std::chrono::high_resolution_clock::time_point start;
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::microseconds>(
                     stop - start)
                     .count()
              << "us decode" << std::endl;
    start = std::chrono::high_resolution_clock::now();
#endif

    /* reset the acknowledge flag */
    res += m_TelItf->setCodeRxAck(false);
    return res;
}