/**
 * \file	ConfigParser.cpp
 * \date	20.11.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief This Class is for reading and parsing a JSON file.
 *        This JSON file defines all Call numbers and adresses.
 *        Including the PBX's address, and password.
 */

#include "ConfigParser.h"

#include "ConfTypeConverter.h"
#include "Result.h"


ConfigParser::ConfigParser(EStation station) {
    m_station = station;
    if(station == eUndefined) {
        throw std::invalid_argument(FFLtoStr(
            "Invalid/Undefined station configured!",
            __FILE__,
            __FUNCTION__,
            __LINE__));
    }
    parse();
}

ConfigParser::~ConfigParser() {
}

void ConfigParser::parse() {
    m_confRouteList.clear();

    // Read the configuration file
    json j;
    try {
        std::ifstream jStream(CONF_LOC, std::ifstream::in);
        jStream >> j;
        jStream.close();
    }
    catch(...) {
        throw std::invalid_argument(FFLtoStr(
            "Codec Config File Missing or not readable",
            __FILE__,
            __FUNCTION__,
            __LINE__));
    }

    // Read basic settings
    m_isSipLogEnabled = j.at("sipLoggingEnabled");

    // Read the routing pairs
    json routes = j.at("routing");
    for(auto rt = routes.begin(); rt != routes.end(); ++rt) {
        if(configTextToStation(rt->at("station")) == m_station) {
            std::pair<ESource, ESink> pair1 = std::make_pair(
                configTextToSource(rt->at("source1")),
                configTextToSink(rt->at("sink1")));
            std::pair<ESource, ESink> pair2 = std::make_pair(
                configTextToSource(rt->at("source2")),
                configTextToSink(rt->at("sink2")));
            m_confRouteList.push_back(pair1);
            m_confRouteList.push_back(pair2);
        }
    }

    // Make sure there are always only two routes present,
    // otherwise the config is wrong
    assert(m_confRouteList.size() == 2);

    // Read all phone numbers
    json numbers = j.at("phoneNumbers");
    for(auto nm = numbers.begin(); nm != numbers.end(); ++nm) {
        // Create a temporary number and set its station
        SPhoneNumber tmpNum;
        tmpNum.station = configTextToStation(nm->at("station"));
        try{
            tmpNum.description = nm->at("desc");
        }
        catch(...){
            tmpNum.description = "";
        }

        // If the type is SIP, just read the number and set non IP mode
        if(nm->at("type") == "SIP") {
            if(m_station == tmpNum.station) {
                m_addrPBX = nm->at("ip");
                m_passPBX = nm->at("pbxPass");
            }
            tmpNum.num         = nm->at("num");
            tmpNum.isIPAddress = "0";
            tmpNum.callType    = eSIP;
            m_stationsWithPBX.insert(tmpNum.station);
        }

        // With an IP type
        else if(nm->at("type") == "IP") {
            if(eMaster == tmpNum.station) {
                throw std::invalid_argument(FFLtoStr(
                    "Invalid config, Master station cant contain a non PBX "
                    "Phone",
                    __FILE__,
                    __FUNCTION__,
                    __LINE__));
            }

            tmpNum.callType  = eIP;
            tmpNum.num       = nm->at("num");
            tmpNum.isIPAddress = nm->at("ip");
        }
        else if(nm->at("type") == "PC316") {
            tmpNum.callType  = ePC316;
        }

        if(tmpNum.callType != ePC316){
            m_phoneNumbers.push_back(tmpNum);
        }
    }
}

std::vector<std::pair<ESource, ESink>> ConfigParser::getRoutes() {
    return m_confRouteList;
}

std::pair<ESource, ESink> ConfigParser::getRoute(unsigned int pos) {
    if(pos > m_confRouteList.size()) {
        std::pair<ESource, ESink> invalid = {eNoSource, eNoSink};
        return invalid;
    }
    else {
        return m_confRouteList.at(pos);
    }
}

std::string ConfigParser::getPBXAddress() {
    return m_addrPBX;
}

std::string ConfigParser::getPBXPassword() {
    return m_passPBX;
}

std::vector<SPhoneNumber> ConfigParser::getNumbers() {
    return m_phoneNumbers;
}

SPhoneNumber ConfigParser::getNumber(unsigned int pos) {
    if(m_phoneNumbers.size() >= pos) {
        return m_phoneNumbers.at(pos);
    }
    else {
        SPhoneNumber emptyNum;
        emptyNum.num         = "";
        emptyNum.station     = eMaster;
        emptyNum.isIPAddress = "0";
        return emptyNum;
    }
}

std::vector<SPhoneNumber> ConfigParser::getOwnNumbers() {
    std::vector<SPhoneNumber> stationNumbers;
    std::vector<SPhoneNumber>::iterator itr;
    for(itr = m_phoneNumbers.begin(); itr != m_phoneNumbers.end(); itr++) {
        if(itr->station == m_station) {
            stationNumbers.push_back((*itr));
        }
    }
    return stationNumbers;
}

std::vector<SPhoneNumber> ConfigParser::getNumbersToRegister() {
    std::vector<SPhoneNumber> numberToRegister;
    std::vector<SPhoneNumber>::iterator itr;
    for(itr = m_phoneNumbers.begin(); itr != m_phoneNumbers.end(); itr++) {
        if(itr->station != m_station) {
            numberToRegister.push_back((*itr));
        }
    }
    return numberToRegister;
}

SConfSip ConfigParser::getConf() {
    SConfSip conf = {
        .addrPBX         = m_addrPBX,
        .passPBX         = m_passPBX,
        .station         = m_station,
        .stationsWithPBX = m_stationsWithPBX,
        .isSipLogEnabled = m_isSipLogEnabled};

    return conf;
}

bool ConfigParser::isUsingIPCalls() {
    return (
        m_confRouteList.at(0).first == eSourceSIP ||
        m_confRouteList.at(0).second == eSinkSIP);
}
