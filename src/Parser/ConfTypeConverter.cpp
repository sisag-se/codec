/**
 * \file	ConfTypeConverter.cpp
 * \date	04.05.2021
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief This File assists in the conversion of config
 *        Text to enum values and back.
 */

#include "ConfTypeConverter.h"

EStation configTextToStation(const std::string conf) {
    if(conf == "master") {
        return eMaster;
    }
    else if(conf == "slave1") {
        return eSlave1;
    }
    else if(conf == "slave2") {
        return eSlave2;
    }
    else if(conf == "slave3") {
        return eSlave3;
    }
    else {
        return eUndefined;
    }
}

ESink configTextToSink(const std::string conf) {
    if(conf == "encoder") {
        return eSinkEncoder;
    }
    else if(conf == "PC316") {
        return eSinkPC316;
    }
    else if(conf == "SIP") {
        return eSinkSIP;
    }
    else {
        return eNoSink;
    }
}

ESource configTextToSource(const std::string conf) {
    if(conf == "decoder") {
        return eSourceDecoder;
    }
    else if(conf == "PC316") {
        return eSourcePC316;
    }
    else if(conf == "SIP") {
        return eSourceSIP;
    }
    else {
        return eNoSource;
    }
}