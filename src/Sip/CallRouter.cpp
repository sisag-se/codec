/**
 * \file	CallRouter.cpp
 * \date	20.10.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief  This Class is for sending and receiving DTMF signals
 */

#include "CallRouter.h"

#include <algorithm>

#include "ConfigParser.h"
#include "Result.h"

#define SYMBOL_SEPARATOR "*"
#define SYMBOL_HANGUP "#"
#define DTMF_SEPARATOR 11
#define DTMF_HANGUP 12

CallRouter::CallRouter(DtmfMap* dtmf, ConfigParser* conf, SisComSip* sip)
    : m_dtmf(dtmf), m_conf(conf), m_sip(sip) {
}

CallRouter::~CallRouter() {
}

void CallRouter::poll() {
    SDtmf newDtmf;
    m_dtmf->receiveRemoteDtmf(newDtmf);

    // A DTMF was detected
    if(newDtmf.Det) {
        // Was the DTMF already handled and is it a valid number
        if(!m_singleDTMF && newDtmf.Num != 0) {
            std::cout << "Got DTMF: " << newDtmf.Num << std::endl;
            handleDTMF(newDtmf);
            m_singleDTMF = true; // The DTMF was handled
        }
    }
    // m_singleDTMF is true when the previous DTMF isnt being received anymore
    // m_singleDTMF is false when no DTMF is being received anymore
    // m_resetOnce is true when the timeout to reset m_singleDTMF is running
    else if(m_singleDTMF && !m_resetOnce) {
        dtmfTimer.setTimeout(
            std::bind(&CallRouter::resetSingleDtmf, this),
            DTMF_SEND_TIME,
            false);
        m_resetOnce = true;
    }
}

void CallRouter::resetSingleDtmf() {
    m_singleDTMF = false;
    m_resetOnce  = false;
}

void CallRouter::handleDTMF(SDtmf newDtmf) {
    // A new DTMF was received whilst not being in a call
    // Thus a new call is being started
    if(m_sip->getCallState() == eInactive && newDtmf.Num != DTMF_HANGUP) {
        m_sip->setCallState(eCalling);
    }

    // Only do a hangup when in a call or someone else is in a call(passive)
    // Note: This will likely never happen because incoming calls are hung
    // up in SisAccount when the current call state isn't inactive.
    // Also this will very likely cause ALL calls to be hung up not just the new arriving one!
    if(m_sip->getCallState() == eInCall || m_sip->getCallState() == ePassive) {
        if(newDtmf.Num == DTMF_HANGUP) {
            m_bufferDTMF.push_back(SYMBOL_HANGUP);
        }
    }
    else {
        // Replace special DTMF's with their symbol for parisng
        if(newDtmf.Num == DTMF_SEPARATOR) {
            m_bufferDTMF.push_back(SYMBOL_SEPARATOR);
        }
        else if(newDtmf.Num == DTMF_HANGUP) {
            m_bufferDTMF.push_back(SYMBOL_HANGUP);
        }
        else if(newDtmf.Num == 10) { // Num 0 = DTMF 10
            m_bufferDTMF.push_back("0");
        }
        else if(newDtmf.Num > 12) {
            throw std::invalid_argument(FFLtoStr(
                "DTMF's above 12 (*) are not implemented?!",
                __FILE__,
                __FUNCTION__,
                __LINE__));
        }
        else {
            m_bufferDTMF.push_back(std::to_string(newDtmf.Num));
        }
    }


    SPhoneHeader header;
    header.state = eInvalid;
    header       = parseBufferForNumber();
    handleParserResult(header);
}

void CallRouter::handleParserResult(SPhoneHeader header) {
    if(header.state == eCalling) {
        //try {
            std::cout << "make call: ";
            std::cout << "isIP: " << header.isIpCall;
            std::cout << " caller: " << header.caller;
            std::cout << " callee: " << header.callee << std::endl;
            if(header.isIpCall) {
                m_sip->makeCall(std::stoi(header.caller), header.callee);
            }
            else {
                m_sip->makeCall(
                    std::stoi(header.caller), std::stoi(header.callee));
            }
        //}
        /*catch(...) {
            /// \todo This is an edgecase that has not been handles yet!
            std::cout << "The current call is occupied! This can only happen "
                         "when two calls are started simultaniously!"
                      << std::endl;
        }*/
    }
    else if(header.state == eAccept) {
        m_sip->setCallState(eInCall);
        m_sip->acceptCall();
    }
    else if(header.state == eHangup) {
        m_sip->hangup();
    }
    else if(header.state == ePassive) {
        m_sip->setCallState(ePassive);
    }
    else if(header.state == eInvalid) {
        // Do nothing
    }
    else {
        throw std::logic_error(FFLtoStr(
            "There is and invalid state when parsing the DTMF buffer!",
            __FILE__,
            __FUNCTION__,
            __LINE__));
    }
}

void CallRouter::sendHeader(SPhoneHeader header) {
    if(header.state == eInvalid) {
        throw std::invalid_argument(FFLtoStr(
            "The given Header is invalid", __FILE__, __FUNCTION__, __LINE__));
    }

    SDtmf tmpDtmf;
    tmpDtmf.isLocal = false;

    //*
    tmpDtmf.Num = DTMF_SEPARATOR;
    m_dtmf->sendDtmf(tmpDtmf);

    // CALLEE
    // Send the number inverted cause the polling reverses the order
    for(unsigned int i = 0; i < header.callee.length(); ++i) {
        tmpDtmf.Num = std::stoi(header.callee.substr(i, 1));
        if(tmpDtmf.Num == 0) {
            tmpDtmf.Num = 10;
        }
        m_dtmf->sendDtmf(tmpDtmf);
    }

    // *
    tmpDtmf.Num = DTMF_SEPARATOR;
    m_dtmf->sendDtmf(tmpDtmf);

    // CALLER
    // Send the number inverted cause the polling reverses the order
    for(unsigned int i = 0; i < header.caller.length(); ++i) {
        tmpDtmf.Num = std::stoi(header.caller.substr(i, 1));
        if(tmpDtmf.Num == 0) {
            tmpDtmf.Num = 10;
        }
        m_dtmf->sendDtmf(tmpDtmf);
    }

    // *
    tmpDtmf.Num = DTMF_SEPARATOR;
    m_dtmf->sendDtmf(tmpDtmf);
}

void CallRouter::sendAck() {
    SDtmf tmpDtmf;
    tmpDtmf.isLocal = false;

    //*
    tmpDtmf.Num = DTMF_SEPARATOR;
    m_dtmf->sendDtmf(tmpDtmf);
}

void CallRouter::sendNack() {
    SDtmf tmpDtmf;
    tmpDtmf.isLocal = false;

    //#
    tmpDtmf.Num = DTMF_HANGUP;
    m_dtmf->sendDtmf(tmpDtmf);
}

SPhoneHeader CallRouter::parseBufferForNumber() {
    SPhoneHeader ph;
    ph.state = eInvalid;

    // Return if the buffer is empty
    if(m_bufferDTMF.size() > 0) {
        for(unsigned int g = 0; g < m_bufferDTMF.size(); g++) {
            std::cout << m_bufferDTMF.at(g);
        }
        std::cout << std::endl;
    }
    else {
        ph.state = eInvalid;
        return ph;
    }

    // If there is a # in the buffer it has priority and causes any call to be
    // hung up
    if(std::find(m_bufferDTMF.begin(), m_bufferDTMF.end(), SYMBOL_HANGUP) !=
       m_bufferDTMF.end()) {
        clearBuffer();
        ph.state = eHangup;
        return ph;
    }

    // When in a call and the first DTMF is a * then the call has been accepted
    else if(
        m_sip->getCallState() == eAccept &&
        m_bufferDTMF.at(0) == SYMBOL_SEPARATOR) {
        ph.state = eAccept;
        clearBuffer();
        return ph;
    }
    else {
        // Check if the buffer is empty and "*" terminated
        if(m_bufferDTMF.empty() ||
           m_bufferDTMF.at(m_bufferDTMF.size() - 1) != SYMBOL_SEPARATOR) {
            ph.state = eInvalid;
            return ph;
        }
        if(m_bufferDTMF.at(0) == SYMBOL_SEPARATOR) {
            // Start at one since the * was confirmed above
            // Filter out anything till the next * appears
            // The filtered out text equals the called number
            unsigned int i;
            for(i = 1; i < m_bufferDTMF.size(); ++i) {
                if(m_bufferDTMF.at(i) != SYMBOL_SEPARATOR) {
                    ph.callee += m_bufferDTMF.at(i);
                }
                else {
                    break;
                }
            }

            // Check if there is more in the buffer
            // If not, the header is incomplete and needs further receiving
            // The next symbol is a *, if the buffer ends with just that, its
            // incomplete
            if(i == m_bufferDTMF.size() || i + 1 == m_bufferDTMF.size()) {
                return ph;
            }

            // Check if the called number is in the list of known numbers
            std::vector<SPhoneNumber> phoneNumbers = m_conf->getNumbers();
            EStation currentStation = m_conf->getConf().station;
            bool isStationCalled    = false;
            bool numberExists       = false;
            SPhoneNumber currentNum;
            for(unsigned int j = 0; j < phoneNumbers.size(); ++j) {
                currentNum = phoneNumbers.at(j);
                if(ph.callee == currentNum.num) {
                    numberExists = true;
                    break;
                }
            }

            // If the number exists in the number table, figure out if 
            // the call is for the current station, if not status is passive
            // if yes, is ip or sip call and format call scheme accordingly
            if(numberExists){
                if(currentNum.station == currentStation) {
                    isStationCalled = true;
                    if(currentNum.isIPAddress != "0") {
                        ph.callee   = "sip:" + currentNum.num + "@" + currentNum.isIPAddress;
                        ph.isIpCall = true;
                    }
                    else {
                        ph.isIpCall = false;
                    }
                }
            }
            else if(currentStation == eMaster){
                std::cout << "Number doesnt exist, calling pbx in AS for last resort" << std::endl;
                isStationCalled = true;
                ph.isIpCall = false;
            }

            // Filter till the next * this is the final one
            // Anything till * equals to the calling number
            for(; i < m_bufferDTMF.size(); ++i) {
                if(m_bufferDTMF.at(i) == SYMBOL_SEPARATOR) {
                    ph.state = eCalling; // the end of a valid header was
                                         // reached
                }
                else {
                    ph.caller += m_bufferDTMF.at(i);
                }
            }

            // Clear the buffer when an entire header was parsed
            if(ph.state == eCalling) {
                clearBuffer();
            }

            // When the number exist but is not conneccted to the current
            // station, stay passive
            if(!isStationCalled) {
                ph.state = ePassive;
            }

            // The number does not exist at all, hangup
            // Causes: DTMF's received are wrong, Wrong Config
            // Disabled cause a different approach to non existing numbers is used
            /*if(!numberExists) {
                clearBuffer();
                ph.state = eHangup;
                return ph;
            }*/
        }
    }

    return ph;
}


void CallRouter::clearBuffer() {
    m_bufferDTMF.clear();
}