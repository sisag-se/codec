#include "SisAccount.h"

#include "SisCall.h"

//*** SisAccount Methods ***
//** Public **
SisAccount::SisAccount(SisComSip *sip): m_sip(sip) {
}

SisAccount::~SisAccount() {
    for(std::vector<Call *>::iterator it = calls.begin(); it != calls.end();) {
        delete(*it);
        it = calls.erase(it);
    }
}

void SisAccount::removeCall(Call *call) {
    for(std::vector<Call *>::iterator it = calls.begin(); it != calls.end(); ++it) {
        if(*it == call) {
            delete(*it);
            calls.erase(it);
            break;
        }
    }
    m_sip->setCurrentCallNum(-1);
}

void SisAccount::removeCall() {
    for(unsigned int i = 0; i < calls.size(); ++i) {
        if(calls.at(i)){
            delete(calls.at(i));
        }
    }
    calls.clear();
    calls.resize(0);
    m_sip->setCurrentCallNum(-1);
    std::cout << "calls, DELETED ('-' )" << std::endl;
}

void SisAccount::onRegState(OnRegStateParam &prm) {
    AccountInfo ai = getInfo();
    /*std::cout << (ai.regIsActive ? "*** Register: code="
                                 : "*** Unregister: code=")
              << prm.code << std::endl;*/
}

void SisAccount::onIncomingCall(OnIncomingCallParam &iprm) {
    Call *call               = new SisCall(this, m_sip, iprm.callId);
    CallInfo currentCallInfo = call->getInfo();
    CallOpParam prm;


    if(m_sip->getCallState() != eInactive) {
        prm.statusCode = PJSIP_SC_DECLINE;
        call->hangup(prm);
        delete call;
    }
    else {
        std::cout << "*** Incoming Call: " << currentCallInfo.remoteUri << " ["
                  << currentCallInfo.stateText << "]" << std::endl;
        calls.push_back(call);
        
        prm.statusCode = PJSIP_SC_RINGING;
        call->answer(prm);

        m_sip->setCurrentCallNum(m_sip->uriToNumber(currentCallInfo.localContact));
        m_sip->setCallState(eAccept);

        SPhoneHeader header;
        header.caller =
            std::to_string(m_sip->uriToNumber(currentCallInfo.remoteUri));
        header.callee =
            std::to_string(m_sip->uriToNumber(currentCallInfo.localUri));
        header.state = eInactive;


        std::cout << "rem cont: " << currentCallInfo.remoteContact << std::endl;
        std::cout << "rem uri: " << currentCallInfo.remoteUri << std::endl;
        std::cout << "loc cont: " << currentCallInfo.localContact << std::endl;
        std::cout << "loc uri: " << currentCallInfo.localUri << std::endl;

        std::cout << "Caller: " << header.callee << std::endl;

        m_sip->m_router->clearSamplesBuffer();

        m_sip->m_sendHeaderCallback(header);
    }
}