/**
 * \file	SisComSip.cpp
 * \date	30.09.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief This Class is for handling all the SIP stuff via the pjsip library.
 */

#include "SisComSip.h"

#include <chrono>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>

#include "GetIP.h"
#include "SipConf.h"
#include "SisAccount.h"
#include "SisCall.h"


//*** SisComSip Methods ***
//** Public **
SisComSip::SisComSip(AudioRouter* ar, TelMap* telMap, ECallState* state)
    : SisThread(eSip), m_router(ar), m_telMap(telMap), m_callState(state) {
    setCallState(eInactive);
}

SisComSip::~SisComSip() {
    // libDestroy can raise errors but this doesnt matter as it only gets
    // destroyed upon closing the Codec
    m_enp.libDestroy();
}

void SisComSip::init(
    SConfSip conf,
    std::function<void(SPhoneHeader)> sendHeaderCallback,
    std::function<void()> sendAck,
    std::function<void()> sendNack,
    Sync* pSync /* NULL */,
    Result* pRes /* NULL */) {
    m_conf               = conf;
    m_sendHeaderCallback = sendHeaderCallback;
    m_sendAck            = sendAck;
    m_sendNack           = sendNack;

    if(pSync) {
        pSync->reset();
    }
    Command* pCmd = new Command(eCmd_init, NULL, pSync, pRes);
    post(pCmd);
}

void SisComSip::makeCall(
    unsigned int accountNum,
    std::string num,
    Sync* pSync /* NULL */,
    Result* pRes /* NULL */) {
    try {
        makeCall(getAccountForNumber(accountNum), num, pSync, pRes);
    }
    // The account to call from was not found
    catch(...) {
        registerNumber(accountNum, pSync, pRes);
        usleep(20000); // Sleep for 20ms to account for thread delay, Note: this is dumb!
        makeCall(getAccountForNumber(accountNum), num, pSync, pRes);
    }
}

void SisComSip::makeCall(
    unsigned int accountNum,
    const unsigned int num,
    Sync* pSync /* NULL */,
    Result* pRes /* NULL */) {
    try {
        makeCall(getAccountForNumber(accountNum), num, pSync, pRes);
    }
    // The account to call from was not found
    catch(...) {
        registerNumber(accountNum, pSync, pRes);
        makeCall(getAccountForNumber(accountNum), num, pSync, pRes);
    }
}

void SisComSip::makeCall(
    SisAccount* acc,
    const unsigned int num,
    Sync* pSync /* NULL */,
    Result* pRes /* NULL */) {
    std::string sipURI = "sip:";
    sipURI += std::to_string(num);
    sipURI += "@";
    sipURI += m_conf.addrPBX;
    makeCall(acc, sipURI, pSync, pRes);
}

void SisComSip::makeCall(
    SisAccount* acc,
    std::string num,
    Sync* pSync /* NULL */,
    Result* pRes /* NULL */) {
    if(pSync) {
        pSync->reset();
    }

    if(!(getCallState() == eInactive || getCallState() == eCalling)) {
        throw std::logic_error(FFLtoStr(
            "Cant call two numbers at the same time!",
            __FILE__,
            __FUNCTION__,
            __LINE__));
    }

    if(acc == NULL) {
        throw std::logic_error(FFLtoStr(
            "The account for making the call is NULL!",
            __FILE__,
            __FUNCTION__,
            __LINE__));
    }

    // Clear any residual samples
    m_router->clearSamplesBuffer();
    // Start a timeout that cancels the current call in case it wont get
    // accepted in time
    m_hangupTimer.setTimeout(
        std::bind(&SisComSip::timerHangup, this), HANGUP_TIMEOUT, false);

    paramMakeCall* param = new paramMakeCall;
    param->acc           = acc;
    param->num           = num;

    Command* pCmd = new Command(eCmd_makeCall, param, pSync, pRes);
    post(pCmd);
}

void SisComSip::acceptCall(Sync* pSync /* NULL */, Result* pRes /* NULL */) {
    Command* pCmd = new Command(eCmd_accept, NULL, pSync, pRes);
    post(pCmd);

    std::cout << "accept" << std::endl;

    setCallState(eInCall);
}

void SisComSip::timerHangup() {
    std::cout << "Timeout" << std::endl;
    hangup();
}

void SisComSip::hangup(Sync* pSync /* NULL */, Result* pRes /* NULL */) {
    Command* pCmd = new Command(eCmd_hangup, NULL, pSync, pRes);
    post(pCmd);

    setCallState(eInactive); // open back up for new calls
}

void SisComSip::stopHangupTimer() {
    m_hangupTimer.stop();
}

ECallState SisComSip::getCallState() {
    return *m_callState;
}

void SisComSip::setCallState(ECallState state) {
    *m_callState = state;

    if(state == eInCall) {
        // Hangup the call after 14min 55sec cause always after 15min there is 
        // some weird audio glitching happening. Cause unknown. 
        m_longCallTimer.setTimeout(
        std::bind(&SisComSip::timerHangup, this), 895000, false);
    }
    else{
        m_longCallTimer.stop();
    }

    // Define the communication direction on the master
    // Slaves will always have noSipComm
    SState newState;
    if(m_conf.station == eMaster) {
        if(state == eInCall) {
            newState.Comm = eSipAsFz;
        }
        else if(state == ePassive) {
            newState.Comm = eSipFzFz;
        }
        else {
            newState.Comm = eNoSipComm;
        }
    }
    else {
        newState.Comm = eNoSipComm;
    }
    m_telMap->setState(eCodeTX, newState);
}

unsigned int SisComSip::getCurrentCallNum() {
    return m_accountInCall;
}

void SisComSip::setCurrentCallNum(unsigned int newNum) {
    std::cout << "Set current num: " << newNum << std::endl;
    m_accountInCall = newNum;
}

void SisComSip::registerNumber(
    unsigned int num, Sync* pSync /* NULL */, Result* pRes /* NULL */) {
    if(pSync) {
        pSync->reset();
    }

    SPhoneNumber* paramNum = new SPhoneNumber;
    paramNum->num          = std::to_string(num);
    paramNum->isIPAddress  = "0";

    std::cout << "Registering Number: " << paramNum->num << std::endl;

    Command* pCmd = new Command(eCmd_regNumber, paramNum, pSync, pRes);
    post(pCmd);
}

void SisComSip::registerNumber(
    SPhoneNumber num, Sync* pSync /* NULL */, Result* pRes /* NULL */) {
    if(pSync) {
        pSync->reset();
    }

    SPhoneNumber* paramNum = new SPhoneNumber;
    *paramNum              = num;

    std::cout << "Registering Number: " << paramNum->num << std::endl;

    Command* pCmd = new Command(eCmd_regNumber, paramNum, pSync, pRes);
    post(pCmd);
}

void SisComSip::registerNumbers(
    std::vector<unsigned int> num,
    Sync* pSync /* NULL */,
    Result* pRes /* NULL */) {
    if(pSync) {
        pSync->reset();
    }

    for(unsigned int i = 0; i < num.size(); i++) {
        registerNumber(num.at(i), pSync, pRes);
    }
}

void SisComSip::registerNumbers(
    std::vector<SPhoneNumber> num,
    Sync* pSync /* NULL */,
    Result* pRes /* NULL */) {
    if(pSync) {
        pSync->reset();
    }

    for(unsigned int i = 0; i < num.size(); i++) {
        registerNumber(num.at(i), pSync, pRes);
    }
}

void SisComSip::yeetNumber(
    unsigned int num, Sync* pSync /* NULL */, Result* pRes /* NULL */) {
    if(pSync) {
        pSync->reset();
    }

    Command* pCmd = new Command(eCmd_yeetNumber, &num, pSync, pRes);
    post(pCmd);
}

unsigned int SisComSip::getAudioLevel() {
    ECallState state = getCallState();
    if(state == eInCall) {
        return getAudioLevel(m_accountInCall);
    }
    else { // Currently no call "active" to get the audio from
        return 0;
    }
}

unsigned int SisComSip::getAudioLevel(unsigned int callNum) {
    if(m_registeredAccounts.size() == 0 ||
       m_registeredAccounts.find(callNum) == m_registeredAccounts.end()) {
        std::cout << "There are no accounts around here" << std::endl;
        return 0;
    }
    // Avoid access violations
    else if(m_registeredAccounts.at(callNum)->calls.size() == 0) {
        std::cout << "There are no active calls for this account" << std::endl;
        return 0;
    }
    try {
        m_registeredAccounts.at(callNum)->calls.at(0);

        unsigned int* param = new unsigned int;
        *param              = callNum;

        Command* pCmd = new Command(eCmd_getAudioLevel, param, NULL, NULL);
        post(pCmd);

        return m_audioLevel;
    }
    catch(...) {
        return 0;
    }
}

unsigned int SisComSip::uriToNumber(std::string uri) {
    bool state      = false; // true when a number was detected
    std::string num = "";

    // Search till the first digit is found then collect them till no further
    // digits are found
    for(unsigned int i = 0; i < uri.length(); i++) {
        if(isdigit(uri[i])) {
            if(state == false) {
                state = true;
            }
            num += uri[i];
        }
        else if(state) {
            break;
        }
    }
    return atoi(num.c_str());
}

//************************************************************************************************************
//*** Dispatcher ***
//************************************************************************************************************
int SisComSip::dispatch(Command* pCmd) {
    Result res;                // result with error message
    Sync sync;                 // pthread sync parameter
    int ret = CMD_NOT_HANDLED; // return value

    // Check for valid command pointer
    if(!pCmd) {
        return CMD_NOT_HANDLED;
    }

    if(pCmd->getCmdId() >= 0) { // get commands to run
                                // normal running process..
        int cmdId = pCmd->getCmdId();
        if(cmdId == eCmd_init) {
            thInit();
            ret = CMD_HANDLED_SUCCESSFUL;
        }
        else if(cmdId == eCmd_makeCall) {
            paramMakeCall* param = (paramMakeCall*) pCmd->getParam();
            thMakeCall(param->acc, param->num);
            delete param;
        }
        else if(cmdId == eCmd_regNumber) {
            SPhoneNumber* param = (SPhoneNumber*) pCmd->getParam();
            thRegisterNumber(*param);
            delete param;
        }
        else if(cmdId == eCmd_yeetNumber) {
        }
        else if(cmdId == eCmd_getAudioLevel) {
            unsigned int* callPos = (unsigned int*) pCmd->getParam();
            thGetAudioLevel(callPos);
            delete callPos;
        }
        else if(cmdId == eCmd_hangup) {
            thHangup();
        }
        else if(cmdId == eCmd_accept) {
            thAccept();
        }

        else {
            // call the base dispatcher in any other case
            ret = SisThread::dispatch(pCmd);
        }
    }
    return ret;
}



//************************************************************************************************************
//*** Private Methods ***
//************************************************************************************************************
bool SisComSip::hasStationPBX(EStation station) {
    if(m_conf.stationsWithPBX.find(station) != m_conf.stationsWithPBX.end()) {
        return true;
    }
    else {
        return false;
    }
}

void SisComSip::thInit() {
    // The following functions can raise an error, let the program crash as this
    // is certainly due to a user error
    m_enp.libCreate(); // Create Sip Endpoint

    configureSip(); // Set Log and Transport stuff and start the endpoint
}

Call* SisComSip::thMakeCall(SisAccount* acc, std::string num) {
    std::cout << "calling: " << num << std::endl;

    Call* call = new SisCall(acc, this, -1);
    acc->calls.push_back(call);
    CallOpParam prm(true);
    prm.opt.audioCount = 1;
    prm.opt.videoCount = 0;

    call->makeCall(num, prm);

    return call;
}

SisAccount* SisComSip::thRegisterNumber(SPhoneNumber num) {
    Result res;

    std::string uri = "sip:";
    uri += num.num;
    uri += "@";
    // If the current station has a PBX, register all number on this PBX
    // Otherwise use the local address for registration
    bool usePBX = hasStationPBX(m_conf.station);
    if(usePBX) {
        uri += m_conf.addrPBX;
    }
    else {
        uri += getIP();
    }

    std::cout << num.isIPAddress << std::endl;
    std::cout << uri << std::endl;

    AccountConfig acc_cfg;
    acc_cfg.idUri = uri;
    if(usePBX) {
        acc_cfg.regConfig.registrarUri = "sip:";
        acc_cfg.regConfig.registrarUri += m_conf.addrPBX;
    }
    acc_cfg.sipConfig.authCreds.push_back(
        AuthCredInfo("digest", "*", num.num, 0, m_conf.passPBX));

    // Create new account
    SisAccount* acc = new SisAccount(this);
    try {
        acc->create(acc_cfg);
    }
    catch(...) {
        res.setResult(
            ERR_SIP, "Adding account failed", __FILE__, __func__, __LINE__);
        return NULL;
    }

    // Add the new account to a map which associates it with its number
    m_registeredAccounts.emplace(std::stoi(num.num), acc);

    return acc;
}

Result SisComSip::thYeetNumber(unsigned int num) {
    Result res;

    std::map<unsigned int, SisAccount*>::iterator accountPointer =
        m_registeredAccounts.find(num);
    if(accountPointer != m_registeredAccounts.end()) {
        delete accountPointer->second;
        m_registeredAccounts.erase(accountPointer);
        return res;
    }

    res.setResult(
        ERR_SIP,
        "Deleting account failed. Number not present in list!",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

void SisComSip::thGetAudioLevel(unsigned int* callPos) {
    SisCall* activeCall =
        (SisCall*) m_registeredAccounts.at(*callPos)->calls.at(0);
    try {
        if(activeCall->m_capPortId != -1) {
            unsigned int newLevel = 0;
            pjsua_conf_get_signal_level(
                activeCall->m_capPortId, &newLevel, NULL);

            if(newLevel == 0){
                std::cout << "Warning! Audio level is zero!" << std::endl;
            }

            // Double the level in order to have it around the same values as
            // PC316 and clamp the max value to 255
            newLevel = newLevel * 2;
            if(newLevel > 255) {
                newLevel = 255;
            }

            // Average the audio level over three function calls
            m_levelQueue.push_back(newLevel);
            if(m_levelQueue.size() > 3) {
                m_levelQueue.erase(m_levelQueue.begin());
            }

            unsigned int avgLevel = 0;
            for(unsigned int i = 0; i < m_levelQueue.size(); i++) {
                avgLevel += m_levelQueue.at(i);
            }
            m_audioLevel = avgLevel / m_levelQueue.size();
        }
        else {
            m_audioLevel = 0;
        }
    }
    catch(...) {
        std::cout << "unable to get audio level" << std::endl;
        m_audioLevel = 0;
    }
}

void SisComSip::thHangup() {
    // Throws an error when the account doesnt exist
    // But since this function can get called anytime someone hangs up,
    // it's possible to have a nonexisting account hung up
    SisAccount* ac;
    try {
        ac = getAccountForNumber(m_accountInCall);
    }
    catch(...) {
        std::cout << "Couldnt find account to hangup" << std::endl;
        return; // The account doesnt exist, just return without doing a thing
    }

    // Regular Hangup Routine
    CallOpParam prm;
    prm.statusCode = PJSIP_SC_DECLINE;
    ac->removeCall();       // Remove call from the accounts internal call list
    m_enp.hangupAllCalls(); // Dont need to specifiy a call object, the endpoint
                            // can end them all
    m_sendNack();           // Tell the others to hang up aswell
    setCurrentCallNum(-1);  // The call no longer exists, reset the number
}

void SisComSip::thAccept() {
    CallOpParam prm;
    prm.statusCode = PJSIP_SC_OK;
    SisCall* callToAccept =
        (SisCall*) getAccountForNumber(m_accountInCall)->calls.at(0);
    callToAccept->answer(prm);
}

void SisComSip::configureSip() {
    // Configure Endpoint
    EpConfig epcfg;
    epcfg.logConfig.level = 6;
    if(m_conf.isSipLogEnabled) {
        epcfg.logConfig.filename = LOG_LOCATION;
    }
    m_enp.libInit(epcfg);

    // Transport
    TransportConfig tcfg;
    tcfg.port = 5060;
    // tcfg.publicAddress = ADDR_LOCAL; //Set this when multiple IP address are
    // present on the device
    m_enp.transportCreate(PJSIP_TRANSPORT_UDP, tcfg);

    // Start library
    m_enp.libStart();
    PJ_LOG(3, (THIS_FILE, "*** PJSUA2 STARTED ***"));
}

SisAccount* SisComSip::getAccountForNumber(unsigned int accountNum) {
    try {
        return m_registeredAccounts.at(accountNum);
    }
    catch(...) {
        throw std::logic_error(FFLtoStr(
            "There is no account present for the given number",
            __FILE__,
            __FUNCTION__,
            __LINE__));
    }
    return nullptr;
}
