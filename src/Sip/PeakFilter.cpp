/** *
 * \file	PeakFilter.cpp
 * \date	09.03.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief This Class is for filtering the AudioLevel from SIP calls via peak
 * detection and decay. Peaks are detected and stored. The Stored peaks slowly
 * decay till a bigger peak comes and replaces the last one
 */



#include "PeakFilter.h"

PeakFilter::PeakFilter() {
}

PeakFilter::~PeakFilter() {
}

unsigned int PeakFilter::filter(unsigned int newLevel) {
    unsigned int levelCalc = 0;
    unsigned int levelFilt = 0;

    levelFilt = m_levelStore / FILTER_DECAY_TIME;
    if((m_levelStore - levelFilt) < 0 || levelFilt < 1) {
        levelCalc = 0;
    }
    else {
        levelCalc = m_levelStore - levelFilt;
    }

    if(newLevel > levelCalc) {
        m_levelStore = newLevel;
    }
    else {
        m_levelStore = levelCalc;
    }

    return m_levelStore;
}