#include "SisCall.h"

#include <chrono>
#include "SipConf.h"

AudioRouter *routerPointer;
SisComSip *scSip;
unsigned int minLevel = 0;

//*** SisCall Methods ***
//** Public **
SisCall::SisCall(Account *acc, SisComSip *sip, int call_id)
    : Call(*acc, call_id), m_sip(sip), m_acc(acc) {
    scSip         = sip;
    routerPointer = sip->m_router;
    STelConfig conf;
    scSip->m_telMap->getTelConfig(conf);
    minLevel = conf.MinLevel;
}

SisCall::~SisCall() {
    destroyMedia();
}

void SisCall::onCallState(OnCallStateParam &prm) {
    PJ_UNUSED_ARG(prm);

    CallInfo ci = getInfo();
    std::cout << "*** Call: " << ci.remoteUri << " [" << ci.stateText << "]"
              << std::endl;

    if(ci.state == PJSIP_INV_STATE_CALLING) {
        std::cout << "*** Call: Incoming" << std::endl;
        if(m_sip->getCallState() == eCalling) {
            m_sip->setCurrentCallNum(m_sip->uriToNumber(ci.localContact));
        }
    }

    else if(ci.state == PJSIP_INV_STATE_CONFIRMED) {
        std::cout << "*** Call: Accepted" << std::endl;
        if(m_sip->getCallState() == eCalling) {
            m_sip->m_sendAck();
            m_sip->stopHangupTimer();
        }
        m_sip->setCallState(eInCall);
        m_sip->setCurrentCallNum(m_sip->uriToNumber(ci.localContact));
        std::cout << "Call says: " << m_sip->getCurrentCallNum() << std::endl;
    }

    else if(ci.state == PJSIP_INV_STATE_DISCONNECTED) {
        std::cout << "*** Call: ended" << std::endl;
        m_sip->setCallState(eInactive);
        m_sip->hangup();
    }
}

void SisCall::onCallTransferRequest(OnCallTransferRequestParam &prm) {
    // Create a new account to transfer to
    prm.newCall = new SisCall(m_acc, m_sip);
}

void SisCall::onCallReplaced(OnCallReplacedParam &prm) {
    // Create a new accout to replace the current one with
    prm.newCall = new SisCall(m_acc, m_sip, prm.newCallId);
}

void SisCall::onCallMediaState(OnCallMediaStateParam &prm) {
    // Cause this function may get called twice (when a call is incomming)
    // we need to destry the previously created media stuff
    // Only the last created media works
    destroyMedia();

    CallInfo ci = getInfo();
    pjsua_call_info ciUA;
    pjsua_call_get_info(ci.id, &ciUA);
    pjsua_conf_port_info cpi;
    pjsua_conf_get_port_info(ciUA.conf_slot, &cpi);

    // Define the buffer size based on the calls default values for BPS and SPF
    unsigned int buf_size = cpi.bits_per_sample * cpi.samples_per_frame / 8;

    // Log the calls audio parameters
    PJ_LOG(3, (THIS_FILE, "bits_per_sample: %d", cpi.bits_per_sample));
    PJ_LOG(3, (THIS_FILE, "samples_per_frame: %d", cpi.samples_per_frame));
    PJ_LOG(3, (THIS_FILE, "clock_rate: %d", cpi.clock_rate));
    PJ_LOG(3, (THIS_FILE, "channel_count: %d", cpi.channel_count));
    PJ_LOG(3, (THIS_FILE, "Buf Size: %d", buf_size));

    // Allocate the send and receive buffers from a memory pool
    // release the pool before creating a new one, as apparantly this method is called twice?????

    m_pool = pjsua_pool_create("POOLNAME", 2000, 2000);
    buffer0   = pj_pool_zalloc(m_pool, buf_size);
    buffer2   = pj_pool_zalloc(m_pool, buf_size);

    // Create the actual capture and playback
    // Capture = Get frames from the IP-Phone
    // Playback = Send Frames to the IP-Phone
    pjmedia_mem_capture_create(
        m_pool,
        buffer0,
        buf_size,
        cpi.clock_rate,
        CHANEL_COUNT,
        cpi.samples_per_frame,
        cpi.bits_per_sample,
        0,
        &m_prtCap);
    pjmedia_mem_capture_set_eof_cb2(m_prtCap, buffer0, captureSipFrames);

    pjmedia_mem_player_create(
        m_pool,
        buffer2,
        buf_size,
        cpi.clock_rate,
        CHANEL_COUNT,
        cpi.samples_per_frame,
        cpi.bits_per_sample,
        0,
        &m_prtPlay);
    pjmedia_mem_player_set_eof_cb(m_prtPlay, buffer2, playSipFrames);
    // The pjmedia_mem_player_set_eof_cb2 doesnt work its two times too slow
    // with pushing frames

    pjsua_conf_add_port(m_pool, m_prtCap, &m_capPortId);
    pjsua_conf_connect(ciUA.conf_slot, m_capPortId); // connect conference with port

    pjsua_conf_add_port(m_pool, m_prtPlay, &m_playPortId);
    pjsua_conf_connect(m_playPortId, ciUA.conf_slot); // Connect port with conference
}

void SisCall::destroyMedia(){
    const std::lock_guard<std::mutex> lockMedia(mediaMutex);
    
    if(m_capPortId > 0){
        pjsua_conf_remove_port(m_capPortId);
    }

    if(m_playPortId > 0){
        pjsua_conf_remove_port(m_playPortId);
    }

    m_capPortId = -1;
    m_playPortId = -1;

    if(m_pool){
        pj_pool_secure_release(&m_pool);
        m_pool = NULL;
        buffer0 = NULL;
        buffer2 = NULL;
    }

    if(m_prtCap){
        pjmedia_port_destroy(m_prtCap);
        m_prtCap = NULL;
    }

    if(m_prtPlay){
        pjmedia_port_destroy(m_prtPlay);
        m_prtPlay = NULL;
    }
}

//*** Classic C-Style Functions for handling audio frames ***
void captureSipFrames(pjmedia_port *port, void *usr_data) {
    // SisCom works with 480 samples per frame, Sip with 160
    // The router collects three sip frames before signalling the encoder to
    // work.

#ifdef DEBUG_TIMING
    static std::chrono::high_resolution_clock::time_point start;
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::microseconds>(
                     stop - start)
                     .count()
              << "us cap" << std::endl;
    start = std::chrono::high_resolution_clock::now();
#endif

// There is a noise floor to cut off unnecessary traffic
// In order to prevent early cutoff, collect some more frames
// noiseFloorCounter counts how many frames have been collected below the noise
// floor The noise floor can be disabled by uncommenting the define in the
// SipConf.h file
#ifndef DISABLE_NOISE_FLOOR
    static unsigned int noiseFloorCounter;
    unsigned int audioLevel = scSip->getAudioLevel();

    // Count how long there was no audio
    // Note: audiolevel can only be an even number
    if(audioLevel < minLevel) {
        if(noiseFloorCounter < NOISE_FLOOR_DELAY + 10) { // prevent an overflow
            noiseFloorCounter++;
        }
    }
    else {
        noiseFloorCounter = 0;
    }

    // Sample as long as there is audio, then sample a bit over that
    if(noiseFloorCounter < NOISE_FLOOR_DELAY) { // 20*20ms = 400ms till cutoof
        std::vector<int> SamplesOut;

        // Make sure the frame has a constant size
        SamplesOut.clear();
        SamplesOut.resize(SIP_SAMPLES_PER_FRAME);

        // Chop void pointer into samples of two Bytes
        uint16_t *test = (uint16_t *) usr_data;
        for(int i = 0; i < SIP_SAMPLES_PER_FRAME; ++i) {
            SamplesOut.at(i) = test[i];
        }
        routerPointer->pushFrame(eSourceSIP, SamplesOut);
    }
#else
    std::vector<int> SamplesOut;

    SamplesOut.clear();
    SamplesOut.resize(SIP_SAMPLES_PER_FRAME);

    // Chop void pointer into samples of two Bytes
    uint16_t *test = (uint16_t *) usr_data;
    for(int i = 0; i < SIP_SAMPLES_PER_FRAME; ++i) {
        SamplesOut.at(i) = test[i];
    }

    routerPointer->pushFrame(eSourceSIP, SamplesOut);
#endif
}

pj_status_t playSipFrames(pjmedia_port *port, void *usr_data) {
    // One SisCom frame needs to be cut down into STORE_FRAMES ammount of SIP
    // frames

#ifdef DEBUG_TIMING
    static std::chrono::high_resolution_clock::time_point start;
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::microseconds>(
                     stop - start)
                     .count()
              << "us play" << std::endl;
    start = std::chrono::high_resolution_clock::now();
#endif

    Result res;                 // Result with error message
    std::vector<int> SamplesIn; // Vector of input Samples
    static int16_t
        in[SIP_SAMPLES_PER_FRAME]; // Array for one Frame of input samples

    routerPointer->getFrame(eSinkSIP, SamplesIn);

    // Chop up a SisCom sample into three sip samples
    for(unsigned int i = 0; i < SamplesIn.size(); ++i) {
        in[i] = (int16_t) SamplesIn.at(i);
    }

    // Two bytes per sample
    memcpy(usr_data, in, SIP_SAMPLES_PER_FRAME * 2);


    return PJ_SUCCESS;
}