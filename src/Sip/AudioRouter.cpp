/**
 * \file	AudioRouter.cpp
 * \date	30.09.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief This class is responsible for routing the audio
 */

#include "AudioRouter.h"

#include <iostream>

#include "Result.h"
#include "SipConf.h"

AudioRouter::AudioRouter(ICorTel* pItf, ECallState* state)
    : m_TelItf(pItf), m_callState(state) {
}

AudioRouter::~AudioRouter() {
}

void AudioRouter::setRoute(ESource src, ESink snk) {
    if(src != eNoSource && snk != eNoSink) {
        removeRoute(src);              // Remove the old route if present
        m_plugboard.emplace(src, snk); // Create the new route

        if(src == eSourceSIP) {
            std::vector<int> emptyFrame;
            emptyFrame.resize(160);
            emptyFrame.clear();
        }
    }
}

void AudioRouter::setRoute(std::pair<ESource, ESink> map) {
    if(map.first != eNoSource && map.second != eNoSink) {
        m_plugboard.emplace(map);
    }
}

void AudioRouter::removeRoute(ESource src) {
    // Locate the source-sink pair in the map and erase it if present
    if(src != eNoSource) {
        std::map<ESource, ESink>::iterator srcMapLoc = m_plugboard.find(src);
        if(srcMapLoc != m_plugboard.end()) {
            m_plugboard.erase(srcMapLoc);
        }
    }
}

void AudioRouter::removeRoute(ESink snk) {
    // Locate the source-sink pair in the map and erase it if present
    if(snk != eNoSink) {
        for(std::map<ESource, ESink>::iterator it = m_plugboard.begin();
            it != m_plugboard.end();
            ++it) {
            if(it->second == snk) {
                m_plugboard.erase(it);
                return;
            }
        }
    }
}

ESink AudioRouter::getRoute(ESource src) {
    std::map<ESource, ESink>::iterator srcMapLoc = m_plugboard.find(src);
    if(srcMapLoc != m_plugboard.end()) {
        return srcMapLoc->second;
    }
    else {
        return eNoSink;
    }
}

ESource AudioRouter::getRoute(ESink snk) {
    for(std::map<ESource, ESink>::iterator it = m_plugboard.begin();
        it != m_plugboard.end();
        ++it)
        if(it->second == snk) {
            return it->first;
        }
    return eNoSource;
}

void AudioRouter::clearAllRoutes() {
    m_plugboard.clear();
}

void AudioRouter::clearSamplesBuffer() {
    clearSamplesBuffer(eNoSink);
}

void AudioRouter::clearSamplesBuffer(ESink snk) {
    switch(snk) {
        case eSinkEncoder:
            m_fifoEncode = {};
            break;
        case eSinkSIP:
            m_fifoDecode = {};
            break;
        case eNoSink:
            m_fifoDecode = {};
            m_fifoEncode = {};
            break;
        case eSinkPC316:
        default:
            std::cout << "Invalid Buffer to clear" << std::endl;
            break;
    }
}

void AudioRouter::pushFrame(ESource src, std::vector<int>& frame) {
    ESink sink = getRoute(src);

    // if any data is received when state is passive during a call
    // Its Frame can be discarded
    if(*m_callState == ePassive) {
        return;
    }

    // Resamples SIP or other frames of the wrong size to the size of a SisCom
    // Frame
    if(src != eSourceSIP || frame.size() > SIP_SAMPLES_PER_FRAME) {
        // Resample frames larger than the SIP frame size
        // Iterate over the resample buffer and pick out a single SIP frame
        // Erase the new frame from the buffer and leave the rest intact
        std::vector<int> oversampleBuffer;
        unsigned int deletedSamples = 0;
        for(unsigned int i = 0; i < frame.size(); i++) {
            oversampleBuffer.push_back(
                frame.at(i - (deletedSamples * SIP_SAMPLES_PER_FRAME)));

            if(oversampleBuffer.size() == SIP_SAMPLES_PER_FRAME) {
                handleFrame(sink, oversampleBuffer);
                oversampleBuffer.clear();
            }
        }
    }
    else if(frame.size() == SIP_SAMPLES_PER_FRAME) {
        handleFrame(sink, frame);
    }
    else {
        throw std::invalid_argument(FFLtoStr(
            "There are not enough samples in the frame. Under 160 Samples!",
            __FILE__,
            __FUNCTION__,
            __LINE__));
    }
}

void AudioRouter::getFrame(ESink snk, std::vector<int>& frame) {
    frame.clear();

    switch(snk) {
        case eSinkSIP:
            if(m_fifoDecode.size() > 0) {
                frame = m_fifoDecode.front();
                m_fifoDecode.pop();
            }
            else {
                frame.clear();
                frame.resize(160);
            }
            break;
        case eSinkEncoder:
            extendFrame(m_fifoEncode, frame);
            break;
        default:
            throw std::invalid_argument(FFLtoStr(
                "There is no buffer for the given sink. Wrong config file?",
                __FILE__,
                __FUNCTION__,
                __LINE__));
            break;
    }
}

void AudioRouter::handleFrame(ESink sink, std::vector<int>& frame) {
    switch(sink) {
        case eSinkEncoder:
            m_fifoEncode.push(frame);
            if(m_fifoEncode.size() >= 3) {
                m_sipFrameReady = true;
            }
            break;
        case eSinkSIP:
            m_fifoDecode.push(frame);
            break;
        case eSinkPC316:
            // The frames are 160 samples in size, store them till 480 Samples
            m_fifoAnalogOut.push(frame);
            if(m_fifoAnalogOut.size() >= STORE_FRAMES) {
                // Get three frames from the buffer and make it one
                std::vector<int> SamplesOut;
                extendFrame(m_fifoAnalogOut, SamplesOut);

                // Adapt the audio frame for PC316 by swapping local and remote
                for(unsigned int i = 0; i < SamplesOut.size(); ++i) {
                    SamplesOut.at(i) = swapLocalRemote(SamplesOut.at(i));
                }

                m_TelItf->setSamplesOut(SamplesOut);
            }

            break;
        case eNoSink:
        default:
            throw std::invalid_argument(FFLtoStr(
                "There is no buffer for the given sink. Wrong config file?",
                __FILE__,
                __FUNCTION__,
                __LINE__));
            break;
    }
}

void AudioRouter::extendFrame(
    std::queue<std::vector<int>>& sampleBuffer, std::vector<int>& frame) {
    if(sampleBuffer.size() >= STORE_FRAMES) {
        for(unsigned int i = 0; i < STORE_FRAMES; i++) {
            frame.insert(
                frame.end(),
                sampleBuffer.front().begin(),
                sampleBuffer.front().end());
            sampleBuffer.pop();
        }
    }
    else {
        std::cout << "Frame buffer to small to extend" << std::endl;
        frame.clear();
        frame.resize(480);
    }
}

bool AudioRouter::isSipFrameReady() {
    return m_sipFrameReady;
}

void AudioRouter::setIsSipFrameReady(bool state) {
    m_sipFrameReady = state;
}

int AudioRouter::swapLocalRemote(int data) {
    short Local  = 0; // low word
    short Remote = 0; // high word

    // mask low word
    Local = data & 0xFFFF;
    // write low word to the high word
    data = Local << 16 | Remote;

    return data;
}