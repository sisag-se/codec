/**
 * \file	GetIP.cpp
 * \date	05.05.2021
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief This file contains a single function to get the local IP address
 */

#include <array>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

std::string getIP() {
    std::array<char, 128> buffer;
    std::string result;

    // Command to get the local addres
    const char* cmd = "ip addr show eth0 | awk  '$1==\"inet\"{print $2}'";

    // Exec the above command and capture the stout of it
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if(!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while(fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }

    // Remove CIDR netmask ath the end
    result.erase(result.find_first_of("/"), result.length());

    return result;
}