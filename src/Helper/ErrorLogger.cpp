/*
 * ErrorLogger.cpp
 *
 *  Created on: 25.04.2019
 *      Author: hen
 */

#include <ErrorLogger.h>

#include "Result.h"

ErrorLogger::ErrorLogger(ICorTel* pItf): m_ErrFlags(), m_TelItf(pItf) {
}

ErrorLogger::~ErrorLogger() {
}

Result ErrorLogger::ErrorLog(SLog& ParamLog) {
    Result res; // result with error messages
    m_ErrFlags[ParamLog.Origin].LowErrId = 0;

    std::list<Result::SResultData> data =
        ParamLog.res->getAllResults(); // get all error results
    std::list<Result::SResultData>::iterator iter =
        data.begin(); // get the iterator of the error result map

    while(iter != data.end()) {
        if((*iter).errorId != 0) { // enter most prioritized Error-ID
            if(m_ErrFlags[ParamLog.Origin].LowErrId == 0) {
                m_ErrFlags[ParamLog.Origin].LowErrId = (*iter).errorId;
            }
            else if((*iter).errorId < m_ErrFlags[ParamLog.Origin].LowErrId) {
                m_ErrFlags[ParamLog.Origin].LowErrId = (*iter).errorId;
            }
        }
        ++iter;
    } // end while iterator != data.end
    res += m_TelItf->setError(m_ErrFlags[ParamLog.Origin].LowErrId);
    res += setErrFlagId();
    return res;
}

Result ErrorLogger::setErrFlagId() {
    Result res, OkLog;
    SErrFlags FinalFlags;
    std::map<int, SErrFlags>::iterator iterMap = m_ErrFlags.begin();

    while(iterMap != m_ErrFlags.end()) { // while there are errors
        FinalFlags.Flags |= iterMap->second.Flags;
        if(iterMap->second.LowErrId != 0) { // if there is an error ID
            if(FinalFlags.LowErrId == 0) {
                FinalFlags.LowErrId = iterMap->second.LowErrId;
            }
            else {
                if(iterMap->second.LowErrId < FinalFlags.LowErrId) {
                    FinalFlags.LowErrId = iterMap->second.LowErrId;
                }
            }
        }
        iterMap++;
    }
    return res;
}
