/**
 * \file	Result.h
 * \date	03.02.2016
 * \author	mem, brr
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 */

#include "Result.h"

#include <iostream>
#include <list>
#include <sstream>
#include <string>

Result::Result(): m_currentStatus(0), m_resultData() {
}

Result::Result(const Result& res) {
    *this += res;
}

Result::~Result() {
    clearAll();
}

void Result::setResult(const SResultData& res) {
    m_resultData.push_back(res);
}

void Result::setResult(
    const int id,
    const std::string& msg,
    const std::string file /*= ""*/,
    const std::string func /*= ""*/,
    const int line /*= 0*/) {
    if(m_currentStatus == 0 && id != 0) {
        m_currentStatus = 1; // set default value for common failure status
    }
    else if(id == 0) {
        m_currentStatus =
            0; // reset status if an intended "no error = id 0" is set
    }

    printResult(); // Push the error message to the terminal

    SResultData res(m_currentStatus, id, createMsg(file, func, line, msg));
    m_resultData.push_back(res);
}

void Result::setResultIfFailed(
    const int id,
    const std::string& msg,
    const std::string file /*= ""*/,
    const std::string func /*= ""*/,
    const int line /*= 0*/) {
    if(m_currentStatus != 0) {
        SResultData res(m_currentStatus, id, createMsg(file, func, line, msg));
        m_resultData.push_back(res);
    }
}

Result::SResultData& Result::getResult() {
    return m_resultData.back();
}

std::string& Result::getMsg() {
    return m_resultData.back().resultMsg;
}

int Result::getErrorId() const {
    return m_resultData.back().errorId;
}

int Result::getStatus() const {
    return m_currentStatus;
}

void Result::printResult() {
    SResultData res = getResult();
    print(res);
}

void Result::printAllResults() const {
    std::list<SResultData>::const_iterator iter;
    for(iter = m_resultData.begin(); iter != m_resultData.end(); ++iter) {
        SResultData res = *iter;
        print(res);
    }
}

std::list<Result::SResultData> Result::getAllResults() const {
    return m_resultData;
}

Result::operator int() const {
    return getStatus();
}

Result& Result::operator=(const Result& rhs) {
    if(this == &rhs) {
        return *this;
    }
    clearAll();
    m_currentStatus = rhs.m_currentStatus;
    m_resultData    = rhs.getAllResults();
    return *this;
}

Result& Result::operator=(const int& status) {
    m_currentStatus = status;
    return *this;
}

Result& Result::operator+=(const Result& rhs) {
    // do not append any information of a successful rhs
    if(this == &rhs || rhs == 0) {
        return *this;
    }
    std::list<SResultData> rhsData = rhs.getAllResults();
    m_resultData.insert(m_resultData.end(), rhsData.begin(), rhsData.end());
    m_currentStatus = rhs.m_currentStatus;
    return *this;
}

int Result::getSize() {
    return m_resultData.size();
}

void Result::reset() {
    clearAll();
}

void Result::clearAll() {
    m_currentStatus = 0;
    m_resultData.clear();
}

void Result::print(const SResultData& res) const {
    std::cout << "Error-ID " << res.errorId << ": " << res.resultMsg
              << std::endl;
}

std::string Result::createMsg(
    const std::string file,
    const std::string func,
    const int line,
    const std::string& msg) {
    std::stringstream ss;
    if(file != "") {
        ss << " " << file;
    }
    if(func != "" && file != "") {
        ss << "/" << func;
    }
    if(line > 0 && func != "" && file != "") {
        ss << "/" << line << "   ";
    }
    ss << msg;
    return ss.str();
}


std::string FFLtoStr(
    const std::string msg,
    const std::string file,
    const std::string function,
    const unsigned int line) {
    return "Error: " + msg + "; File: " + file + " Func:" + function +
           " Line: " + std::to_string(line);
}