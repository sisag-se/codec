/** *
 * \file	CorAccess.cpp
 * \date	13.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief 	Implementation of the CorAccess class
 */

#include "CorAccess.h"

#include <iostream>
#include <queue>

#include "ErrorId.h"

CorAccess::CorAccess()
    : MemAccess(COR_DEVICE),
      m_CorData(),
      m_currentInterrupt(),
      m_lastInterrupt(),
      m_detectedInterrupts() {
}

CorAccess::~CorAccess() {
}

Cor* CorAccess::getCorReadAccess(Result& res) {
    res = updateCorImage(); // update cor image before reading
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "updateCorImage failed",
        __FILE__,
        __func__,
        __LINE__);
    return &m_CorData;
}

Cor* CorAccess::getCorWriteAccess(Result& res) {
    res = this->openMem(0); // open virtual memory page
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "CorWriteAccess failed",
        __FILE__,
        __func__,
        __LINE__);
    if(res == 0) {
        Cor* cor =
            (Cor*) m_PBaseMap; // points to the virtual address of CSR memory
        return cor;
    }
    return NULL;
}

Result CorAccess::quitCorWriteAccess() {
    Result res = this->closeMem(); // close virtual memory page
    return res;
}

Result CorAccess::updateCorImage() {
    Result res = this->openMem(0); // open virtual memory page
    if(res == 0) {
        Cor* cor =
            (Cor*) m_PBaseMap; // points to the virtual address of CSR memory
        m_CorData = *cor;
    }
    res = this->closeMem(); // close virtual memory page
    return res;
}

Result CorAccess::checkInterrupts() {
    SInterrupt detectedInterrupt;
    Result res; // Result with the error messages

    updateCorImage();       // update Cor before checking the interrupts
    res = this->openMem(0); // open virtual memory page
    if(res == 0) {
        Cor* cor =
            (Cor*) m_PBaseMap; // points to the virtual address of Cor memory
        m_currentInterrupt.InRdy =
            cor->name.TelInControll.InRdy; // read new samples ready flag
        m_currentInterrupt.RxRdy =
            cor->name.CodRxCtl.InRdy; // read new Rx code ready flag
    }
    res = this->closeMem(); // close virtual memory page
    if(evaluateInterrupts(detectedInterrupt) > 0) {
        m_detectedInterrupts.push(detectedInterrupt);
    }
    return res;
}

SInterrupt CorAccess::getInterrupts() {
    SInterrupt detectedInterrupt = {0, 0};

    if(m_detectedInterrupts.size() > 0) {
        detectedInterrupt = m_detectedInterrupts.front();
    }
    return detectedInterrupt;
}

void CorAccess::deleteOldestInterrupt(SInterrupt executedInterrupt) {
    if(executedInterrupt.InRdy == 1) {
        m_detectedInterrupts.front().InRdy = 0;
    }
    if(executedInterrupt.RxRdy == 1) {
        m_detectedInterrupts.front().RxRdy = 0;
    }
    if((m_detectedInterrupts.front().InRdy == 0) &&
       (m_detectedInterrupts.front().RxRdy == 0) &&
       (m_detectedInterrupts.size() > 0)) {
        m_detectedInterrupts.pop();
    }
}

void CorAccess::deleteAllInterrupts() {
    SInterrupt emptyIrpt = {0, 0};
    m_currentInterrupt   = emptyIrpt;
    m_lastInterrupt      = emptyIrpt;

    while(!m_detectedInterrupts.empty()) {
        m_detectedInterrupts.pop();
    }
}

int CorAccess::evaluateInterrupts(SInterrupt& detectedInterrupt) {
    int InterruptCounter = 0;

    if(m_lastInterrupt.InRdy == 0 && m_currentInterrupt.InRdy > 0) {
        detectedInterrupt.InRdy = 1; // new Samples interrupt detected
        ++InterruptCounter;
    }
    else {
        detectedInterrupt.InRdy = 0;
    }

    if(m_lastInterrupt.RxRdy == 0 && m_currentInterrupt.RxRdy > 0) {
        detectedInterrupt.RxRdy = 1; // new Code interrupt detected
        ++InterruptCounter;
    }
    else {
        detectedInterrupt.RxRdy = 0;
    }

    m_lastInterrupt = m_currentInterrupt;
    return InterruptCounter;
}
