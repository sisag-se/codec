/** *
 * \file	DtmfMap.cpp
 * \date	08.02.2021
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief	implementation of the dtmfMap class
 */

#include "DtmfMap.h"

#include <iostream>
#include <thread>
#include <vector>

#include "CorAccess.h"
#include "ErrorId.h"

#define DTMF_SPACER_NUM 69

DtmfMap::DtmfMap(CorAccess* pCor): ICorDtmf(), m_pCor(pCor) {
}

DtmfMap::~DtmfMap() {
}

void DtmfMap::sendDtmf(SDtmf dtmf) {
    SDtmf spacer;
    spacer.Num = DTMF_SPACER_NUM; // The Spacer is an invalid number that tells
                                  // the sender to pause
    spacer.isLocal = dtmf.isLocal;
    spacer.Det     = dtmf.Det;
    dtmfQueue.emplace(dtmf);
    dtmfQueue.emplace(spacer);

    if(!dtmfTimer.running()) {
        dtmfTimer.setInterval(
            std::bind(&DtmfMap::sendDtmfLiterally, this), DTMF_SEND_TIME, true);
    }
}

void DtmfMap::sendDtmf(std::vector<SDtmf> dtmf) {
    for(unsigned int i = 0; i < dtmf.size(); i++) {
        sendDtmf(dtmf.at(i));
    }
}

Result DtmfMap::copyDtmf(EInterface iface) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image
    SDtmf tempDtmfSend;
    tempDtmfSend.Det = 0;
    tempDtmfSend.Num = 0;
    SDtmf tempDtmfReceive;
    tempDtmfReceive.Det = 0;
    tempDtmfReceive.Num = 0;

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        tempDtmfSend.Det = cor->name.TelInState.LocalDtmfDet;
        tempDtmfSend.Num = cor->name.TelInState.LocalDtmfNum;

        tempDtmfReceive.Det = cor->name.CodRxCtl.DtmfValid;
        tempDtmfReceive.Num = cor->name.CodRxCtl.DtmfNum;
    }

    cor = m_pCor->getCorWriteAccess(res);
    if(cor && (res == 0)) {
        if(iface == eDigital) {
            cor->name.CodTxState.DtmfValid = tempDtmfSend.Det;
            cor->name.CodTxState.DtmfNum   = tempDtmfSend.Num;

            cor->name.TelOutControll.DtmfSend = tempDtmfReceive.Det;
            cor->name.TelOutControll.DtmfNum  = tempDtmfReceive.Num;
        }
        else if(iface == eWeb) {
            // Webserver Display
            cor->name.CodWebDtmf.LocalValid = tempDtmfSend.Det;
            cor->name.CodWebDtmf.LocalNum   = tempDtmfSend.Num;

            cor->name.CodWebDtmf.RemoteValid = tempDtmfReceive.Det;
            cor->name.CodWebDtmf.RemoteNum   = tempDtmfReceive.Num;
        }
    }
    res += m_pCor->quitCorWriteAccess();
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Sending a DTMF failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result DtmfMap::receiveLocalDtmf(SDtmf& dtmf) {
    Result res;          // Result with Error message
    Cor* cor     = NULL; // Cor image
    bool hasDtmf = false;
    res += hasLocalDtmf(hasDtmf);

    // Check if a DTMF is present and its reading had no error
    if(!hasDtmf || res.getStatus() != 0) {
        dtmf.Det = false;
        return res;
    }

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        dtmf.Det     = cor->name.TelInState.LocalDtmfDet;
        dtmf.Num     = cor->name.TelInState.LocalDtmfNum;
        dtmf.isLocal = true;
    }
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "Failed determining the remote DTMF",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result DtmfMap::receiveRemoteDtmf(SDtmf& dtmf) {
    Result res;          // Result with Error message
    Cor* cor     = NULL; // Cor image
    bool hasDtmf = false;
    res += hasRemoteDtmf(hasDtmf);

    // Check if a DTMF is present and its reading had no error
    if(!hasDtmf || res != 0) {
        dtmf.Det = false;
        return res;
    }

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        dtmf.Det     = true;
        dtmf.Num     = cor->name.CodRxCtl.DtmfNum;
        dtmf.isLocal = false;
    }
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "Failed determining the remote DTMF",
        __FILE__,
        __func__,
        __LINE__);

    // if(dtmf.Num != 0) std::cout << dtmf.Num << std::endl;
    return res;
}

Result DtmfMap::hasLocalDtmf(bool& hasDtmf) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        hasDtmf = (bool) cor->name.TelInState.LocalDtmfDet;
    }
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "Checking wheter a local DTMF is present failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result DtmfMap::hasRemoteDtmf(bool& hasDtmf) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        hasDtmf = (bool) cor->name.CodRxCtl.DtmfValid;
    }
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "Checking wheter a remote DTMF is present failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}


void DtmfMap::sendDtmfLiterally() {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    if(dtmfQueue.size() > 0) {
        SDtmf dtmf = dtmfQueue.front();

        cor = m_pCor->getCorWriteAccess(res);

        if(cor && (res == 0)) {
            // Send a spacer when needed
            if(dtmf.Num == DTMF_SPACER_NUM) {
                dtmf.Num = 0;
                dtmf.Det = false;
            }
            else {
                dtmf.Det = true;
            }

            // Send out the DTMF either local or remote
            if(dtmf.isLocal) {
                cor->name.TelOutControll.DtmfSend = dtmf.Det;
                cor->name.TelOutControll.DtmfNum  = dtmf.Num;
            }
            else {
                cor->name.CodTxState.DtmfValid = dtmf.Det;
                cor->name.CodTxState.DtmfNum   = dtmf.Num;
            }
        }
        res += m_pCor->quitCorWriteAccess();
        res.setResultIfFailed(
            ERR_COR_WRITE_ACCESS,
            "Sending a DTMF failed",
            __FILE__,
            __func__,
            __LINE__);

        std::cout << "Sending DTMF: " << dtmf.Num << std::endl;

        if(res == 0) {
            dtmfQueue.pop();
        }
    }
    else {
        dtmfTimer.stop();

        // Stop sending the last dtmf
        cor = m_pCor->getCorWriteAccess(res);
        if(cor && (res == 0)) {
            // local
            cor->name.TelOutControll.DtmfSend = false;
            cor->name.TelOutControll.DtmfNum  = 0;

            // remote
            cor->name.CodTxState.DtmfValid = false;
            cor->name.CodTxState.DtmfNum   = 0;
        }
        res += m_pCor->quitCorWriteAccess();
        res.setResultIfFailed(
            ERR_COR_WRITE_ACCESS,
            "Stop sending a DTMF failed",
            __FILE__,
            __func__,
            __LINE__);
    }

    // This method cant return a result cause std::function
    // cant have a class as a return parameter as it creates an instance of it.
    // Bruh! return res;
}