/** *
 * \file	MemAccess.cpp
 * \date	11.4.2016
 * \author	bem, mad
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	2.0
 *
 * \brief implementation of the MemAccess class
 */

#include "MemAccess.h"

#include <fcntl.h>
#include <stddef.h>
#include <sys/mman.h>
#include <unistd.h>

#include <vector>

#include "ErrorId.h"

MemAccess::MemAccess(const char *device)
    : m_PBaseMap(0), m_Fd(0), m_PDevice(device), m_BaseAddr(0) {
    /* create the mutex */
    pthread_mutex_init(&m_mutex, NULL);
}

MemAccess::~MemAccess() {
    /* destroy the mutex */
    pthread_mutex_destroy(&m_mutex);
}

// open Linux Memory Device
Result MemAccess::openMem(const off_t baseAddr) {
    Result result; // Result with Error message

    pthread_mutex_lock(
        &m_mutex); // locks the memory access until device will be closed
    // open Device :
    if((m_Fd = open(m_PDevice, O_RDWR | O_SYNC)) == -1) {
        result.setResult(
            ERR_COR_ACCESS,
            "Error: Device kann nicht geoeffnet werden",
            __FILE__,
            __func__,
            __LINE__);
    }
    // open Memory-Page:
    else {
        void *baseMap = mmap(
            NULL,
            PAGE_SIZE,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            m_Fd,
            baseAddr & ~(PAGE_SIZE - 1));
        if(baseMap == (void *) -1) {
            result.setResult(
                ERR_COR_ACCESS,
                "Error: Memory map kann nicht geoeffnet werden",
                __FILE__,
                __func__,
                __LINE__);
            close(m_Fd); // close Filehandler
        }
        m_PBaseMap = (unsigned int *) baseMap;
    }
    return result;
}

Result MemAccess::writeMem(unsigned int offsetAddr, int value) {
    Result result; // Result with Error message

    if(offsetAddr < PAGE_SIZE) {
        volatile unsigned int *virt_addr; // virtual address
        virt_addr =
            (unsigned int
                 *) (m_PBaseMap + offsetAddr); // generate virtual address
        *virt_addr = value;                    // write to address
    }
    else {
        result.setResult(
            ERR_COR_WRITE_ACCESS,
            "Error: offsetAddr  ist groesser als PAGE_SIZE-1",
            __FILE__,
            __func__,
            __LINE__);
    }
    return result;
}

Result MemAccess::writeMem(unsigned int offsetAddr, std::vector<int> &values) {
    Result result; // Result with Error message

    if(offsetAddr + values.size() < PAGE_SIZE) {
        volatile unsigned int *virt_addr; // virtual Address
        virt_addr =
            (unsigned int
                 *) (m_PBaseMap + offsetAddr); // generate virtual Address
        for(std::vector<int>::size_type i = 0; i != values.size(); i++) {
            *virt_addr = values[i]; // write to Address
            virt_addr++;
        }
    }
    else {
        result.setResult(
            ERR_COR_WRITE_ACCESS,
            "Error: offsetAddr + values.size() is greater than the PAGE_SIZE-1",
            __FILE__,
            __func__,
            __LINE__);
    }
    return result;
}

Result MemAccess::readMem(unsigned int offsetAddr, int value) {
    Result result; // Result with Error
    if(offsetAddr < PAGE_SIZE) {
        volatile unsigned int *virt_addr; // virtual Address
        virt_addr =
            (unsigned int
                 *) (m_PBaseMap + offsetAddr); // generate virtual Address
        value = *virt_addr;                    // write to Address
    }
    else {
        result.setResult(
            ERR_COR_READ_ACCESS,
            "Error: offsetAddr is greater than the PAGE_SIZE-1",
            __FILE__,
            __func__,
            __LINE__);
    }
    return result;
}

Result MemAccess::readMem(unsigned int offsetAddr, std::vector<int> &values) {
    Result result; // Result with Error
    if(offsetAddr + values.size() < PAGE_SIZE) {
        volatile unsigned int *virt_addr; // virtual Address
        virt_addr =
            (unsigned int
                 *) (m_PBaseMap + offsetAddr); // generate virtual Address
        for(std::vector<int>::size_type i = 0; i != values.size(); i++) {
            values[i] = *virt_addr; // write to Address
            virt_addr++;
        }
    }
    else {
        result.setResult(
            ERR_COR_READ_ACCESS,
            "Error: offsetAddr + values.size() is greater than the PAGE_SIZE-1",
            __FILE__,
            __func__,
            __LINE__);
    }
    return result;
}

Result MemAccess::closeMem() {
    Result result; // Result with Error
    if(munmap(m_PBaseMap, PAGE_SIZE) == -1) {
        result.setResult(
            ERR_COR_ACCESS,
            "Error: Memory map can not be closed",
            __FILE__,
            __func__,
            __LINE__);
    }
    close(m_Fd); // close Filehandler

    pthread_mutex_unlock(&m_mutex); // device is now free for other clients
    return result;
}
