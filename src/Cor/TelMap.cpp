/** *
 * \file	TelMap.cpp
 * \date	13.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief	implementation of the telMap class
 */

#include "TelMap.h"

#include <algorithm>
#include <iostream>
#include <vector>

#include "CorAccess.h"
#include "ErrorId.h"

TelMap::TelMap(CorAccess* pCor): ICorTel(), m_pCor(pCor) {
}

TelMap::~TelMap() {
}

Result TelMap::getSamplesIn(std::vector<int>& data) {
    Result res;                     // Result with Error message
    unsigned int sizeOfData = 0;    // Number of samples in
    Cor* cor                = NULL; // Cor image

    data.clear();
    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        // read samples in if cor access was successful
        sizeOfData = cor->name.TelInControll.NumInData;
        data.assign(cor->name.TelInData, cor->name.TelInData + sizeOfData);
    }
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "Cor read samplesIn failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::setSamplesOut(std::vector<int>& data) {
    Result res;                            // Result with Error message
    Cor* cor                = NULL;        // Cor image
    unsigned int sizeOfData = data.size(); // Number of samples out

    cor = m_pCor->getCorWriteAccess(res);

    if(sizeOfData <=
       TEL_DATA_SIZE) { // check size of input data to prevent overflow
        if(cor && (res == 0)) {
            cor->name.TelOutControll.NumOutData = sizeOfData;
            std::copy(data.begin(), data.end(), cor->name.TelOutData);
            cor->name.TelOutControll.OutRdy = true;
        }
    }
    else {
        res.setResult(
            ERR_DATA_SIZE, "To much Samples Out", __FILE__, __func__, __LINE__);
    }
    res += m_pCor->quitCorWriteAccess();

    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Cor write SamplesOut failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::getState(ECorCtl CorId, SState& State) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        switch(CorId) {
            case eSamplesIn:
                State.Comm =
                    static_cast<ETelCommState>(cor->name.TelInState.Comm);
                // State.Dtmf.Det 	= cor->name.TelInState.LocalDtmfDet;
                // State.Dtmf.Num	= cor->name.TelInState.LocalDtmfNum;
                break;

            case eCodeTX:
                State.Comm =
                    static_cast<ETelCommState>(cor->name.CodTxState.Comm);
                // State.Dtmf.Det 	= cor->name.CodTxState.DtmfValid;
                // State.Dtmf.Num	= cor->name.CodTxState.DtmfNum;
                break;

            default:
                res.setResult(
                    ERR_COR_IDENTIFIER,
                    "invalid State identifier",
                    __FILE__,
                    __func__,
                    __LINE__);
                break;
        }
    }
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "Cor read State failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::setState(ECorCtl CorId, SState& State) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorWriteAccess(res);

    if(cor && (res == 0)) {
        switch(CorId) {
            case eSamplesIn:
                cor->name.TelInState.Comm = State.Comm;
                // cor->name.TelInState.LocalDtmfDet	= State.Dtmf.Det;
                // cor->name.TelInState.LocalDtmfNum	= State.Dtmf.Num;
                break;

            case eCodeTX:
                cor->name.CodTxState.Comm = State.Comm;
                // cor->name.CodTxState.DtmfValid	= State.Dtmf.Det;
                // cor->name.CodTxState.DtmfNum	= State.Dtmf.Num;
                break;

            default:
                res.setResult(
                    ERR_COR_IDENTIFIER,
                    "invalid State identifier",
                    __FILE__,
                    __func__,
                    __LINE__);
                break;
        }
    }
    res += m_pCor->quitCorWriteAccess();
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Cor write State failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::getCtl(ECorCtl CorId, SCtl& control) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        switch(CorId) {
            case eSamplesIn:
                control.Ready   = cor->name.TelInControll.InRdy;
                control.NumData = cor->name.TelInControll.NumInData;
                break;

            case eSamplesOut:
                control.Ready   = cor->name.TelOutControll.OutRdy;
                control.NumData = cor->name.TelOutControll.NumOutData;
                break;

            case eCodeRX:
                control.Ready   = cor->name.CodRxCtl.InRdy;
                control.NumData = cor->name.CodRxCtl.NumInData;
                // control.Dtmf.Det 	= cor->name.CodRxCtl.DtmfValid;
                // control.Dtmf.Num	= cor->name.CodRxCtl.DtmfNum;
                break;

            case eCodeTX:
                control.Ready   = cor->name.CodTxCtl.OutRdy;
                control.NumData = cor->name.CodTxCtl.NumOutData;
                break;

            default:
                res.setResult(
                    ERR_COR_IDENTIFIER,
                    "invalid Ctl (Control) identifier",
                    __FILE__,
                    __func__,
                    __LINE__);
                break;
        }
    }
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "Cor read Ctl (Control) failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::setCtl(ECorCtl CorId, SCtl& control) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorWriteAccess(res);

    if(cor && (res == 0)) {
        switch(CorId) {
            case eSamplesIn:
                cor->name.TelInControll.InRdy     = control.Ready;
                cor->name.TelInControll.NumInData = control.NumData;
                break;

            case eSamplesOut:
                cor->name.TelOutControll.OutRdy     = control.Ready;
                cor->name.TelOutControll.NumOutData = control.NumData;
                // cor->name.TelOutControll.DtmfSend	= control.Dtmf.Det;
                // cor->name.TelOutControll.DtmfNum	= control.Dtmf.Num;
                break;

            case eCodeRX:
                cor->name.CodRxCtl.InRdy     = control.Ready;
                cor->name.CodRxCtl.NumInData = control.NumData;
                cor->name.CodRxCtl.DtmfValid = control.Dtmf.Det;
                cor->name.CodRxCtl.DtmfNum   = control.Dtmf.Num;
                break;

            case eCodeTX:
                cor->name.CodTxCtl.OutRdy     = control.Ready;
                cor->name.CodTxCtl.NumOutData = control.NumData;
                break;

            default:
                res.setResult(
                    ERR_COR_IDENTIFIER,
                    "invalid Ctl (Control) identifier",
                    __FILE__,
                    __func__,
                    __LINE__);
                break;
        }
    }
    res += m_pCor->quitCorWriteAccess();
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Cor write Ctl (Control) failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::getTelConfig(STelConfig& config) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        config.Echo          = cor->name.TelConfig.Echo;
        config.Source        = cor->name.TelConfig.Source;
        config.Processing    = cor->name.TelConfig.Processing;
        config.BytesPerFrame = cor->name.TelConfig.BytesPerFrame;
        config.LocalAdjust   = cor->name.TelConfig.LocalAdjust;
        config.RemoteGain    = cor->name.TelConfig.RemoteGain;
        config.MinLevel      = cor->name.TelConfig.MinLevel;
    }
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "Cor read TelConfig failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::getTelInRssi(STel& Rssi) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image
    cor      = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        Rssi.RssiLocal  = cor->name.TelInRssi.RssiLocal;
        Rssi.RssiRemote = cor->name.TelInRssi.RssiRemote;
    }
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "Cor read TelRSSI failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::setTelTxRssi(STel& Rssi) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image
    cor      = m_pCor->getCorWriteAccess(res);

    if(cor && (res == 0)) {
        cor->name.CodTxRssi.RssiLocal  = Rssi.RssiLocal;
    }
    res += m_pCor->quitCorWriteAccess();
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "Cor write TelRSSI failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::getCodeRx(std::vector<int>& data) {
    Result res;                     // Result with Error message
    Cor* cor                = NULL; // Cor image
    unsigned int sizeOfData = 0;    // Size of code Rx

    data.clear();
    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        sizeOfData = cor->name.CodRxCtl.NumInData;
        data.assign(cor->name.CodecRxData, cor->name.CodecRxData + sizeOfData);
    }
    res.setResultIfFailed(
        ERR_COR_READ_ACCESS,
        "Cor read CodeRX failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::setCodeTx(std::vector<int>& data) {
    Result res;                            // Result with Error message
    Cor* cor                = NULL;        // Cor image
    unsigned int sizeOfData = data.size(); // Size of code Tx

    cor = m_pCor->getCorWriteAccess(res);

    if(sizeOfData <=
       CODE_DATA_SIZE) { // check size of input data to prevent overflow
        if(cor && (res == 0)) {
            cor->name.CodTxCtl.NumOutData = sizeOfData;
            std::copy(data.begin(), data.end(), cor->name.CodecTxData);
            cor->name.CodTxCtl.OutRdy = true; // set Code Out ready
        }
    }
    else {
        res.setResult(
            ERR_DATA_SIZE, "To much TXCode", __FILE__, __func__, __LINE__);
    }
    res += m_pCor->quitCorWriteAccess();
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Cor write CodeTX failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::setCodeRxAck(bool Ack) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorWriteAccess(res);
    if(cor && (res == 0)) {
        cor->name.CodRxState.Ack = Ack;
    }

    res += m_pCor->quitCorWriteAccess();
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "set Code RX acknowledge failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::setSamplesInAck(bool Ack) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorWriteAccess(res);
    if(cor && (res == 0)) {
        cor->name.TelInState.Ack = Ack;
    }
    res += m_pCor->quitCorWriteAccess();
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "set Samples In acknowledge failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::setError(int ErrorID) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorWriteAccess(res);

    if(cor && (res == 0)) {
        cor->name.CodTxState.CodecError = ErrorID;
    }
    res += m_pCor->quitCorWriteAccess();
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Cor write SamplesOut failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}


Result TelMap::getWebRssi(STel& webRssi) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        webRssi.RssiLocal  = cor->name.TelInWebRssi.RssiLocal;
        webRssi.RssiRemote = cor->name.CodRxRssi.RssiRemote;
    }
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Cor get Web Rssi failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::setWebRssi(STel& webRssi){
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorWriteAccess(res);
    if(cor && (res == 0)) {
        cor->name.CodTxRssi.WebRssiLocal = webRssi.RssiLocal;
        cor->name.CodTxRssi.WebRssiRemote = webRssi.RssiRemote;
    }
    res += m_pCor->quitCorWriteAccess();
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Cor set Web Rssi failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::getSection(unsigned int& section) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        section = cor->name.TelConfig.Section;
    }
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Cor get section failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::getStation(EStation& station) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        station = static_cast<EStation>(cor->name.TelConfig.Station);
    }
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Cor get station failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::getInterface(EInterface& iface) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorReadAccess(res);

    if(cor && (res == 0)) {
        iface = static_cast<EInterface>(cor->name.TelConfInterface.Interface);
    }
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Cor get interface failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}

Result TelMap::setInterface(EInterface& iface) {
    Result res;      // Result with Error message
    Cor* cor = NULL; // Cor image

    cor = m_pCor->getCorWriteAccess(res);

    if(cor && (res == 0)) {
        cor->name.TelConfInterface.Interface = iface;
    }
    res += m_pCor->quitCorWriteAccess();
    res.setResultIfFailed(
        ERR_COR_WRITE_ACCESS,
        "Cor set interface failed",
        __FILE__,
        __func__,
        __LINE__);
    return res;
}