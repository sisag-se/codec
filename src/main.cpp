/**
 * \file	main.cpp
 * \date	06.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief Main program for the Codec Task
 */

// External
#include <pthread.h>
#include <unistd.h>

#include <cstdlib>
#include <iostream>
#include <unordered_set>

// Misc
#include "ErrorLogger.h"
#include "Result.h"
#include "Sync.h"

// Codec
#include "Decoder.h"
#include "Encoder.h"

// Interfaces
#include "CorAccess.h"
#include "CorMapping.h"
#include "DtmfMap.h"
#include "TelMap.h"

// SIP
#include "AudioRouter.h"
#include "CallRouter.h"
#include "ConfigParser.h"
#include "SisComSip.h"

#define CONF_CHECK_TIME 1000

CorAccess corAccess;                 /**< COR-Memory access instance */
const unsigned int cIdleTime = 1000; /**< idle time in cycle [us] */
ECallState callState         = eInactive;

/**
 * \brief RSSI and other data needs to be copied every Protocol
 */
Result copyData();

/**
 * \brief Invokes the corresponding function if interrupt flag is set.
 * \return Result with error messages.
 */
Result dispatchInterrupts(CallRouter&);

// Global class instances
TelMap telMap(&corAccess); /**< COR-Memory TelMap access instance */
DtmfMap dtmf(&corAccess);  /**< COR-Memory DTMF instance */
ErrorLogger Log(&telMap);  /**< Error Logger instance*/
AudioRouter Router(&telMap, &callState);     /**< Audio Router instance*/
SisComSip Sip(&Router, &telMap, &callState); /**< SisCom Sip instance*/
Encoder CodeTX(&telMap, &Router, &Sip);      /**< Encoder instance*/
Decoder CodeRX(&telMap, &Router);            /**< Decoder instance*/
PeakFilter pkFilter;

/* Start of the main */
int main(void) {
    /* Set and test the policy and the priority of the main thread */
    int prio = 0;
    int pol  = 0;
    ErrorLogger::SLog ErrorLog;
    SisThread::getScheduleAndPrio(pthread_self(), pol, prio);
    SisThread::setScheduleAndPrio(pthread_self(), SCHED_RR, 50);
    SisThread::getScheduleAndPrio(pthread_self(), pol, prio);

    Result res; /**< Result for Error messages */
    Sync sync;  /**< Sync Parameter for threads */

    EStation station = eUndefined;
    telMap.getStation(station);

    // Set the audio routing based on the config
    ConfigParser conf(station);
    Router.setRoute(conf.getRoute(0));
    Router.setRoute(conf.getRoute(1));

    CallRouter cr(&dtmf, &conf, &Sip);            /**< CallRouter instance*/

    // Dont start the SIP class when its not needed
    if(conf.isUsingIPCalls()) {
        EInterface iface = eDigital;
        telMap.setInterface(iface);
        SState newState;
        newState.Comm = eNoSipComm;
        telMap.setState(eCodeTX, newState);

        res = Sip.SisThread::start(SCHED_RR, 50);
        if(res != 0) { // if there is an error do the error log
            res.setResult(
                ERR_CODEC,
                "Error: unable to create Sip thread, ",
                __FILE__,
                __func__,
                __LINE__);
            sync.reset();
            ErrorLog.Origin = SisThread::eMain;
            ErrorLog.res    = &res;
            Log.ErrorLog(ErrorLog);
            exit(-1);
        }
        Sip.init(
            conf.getConf(),
            std::bind(&CallRouter::sendHeader, &cr, std::placeholders::_1),
            std::bind(&CallRouter::sendAck, &cr),
            std::bind(&CallRouter::sendNack, &cr));

        Sip.registerNumbers(conf.getNumbersToRegister());
    }
    else {
        EInterface iface = eAnalog;
        telMap.setInterface(iface);
    }

    /* Start the Encoder thread */
    /* set the policy and the priority of the encoder thread */
    res = CodeTX.SisThread::start(SCHED_RR, 40);
    if(res != 0) { // if there is an error do the error log
        res.setResult(
            ERR_CODEC,
            "Error: unable to create Encoder thread, ",
            __FILE__,
            __func__,
            __LINE__);
        sync.reset();
        ErrorLog.Origin = SisThread::eMain;
        ErrorLog.res    = &res;
        Log.ErrorLog(ErrorLog);
        exit(-1);
    }
    /* initialize the Encoder */
    CodeTX.init();


    /* Start Decoder thread */
    /* set the policy and the priority of the decoder thread */
    res = CodeRX.SisThread::start(SCHED_RR, 45);
    if(res != 0) { // if there is an error do the error log
        res.setResult(
            ERR_CODEC,
            "Error: unable to create Decoder thread, ",
            __FILE__,
            __func__,
            __LINE__);
        sync.reset();
        ErrorLog.Origin = SisThread::eMain;
        ErrorLog.res    = &res;
        Log.ErrorLog(ErrorLog);
        exit(-1);
    }
    /* initialize the Decoder */
    CodeRX.init();

    /* update the cor data first */
    res += corAccess.updateCorImage();

    /* delete all current Interrupts first */
    corAccess.deleteAllInterrupts();

    /* Main Loop */
    while(1) {
        /* 1. Check interrupts */
        res += corAccess.checkInterrupts();

        /* 2. Invoke requested commands */
        res += dispatchInterrupts(cr);

        /* 3. when there are error results, then do the error Log */
        ErrorLog.Origin = SisThread::eMain;
        ErrorLog.res    = &res;
        Log.ErrorLog(ErrorLog);
        res.reset();

        /* 3. sleep for cIdleTime us before running the main loop again */
        usleep(cIdleTime);
    } // end of the main loop

    return 0; // end of main should never be reached
}

Result dispatchInterrupts(CallRouter &cr) {
    Result res; // result with error messages
    Sync sync;  // sync parameter for pthread
    // keep track of the executed interrupts
    SInterrupt executedInterrupts = {0, 0};

    // Only copy DTMF's if PC316 is used and only get the SIP audio level if SIP
    // is used
    if(Router.getRoute(eSourceSIP) == eNoSink) {
        dtmf.copyDtmf(eDigital);
    }
    else {
        cr.poll();
    }

    // Copy any dtmf's for them to be presented on the Webread
    dtmf.copyDtmf(eWeb);
    copyData();

    /* if there are new input samples */
    if(corAccess.getInterrupts().InRdy) {
        // set encoding request
        CodeTX.Encode(&sync, &res);
        sync.wait();
        executedInterrupts.InRdy = 1;
    }
    else if(
        Router.isSipFrameReady() && Router.getRoute(eSourceSIP) != eNoSink) {
        // set encoding request
        CodeTX.Encode(&sync, &res);
        sync.wait();
        Router.setIsSipFrameReady(false);
    }

    // if there are new coded samples received
    if(corAccess.getInterrupts().RxRdy) {
        // set decoding request
        CodeRX.Decode(&sync, &res);
        sync.wait();
        executedInterrupts.RxRdy = 1;
    }

    // delete all Executed interrupts
    corAccess.deleteOldestInterrupt(executedInterrupts);
    return res;
}

Result copyData() {
    Result res; // Result with error message
    SState tempStateSend;
    STel rssi;
    STel webRssi;

    // copy tel Rssi
    if(Router.getRoute(eSourceSIP) == eNoSink) {
        // copy State
        res += telMap.getState(eSamplesIn, tempStateSend);
        res += telMap.setState(eCodeTX, tempStateSend);

        res += telMap.getTelInRssi(rssi);
        res += telMap.setTelTxRssi(rssi);

        res += telMap.getWebRssi(webRssi);
        res += telMap.setWebRssi(webRssi);
    }
    else {
        unsigned int level = Sip.getAudioLevel();
        level              = pkFilter.filter(level);

        res += telMap.getTelInRssi(rssi);
        res += telMap.getWebRssi(webRssi);
        rssi.RssiLocal = level;
        webRssi.RssiLocal = level;
        if(Sip.getCallState() == eInactive){
            webRssi.RssiRemote = 0;
        }
        res += telMap.setTelTxRssi(rssi);
        res += telMap.setWebRssi(webRssi);
    }

    return res;
}
