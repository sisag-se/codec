/**
 * \file	ConfTypeConverter.h
 * \date	04.05.2021
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief This File assists in the conversion of config
 *        Text to enum values and back.
 */

#pragma once

#include "TelStruct.h"

/**
 * \brief	Takes a station string from the config and returns its enum
 *          equivalent 
 * \param[in]	The string from the config to get the enum of
 * \return  The enum equvalent of the given string
 */
EStation configTextToStation(const std::string conf);

/**
 * \brief	Returns the enum equivalent of a sink string from the config
 * \param[in]	The string from the config to get the enum of
 * \return  The enum equvalent of the given string
 */
ESink configTextToSink(const std::string);

/**
 * \brief	Returns the enum equivalent of a source string from the config 
 * \param[in]	The string from the config to get the enum of
 * \return  The enum equvalent of the given string
 */
ESource configTextToSource(const std::string);