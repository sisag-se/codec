/**
 * \file	configParser.h
 * \date	20.11.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief This Class is for reading and parsing a JSON file.
 *        This JSON file defines all Call numbers and adresses.
 *        Including the PBX's address, and password.
 *        The SipConf.h file is therefore not needed anymore, but is left for
 * testing
 */

#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>

#include "TelStruct.h"
#include "json.hpp"

using json = nlohmann::json;

// Where the sip config is located in relation to the codec binary
#define CONF_LOC "/root/ConfigFiles/sip_config.json"

/**
 * \brief 	Struct for a phone number
 */
typedef struct {
    std::string num;  // The phone number in non sip URI or an ip address
    EStation station; // Tells where in the SisCom this number is being used
    ECallType callType;
    // Is 0 when a PBX is present and numbers can be used
    // When non 0, ip adresses are used for calls and this tells which ip it is
    std::string isIPAddress;
    std::string description;
} SPhoneNumber;

/**
 * \brief 	Struct for configuring SisComSip
 */
typedef struct {
    std::string addrPBX;
    std::string passPBX;
    EStation station;
    std::unordered_set<EStation> stationsWithPBX;
    bool isSipLogEnabled;
} SConfSip;


/**
 * \class 	ConfigParser
 * \brief 	Class used to parse a config file for all PBX info and phone numbers
 */
class ConfigParser {
   public:
    /**
     * \brief	Constructor, parses the entire config upon creation
     */
    ConfigParser(EStation);

    /**
     * \brief	Plain destructor...
     */
    ~ConfigParser();

    /**
     * \brief	Method for (re)parsing the config file
     */
    void parse();

    /**
     * \brief	returns the configured PBX address
     */
    std::string getPBXAddress();

    /**
     * \brief	Returns the configured password for authenticating with the PBX
     */
    std::string getPBXPassword();

    /**
     * \brief	Return a list with all phone numbers
     */
    std::vector<SPhoneNumber> getNumbers();

    /**
     * \brief	Return a specific number
     */
    SPhoneNumber getNumber(unsigned int);

    /**
     * \brief	Return a list of numbers of the configured station
     * \return  All numbers that are present on this Station
     */
    std::vector<SPhoneNumber> getOwnNumbers();

    /**
     * \brief	Return a list of numbers of that are not from the current Station
     * \return  A list of numbers to register.
     */
    std::vector<SPhoneNumber> getNumbersToRegister();

    /**
     * \brief	This returns the configured routes
     * \return  A Map with the configured routes
     */
    std::vector<std::pair<ESource, ESink>> getRoutes();

    /**
     * \brief	This returns a configured route
     * \param[in]	Position to return the route of
     * \return  A pair with the configured route
     */
    std::pair<ESource, ESink> getRoute(unsigned int);

    /**
     * \brief	Retun a conf struct to configure SisComSip
     * \param[in]	Position to return the route of
     * \return  A pair with the configured route
     */
    SConfSip getConf();

    /**
     * \brief	Returns true when SIP is confgured, false when PC316
     * \return  true when SIP is confgured, false when PC316
     */
    bool isUsingIPCalls();


   private:
    // Cache all values in memory
    std::vector<SPhoneNumber> m_phoneNumbers;
    std::vector<std::pair<ESource, ESink>> m_confRouteList;
    std::unordered_set<EStation> m_stationsWithPBX;
    std::string m_addrPBX  = "";
    std::string m_passPBX  = "";
    bool m_isSipLogEnabled = false;
    EStation m_station;
};
