/** *
 * \file	CodecCtl.h
 * \date	20.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief 	Controls for the Opus Codec. for more information take a look at
 * this document: Altera SoC - 4 SoftwareDesignFlow.docx
 * path: P:\TE-Projekte\Print\EntwicklungsOrdner2\Altera\SOC-Linux
 */

#ifndef CODECCTL_H_
#define CODECCTL_H_

#include "CorMapping.h"

#define FRAME_SIZE 		8*60					///< size of one Frame of Samples
#define SAMPLE_RATE 	8000					///< Sample rate of the Audio input 8kHz
#define CHANNELS 		1						///< must be one we have only one audio channel NEVER CHANGE THIS SETTING!!!
#define APPLICATION 	OPUS_APPLICATION_VOIP	///< application is coding Audio
#define BITRATE 		6800					///< Bit Rate on witch the Codec is operating

#define MAX_FRAME_SIZE 	TEL_DATA_SIZE	///< maximal size of an audio frame is the same as the maximal size of teldata in the Cor
#define MAX_PACKET_SIZE	CODE_DATA_SIZE	///< maximal size of a coded packet is the same as the maximal size of Coded data in the Cor

#endif /* CODECCTL_H_ */
