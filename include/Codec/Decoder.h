/** *
 * \file	Decoder.h
 * \date	15.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief 	Header File for the decoder class
 */

#ifndef SRC_CODEC_DECODER_H_
#define SRC_CODEC_DECODER_H_

#include "SisThread.h"
#include "ICorTel.h"
#include "Result.h"
#include "AudioRouter.h"

class ICorTel;
struct OpusDecoder;

/**
 * \class 	Decoder
 * \brief 	The decoder class handles the decoding of the Tel samples
 * 			using the Opus decoder
 */
class Decoder: public SisThread {
public:
	/**
	 * \brief	Constructor does not call the initialization of the decoder
	 * 			the init command has to be called separately!!
	 * \param[in]	pItf	interface to the Tel data in the Cor
	 */
	Decoder(ICorTel* pItf, AudioRouter *);

	/**
	 * \brief Destructor, destroys the Opus decoder
	 */
	virtual ~Decoder();

	/**
	 * \brief Send init request to the decoder thread
	 */
	int init(Sync* pSync = NULL, Result* pRes = NULL);

	/**
	 * \brief Send decoding request to the decoder thread
	 */
	int Decode(Sync* pSync = NULL, Result* pRes = NULL);

private:
	/**
	 * \brief Dispatcher for commands
	 */
	virtual int dispatch(Command* pCmd);

	/**
	 * \brief 	Internal function to perform init command.
	 * 			initializes the Opus decoder with the values from the CodecCtl.h File.
	 * \return result with error messages
	 */
	Result initCmd();

	/**
	 * \brief 	Internal function to perform the Decode command
	 * 			Gets data from CodeRx , Decodes them and writes Samples out data to the memory
	 * \return result with error messages
	 */
	Result DecodeCmd();

	/**
	 * \brief enum for commands
	 */
	enum ECmd {
		eCmd_stop = 0,	/**< stop decoding */
		eCmd_init,		/**< initialize */
		eCmd_Decode		/**< Decode Data */
	};

	ICorTel* m_TelItf; 			/**< Interface for the COR memory access */
	OpusDecoder* m_decoder;		/**< Holds the state of the decoder */
	AudioRouter* m_router;		//Connection to the router
};
#endif /* SRC_CODEC_DECODER_H_ */
