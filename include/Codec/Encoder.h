/**
 * \file	Encoder.h
 * \date	15.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief 	Header File for the encoder class
 */

#ifndef SRC_CODEC_ENCODER_H_
#define SRC_CODEC_ENCODER_H_

#include <stddef.h>
#include <SisThread.h>

#include "AudioRouter.h"
#include "SisComSip.h"
#include "PeakFilter.h"

class ICorTel;
struct OpusEncoder;

/**
 * \class Encoder
 * \brief	The Encoder class handles the the encoding of the Tel samples
 * 			using the Opus encoder
 */
class Encoder : public SisThread {
public:
	/**
	 * \brief	Constructor dose not call the initialization of the encoder
	 * 			the init command has to be called separately!!
	 * \param[in]	pItf	Interface to the Tel data in the Cor
	 */
	Encoder(ICorTel* pItf, AudioRouter* rt, SisComSip* sip);

	/**
	 * \brief Destructor, destroys the Opus encoder
	 */
	virtual ~Encoder();

	/**
	 * \brief	Send init request to the encoder thread
	 */
	int init(Sync* pSync = NULL, Result* pRes = NULL);

	/**
	 * \brief 	Send encoding request to the encoder thread
	 */
	int Encode(Sync* pSync = NULL, Result* pRes = NULL);

private:
	/**
	 * \brief Dispatcher for commands
	 */
	virtual int dispatch(Command* pCmd);

	/**
	 * \brief	Internal function to perform init command.
	 * 			initializes the Opus encoder with the values from the CodecCtl.h File.
	 * \return result with error messages
	 */
	Result initCmd();

	/**
	 * \brief	Internal function to perform the encode command
	 * 			Gets data from Samples in, encodes them and writes CodeTx data to the memory
	 * \return result with error messages
	 */
	Result EncodeCmd();

	/**
	 * \brief Internal function to copy other data from samples in to CodeTx
	 * \return result with error messages
	 */
	Result CopyData();

	/**
	 * \brief enum for commands
	 */
	enum ECmd {
		eCmd_stop = 0,	/**< stop encoding */
		eCmd_init,		/**< initialize */
		eCmd_Encode		/**< Encode data */
	};

	ICorTel* m_TelItf; 			/**< Interface for the COR memory access */
	OpusEncoder* m_encoder;		/**< Holds the state of the encoder */
	AudioRouter* m_Router;		//Connection to the Router
	SisComSip* m_sip;			//Connection to the Sip client
};
#endif /* SRC_CODEC_ENCODER_H_ */
