/**
 * \file	paramWrapper.h
 * \date	10.11.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief 	Header File containing struct definitions
 *          This is to overcome the single parameter limit of a thread command.
 */

#pragma once

typedef struct{
    SisAccount* acc;
    std::string num;
}paramMakeCall;