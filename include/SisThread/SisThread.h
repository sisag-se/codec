/** *
 * \file	SisThread.h
 * \date	09.5.2016
 * \author	brr
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 */

#ifndef SisThread_H
#define SisThread_H

#include "Command.h"
#include <pthread.h>
#include <sched.h>
#include <queue>

#define CMD_THREAD_STOP 1
#define CMD_HANDLED_SUCCESSFUL 0
#define CMD_HANDLED_FAILED -2
#define CMD_NOT_HANDLED  -1

class Command;

typedef struct{
    Sync* pSync = NULL;
    Result* pRes = NULL;
}threadCallParam;


/**
 * \class 	SisThread
 * \brief 	SisThread is the base class to create threads. Each class derived from SisThrad
 * 			will be enabled to run in a separate thread and to process a command queue.
 * 			To start the thread the main thread function "ThreadFunc" is called.
 */
class SisThread
{
public:
	enum EThreadName {
		eMain = 0,
		eEncoder,
		eDecoder,
        eSip,
        eRouter,
		eNoName
	};

	 /**
	 * \brief Constructor
     * \param[in] threadName	Thread name for debug purpose
	 */
	SisThread(const EThreadName threadName = eNoName);

	 /**
	 * \brief Destructor
	 */
    virtual ~SisThread();

    /**
     * \brief Creates and starts the thread
     * \param[in] policy	Defines the schedule policy: SCHED_OTHER (default), SCHED_FIFO or SCHED_RR (realtime)
     * \param[in] prio	Defines the priotity of the thread: min/max 0...99
     * \return returns 0 if success otherwise -1
     */
    int start(int policy = SCHED_OTHER, int prio = 0);

    /**
     * \brief 	Stops the thread
     * \remark 	To catch the stopped thread the join for this thread is also included,
     * 			this prevents from a still existing daemon.
     */
    int stop();

    /**
     * \brief Returns the id of the created thread
     */
    pthread_t getThread();

    /**
     * \brief Main thread entry function
     * \param pParam Pointer to parameter data (void*).
     */
    static void* ThreadFunc(void* pParam);

    /**
     * \brief 	Dispatcher for commands
     * \param 	pCmd Command class for processing a certain command function
     * \remark 	This method must be overridden by the derived class to
     * 			implement the specific commands.
     */
    virtual int dispatch(Command* pCmd);

    /**
     * \brief Pushes a command in the queue of the current thread
     * \param pCmd Command class pointer
     */
    int post(Command* pCmd);

    /**
     *\brief 	Gets the schedule policy and the priority of a thread
     *\return 	Returns 0 if okay otherwise -1
     */
    static int getScheduleAndPrio(pthread_t id, int &policy, int &prio);

    /**
	 *\brief 	Sets schedule policy and the priority of a thread, there are root rights necessary
	 *\return 	Returns 0 if okay otherwise -1
	 */
    static int setScheduleAndPrio(pthread_t id, int policy, int prio);

protected:

    /**
     * \brief Sets a threat to sleep if nothing to do
     */
    void pause();

    /**
     * \brief Wakes up a resumed thread
     */
    void resume();

    /**
     * \brief Waits until a new Cmd is in the queue and returns it
     * \return pCmd		The new Cmd
     */
    Command* getCmd();

    /**
     * \brief Deletes a command on the heap and sets the pointer to NULL
     * \param[in] pCmd		The command that should be deleted on the heap.
     */
    void removeCmd(Command* pCmd);

    /**
     * \brief 	Internal stop command to stop and remove the thread
     * \remark 	The Thread class must be removed by the parent
     */
    int stopCmd();

    const EThreadName m_threadName;			/**<holds the thread name */

private:
    pthread_t m_thread;					/**<holds the thread id */
    pthread_mutex_t m_mutex;			/**<prevents to race conditions */
    pthread_cond_t m_condVar;			/**<condition variable to wait for */
    std::queue<Command*> m_cmdQueue; 	/**<command queue for the thread */
};



#endif /* SisThread_H */
