/** *
 * \file	Sync.h
 * \date	09.5.2016
 * \author	brr
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 */

#ifndef Sync_H
#define Sync_H

#include <pthread.h>

/**
 * \class 	Sync
 * \brief 	The Sync class synchronizes two threads by a condition variable of pthread
 */
class Sync
{
public:
	 /**
	  * \brief Constructor
	  */
	Sync();

	 /**
	  * \brief Destructor
	  */
    virtual ~Sync();

	/**
	 * \brief set a thread to sleep
	 */
    void wait();

	/**
	 * \brief wakes up a the sleeping thread
	 */
    void signal();

	/**
	 * \brief enabled the Sync object for a new synchronization
	 */
    void reset();

	/**
	 * \brief Returns the mutex
	 */
    pthread_mutex_t& getMutex();

    /**
	 * \brief Returns the condition variable
	 */
    pthread_cond_t& getCond();


private:
    pthread_mutex_t m_mutex;			/**<prevents to race conditions */
    pthread_cond_t m_condVar;			/**<condition variable to wait for */
    bool m_enableSuspend;
};



#endif /* Sync_H */
