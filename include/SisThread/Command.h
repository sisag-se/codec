/** *
 * \file	Command.h
 * \date	09.5.2016
 * \author	brr
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 */

#ifndef Command_H
#define Command_H

#include <stddef.h>

class Sync;
class Result;

/**
 * \class 	Command
 * \brief 	The command class is used to transfer commands to
 * 			the command queue of a task or a thread.
 */
class Command
{
public:
	 /**
	  * \brief Constructor
	  */
	Command(int cmdId, void* pParam = NULL, Sync* pSync = NULL, Result* pRes = NULL);

	 /**
	  * \brief Destructor
	  */
    virtual ~Command();

	 /**
	  * \brief Returns the stored command number
	  */
    int getCmdId();

	/**
	 * \brief Returns the stored parameter data pointer
	 */
    void* getParam();

    /**
	 * \brief Returns the stored Sync object pointer
	 */
    Sync* getSync();

    /**
	 * \brief Returns the stored Result object pointer
	 */
    Result* getResult();


private:
    int m_CmdId; 		/**< command id*/
    void* m_pParam;		/**< parameter data pointer*/
    Sync* m_pSync; 		/**< Sync pointer*/
    Result* m_pRes; 	/**< Result pointer*/
};



#endif /* SisThread_H */
