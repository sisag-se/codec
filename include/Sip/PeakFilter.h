/** *
 * \file	PeakFilter.h
 * \date	09.03.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 * 
 * \brief This Class is for filtering the AudioLevel from SIP calls via peak detection and decay.
 *        Peaks are detected and stored. The Stored peaks slowly decay till a bigger peak comes and replaces the last one
 */

#pragma once

#define FILTER_DECAY_TIME 20

class PeakFilter{
    public:
        /**
         * \brief	Constructor, there for completion sake.
         */
        PeakFilter();

        /**
         * \brief	Destructor, there for completion sake.
         */
        ~PeakFilter();


        /**
         * \brief	The main filter method.
         * \param[in]	New number to filter
         * \return  	Current filter Value
         */
        unsigned int filter(unsigned int);

    private:
        unsigned int m_levelStore;
};