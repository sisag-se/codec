/** *
 * \file	AudioRouter.h
 * \date	30.09.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 * 
 * \brief This class routes all the phone data from any source to any sink.
 */

#pragma once

#include <map>
#include <queue>

#include "Result.h"
#include "ICorTel.h"

#include "TelStruct.h"

/**
 * \class 	AudioRouter
 * \brief 	Class used to route the audio data. Only one Source can be connected with one sink at a time.
 */
class AudioRouter{
    public:
        /**
         * \brief	Constructor for the router, does nothing
         */
        AudioRouter(ICorTel*, ECallState*);

        /**
         * \brief	Destructor for the router, does nothing
         */
        ~AudioRouter();

        /**
         * \brief	Connect the source with the sink, if the source or sink are already connected,
         *          it will disconnect it and connect the new route.
         */
        void setRoute(ESource, ESink);
        void setRoute(std::pair<ESource, ESink>);

        /**
         * \brief	Disconnect the given source from any sink
         */
        void removeRoute(ESource);

        /**
         * \brief	Disconnect the given sink from any source
         */
        void removeRoute(ESink);

        /**
         * \brief	Get the sink the given source is connected to
         */
        ESink getRoute(ESource);

        /**
         * \brief	Get the source the given sink is connected to
         */
        ESource getRoute(ESink);

        /**
         * \brief	Disconnect everything!
         */
        void clearAllRoutes();

        /**
         * \brief	For the case that the routes change suddenly. This removes the then possibly unused memory.
         *          If the source is eNoSource, it clears all buffers.
         */
        void clearSamplesBuffer();

        /**
         * \brief	For the case that the routes change suddenly. This removes the then possibly unused memory.
         *          If the source is eNoSource, it clears all buffers.
         */
        void clearSamplesBuffer(ESink);

        /**
         * \brief	Push a frame to the routers buffer for the given source.
         */
        void pushFrame(ESource, std::vector<int>&);

        /**
         * \brief	Return the buffer for either SIP or the Encoder
         */
        void getFrame(ESink, std::vector<int>&);

        /**
         * \brief	Return m_sipFrameReady
         */
        bool isSipFrameReady(); 

        /**
         * \brief	Set m_sipFrameReady
         */
        void setIsSipFrameReady(bool state);


    private:
        //** Buffers **
        std::queue<std::vector<int>> m_fifoEncode; //Buffer for the encoder
        std::queue<std::vector<int>> m_fifoDecode; //Buffer from the decoder
        std::queue<std::vector<int>> m_fifoAnalogOut; //Buffer from the PC316
        std::vector<int> m_resampleBuffer; //Buffer used to store sip frames to get to the same sample count as SisCom

        /**
         * \brief	This indicates that a sip frame is ready and was pushed to the router. 
         *          This is used insted of FPGA flags to indicated when to encode.
         */
        bool m_sipFrameReady = false;

        //Connections
        std::map<ESource, ESink> m_plugboard;
        ICorTel* m_TelItf; 			/**< Interface for the COR memory access */

        /**
         * \brief	call state, set from the SisComSip class.
         *          This is used to filter out received data when passive during a call
         */
        ECallState* m_callState;

        /**
         * \brief List of all available commands
         */
        enum ECmd {
            eCmd_stop = 0,	/**< stop encoding */
            eCmd_init		/**< initialize */
        };

        /**
         * \brief 	Swaps the low word from the input to the high word of the output
         * 			this is to set the data to the right palace in the COR so the FPGA can put the Data out.
         * \param[in]	data	Data to perform the calculation on
         * \return Data with the low word of the input data at the high word of the output data
         */
        int swapLocalRemote(int data);

        /**
         * \brief 	This is a helper Function for pushFrame. It handles storing the frames in buffers
         * \param[in]	sink	The sin associated with the frame
         * \param[in]	frame	Frame to handle
         */
        void handleFrame(ESink, std::vector<int>&);

        /**
         * \brief 	This is a helper Function for creating an extended frame from a buffer
         * \param[in]	sampleSource The Buffer to get the samples from
         * \param[in]	frame	The frame to extend
         */
        void extendFrame(std::queue<std::vector<int>>&, std::vector<int>&);
};