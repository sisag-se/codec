/** *
 * \file	SisComSip.h
 * \date	30.09.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 * 
 * \brief This Class is for handling all the SIP stuff via the pjsip library.
 */

#pragma once

#include <pjsua2.hpp>
#include <pj/file_access.h>
#include <iostream>
#include <vector>

#include "Result.h"
#include "SisThread.h"
#include "Sync.h"
#include "Command.h"

#include "ConfigParser.h"
#include "AudioRouter.h"
#include "TelStruct.h"
#include "Timer.h"
#include "TelMap.h"

using namespace pj;

class SisAccount;
class SisCall;

#include "paramWrappers.h"

/**
 * \class 	SisComSip
 * \brief 	THE class to make all the SIP stuff work on the SisCom
 */
//class SisComSip: public SisThread{
class SisComSip:public SisThread{
    public:
    	/**
         * \brief	Constructor, Does all the initial setup and registers all configured numbers.
         */
        SisComSip(AudioRouter*, TelMap*, ECallState*);

        /**
         * \brief	Destructor, Kills all calls and unregisters all numbers
         */
        ~SisComSip();

        /**
         * \brief	Send init request to the encoder thread
         */
        void init(SConfSip,
                std::function<void(SPhoneHeader)>,
                std::function<void()> sendAck,
                std::function<void()> sendNack,
                Sync* pSync = NULL, Result* pRes = NULL);

        /**
         * \brief	Make a call to the given phone number
         * \param[in]	The account number to make the call with
         * \param[in]	The phone number to call
         */
        void makeCall(unsigned int, std::string num, Sync* pSync = NULL, Result* pRes = NULL);

        /**
         * \brief	Make a call to the given phone number
         * \param[in]	The accountnumber to make the call with
         * \param[in]	The phone number to call
         */
        void makeCall(unsigned int, const unsigned int num, Sync* pSync = NULL, Result* pRes = NULL);

        /**
         * \brief	Make a call to the given phone number
         * \param[in]	The account to make the call with
         * \param[in]	The phone number to call
         */
        void makeCall(SisAccount*, unsigned int, Sync* pSync = NULL, Result* pRes = NULL);

        /**
         * \brief	Make a call to the given phone number or IP address
         *          The given string must be a FULL SIP ADDRESS OR IP
         * \param[in]	The phone number or IP to call
         */
        void makeCall(SisAccount*, std::string, Sync* pSync = NULL, Result* pRes = NULL);

        /**
         * \brief	Accept the currently active call
         */
        void acceptCall(Sync* pSync = NULL, Result* pRes = NULL);

        /**
         * \brief	Hangup the currently active call
         */
        void hangup(Sync* pSync = NULL, Result* pRes = NULL);

        /**
         * \brief	Helper function to work around the timers limitation of not accepting paramters define in std::bind
         */
        void timerHangup();

        /**
         * \brief	This function will get called when a call was picked up.
         *          It stops the timer that hangs up a too long unanswered call
         */
        void stopHangupTimer();

        /**
         * \brief	Returns the state of the current call
         * \return Current State
         */
        ECallState getCallState();

        /**
         * \brief	Set the state of the current call
         * \param[in] The new state
         */
        void setCallState(ECallState);

        /**
         * \brief	Returns the number of the current call
         * \return Current number
         */
        unsigned int getCurrentCallNum();

        /**
         * \brief	Set the number of the current call
         * \param[in] The new number
         */
        void setCurrentCallNum(unsigned int);

        /**
         * \brief	Register the given phone number/s on the PBX
         * \param[in]	The phone number/s to Register
         */
        void registerNumber(unsigned int, Sync* pSync = NULL, Result* pRes = NULL);
        void registerNumber(SPhoneNumber num, Sync* pSync = NULL, Result* pRes = NULL);
        void registerNumbers(std::vector<unsigned int> num, Sync* pSync = NULL, Result* pRes = NULL);
        void registerNumbers(std::vector<SPhoneNumber> num, Sync* pSync = NULL, Result* pRes = NULL);

        /**
         * \brief	Delete a given number from the registered ones (to yeet: The act of throwing an object far)
         * \param[in]	The phone number to delete
         */
        void yeetNumber(unsigned int num, Sync* pSync = NULL, Result* pRes = NULL);

        /**
         * \brief	Returns the audio level of the currently active call.
         */
        unsigned int getAudioLevel();

        /**
         * \brief	Returns the audio level of a specific account.
         * \param[in]   The accounts registered number (Number to call this account).
         */
        unsigned int getAudioLevel(unsigned int);

        unsigned int uriToNumber(std::string uri);

        //Callback functions for communicating with the call router
        //These would normally be private, but SisAccount & SisCall need access to them
        std::function<void(SPhoneHeader)> m_sendHeaderCallback;
        std::function<void()> m_sendAck;
        std::function<void()> m_sendNack;

        AudioRouter* m_router;
        TelMap* m_telMap;

        /**
         * \brief	Conf stuff
         */
        SConfSip m_conf;

    private:
        /**
         * \brief	Vector containing pointers to all account instances
         */
        std::map<unsigned int, SisAccount*> m_registeredAccounts;

        /**
         * \brief	Helper Function to determin wheter a station is equipped with a PBX
         * \param[in]	Station to check
         * \return 	True when the station has a PBX
         */
        bool hasStationPBX(EStation);

        /**
         * \brief	Dispatcher for commands, called when a new command arrives
         */
        virtual int dispatch(Command* pCmd);

        /**
         * \brief	same as int but executed within the thread
         */
        void thInit();

        /**
         * \brief	same as makeCall but executed within the thread
         */
        Call* thMakeCall(SisAccount*, const std::string);

        /**
         * \brief	same as registerNumber but executed within the thread
         */
        SisAccount* thRegisterNumber(SPhoneNumber);

        /**
         * \brief	same as yeetNumber but executed within the thread
         */
        Result thYeetNumber(unsigned int);

        /**
         * \brief	same as getAudioLevel but executed within the thread
         *          PJSip requires this to be executed within its thread
         */
        void thGetAudioLevel(unsigned int*);

        /**
         * \brief	same as hangup but executed within the thread
         *          PJSip requires this to be executed within its thread
         */
        void thHangup();

        /**
         * \brief	same as accept but executed within the thread
         *          PJSip requires this to be executed within its thread
         */
        void thAccept();

        /**
         * \brief	This method cofigures all the sip stuff like endpoints and transport
         */
        void configureSip();

        SisAccount* getAccountForNumber(unsigned int);

        /**
         * \brief	Local Endpoint Instance. This thing is
         *          the root of all SIP operations.
         */
        Endpoint m_enp;

        /**
         * \brief	The current state of a call
         */
        ECallState* m_callState;

        /**
         * \brief	Used to pass the audiolevel between the SisCom Sip thread and the rest
         *          Thus the audiolevel you normally get is one reading behind
         */
        unsigned int m_audioLevel = 0;

        /**
         * \brief	Stores the number of the account that is currently in a call
         *          Is -1 when there is no account in a call
         */
        int m_accountInCall = -1;


        Timer <std::function<void()>>m_hangupTimer;
        Timer <std::function<void()>>m_longCallTimer;

        std::vector<int> m_levelQueue;

        /**
         * \brief	Enum categorizing all the possible thread commands
         */
        enum ECmd {
            eCmd_stop = 0,	    //< stop encoding
            eCmd_init,		    //< initialize
            eCmd_makeCall,	    //< Encode data
            eCmd_regNumber,     //< Register a new Number
            eCmd_yeetNumber,    //< Delete a number
            eCmd_getAudioLevel, //< Get the audio level
            eCmd_hangup,        //< Hangup the current call
            eCmd_accept         //< Accept the current call
        };

};
