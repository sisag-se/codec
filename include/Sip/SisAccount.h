/** *
 * \file	SisAccount.h
 * \date	02.03.2021
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 * 
 * \brief This Class is for handling all the SIP accounts
 */

#pragma once

#include <pjsua2.hpp>
#include <vector>
#include "SisComSip.h"

/**
 * \class 	SisAccount
 * \brief 	Subclass to handle a sip account
 */
class SisAccount:public Account{
    public:
        /**
         * \brief	Variable containing pointers to call calls the account is in
         */
        std::vector<Call *> calls;

        /**
         * \brief	Constructor for an account, this does nothing
         */
        SisAccount(SisComSip*);

        /**
         * \brief	Destroy every call the account is in
         */
        ~SisAccount();
        
        /**
         * \brief	Remove a call from the account
         * \param[in]   The call to remove
         */
        void removeCall(Call *);

        /**
         * \brief	Clear all calls in the account
         */
        void removeCall();

        /**
         * \brief	Called anytime the registered status of the account changes.
         * \param[in]   Callback Parameter
         */
        virtual void onRegState(OnRegStateParam &);

        /**
         * \brief	Called anytime there is an incomingCall for the account.
         * \param[in]   Callback Parameter
         */
        virtual void onIncomingCall(OnIncomingCallParam &);

    private:
        SisComSip* m_sip;
};