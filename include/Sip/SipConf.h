/** *
 * \file	SipTypes.h
 * \date	30.09.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 * 
 * \brief A File containing several typedefs for SisComSip
 */

#pragma once


#define THIS_FILE 	"SisComSip.cpp"

#define LOG_LOCATION "/root/LogFiles/sipLog.log"

#define SIP_SAMPLES_PER_FRAME 160
#define SISCOM_SAMPLES_PER_FRAME 480
#define STORE_FRAMES SISCOM_SAMPLES_PER_FRAME/SIP_SAMPLES_PER_FRAME //3 SIP = 1 SisCom Frame
#define CHANEL_COUNT 1

#define SIP_LOG_LEVEL 0

#define HANGUP_TIMEOUT 120000 //Hangup after this much time in ms of no answer

#define NOISE_FLOOR_DELAY 20 //How many frames of 20ms should be waited before no more sip samples are pushed into the router


//#define DEBUG_PBX             // Register the numbers differently for debugging
//#define DISABLE_NOISE_FLOOR   //Disables the noise floor so samples will always be sent no matter their loudness
//#define DEBUG_TIMING          //This enables the print out of timing information on receiving and sending samples