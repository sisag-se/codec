/** *
 * \file	SisCall.h
 * \date	02.03.2021
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 * 
 * \brief This Class is for handling all the SIP calls
 */

#pragma once

#include <pjsua2.hpp>
#include <vector>
#include <mutex>
#include "SisComSip.h"
#include "AudioRouter.h"

/**
 * \class 	SisCall
 * \brief 	Subclass to handle specific features of a single call
 */
class SisCall : public Call{
    public:
        /**
         * \brief	When a new call is created, store its account
         * \param[in]	&acc Reference to the account associated with the call
         * \param[in]	&sip Reference to the SisComSip object handling this call
         * \param[in]	&call_id The Calls id
         */
        SisCall(Account*, SisComSip*, int call_id = PJSUA_INVALID_ID);
        
        /**
         * \brief	Destroy the call
         */
        ~SisCall();
        
        /**
         * \brief	This callback is called anytime the calls state has changed
         * \param[in]   Callback Parameter
         */
        virtual void onCallState(OnCallStateParam &);

        /**
         * \brief	Called when the call is being transferred, this transfer can
         *          either be accepted or rejected.
         * \param[in]	Callback Paramter
         */
        virtual void onCallTransferRequest(OnCallTransferRequestParam &);

        /**
         * \brief	Called when the exisiting call has been replaced with a diffrent one.
         * \param[in]   Callback Parameter
         */
        virtual void onCallReplaced(OnCallReplacedParam &);

        /**
         * \brief	This method is used to setup the calls media environment aka:
         *          memCapture and memPlayback setup.
         *          Called anytime the media state in the call changed.
         * \param[in]   Callback Parameter
         */
        virtual void onCallMediaState(OnCallMediaStateParam &);

        /**
         * \brief	The ID the capture port has in the conference bridge
         */
        pjsua_conf_port_id m_capPortId = -1;

        /**
         * \brief	The ID the playback port has in the conference bridge
         */
        pjsua_conf_port_id m_playPortId = -1;

    private:
        SisComSip* m_sip;

        /**
         * \brief	Var to store the account associated with this call
         */
        Account* m_acc;

        void destroyMedia();

        // The media ports to connect to the call (conference bridge)
        pjmedia_port *m_prtCap = NULL;
        pjmedia_port *m_prtPlay = NULL;
        pj_pool_t* m_pool = NULL;
        void *buffer0;
        void *buffer2;

        bool second = false;
        std::mutex mediaMutex;
};

/*
 * Note: captureSipFrames and playSipFrames are C functions due to casting method pointers
 *       to function pointers being complicated to work with. And this works...
*/


/**
 * \brief	This callback method dumps all frames' audio from SIP to memory.
 * \param[in]	A media port associated with this callback.
 * \param[in]   A void pointer containing the frame's audio data
 */
void captureSipFrames(pjmedia_port* , void *);

/**
 * \brief	This callback method sends an audio frame of data, from memory to SIP.
 * \param[in]	A media port associated with this callback.
 * \param[in]   A void pointer containing the frame's audio data
 */
pj_status_t playSipFrames(pjmedia_port * port, void * usr_data);