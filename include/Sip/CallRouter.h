/**
 * \file	CallRouter.h
 * \date	20.10.2020
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 * 
 * \brief This Class is for sending and receiving DTMF signals
 */

#pragma once

#include <string>
#include <vector>
#include "SisComSip.h"
#include "DtmfMap.h"
#include "TelStruct.h"

#define INCOMPLETE_HEADER -1
#define NOT_THE_ONE_CALLED -2

/**
 * \class 	CallRouter
 * \brief 	This class handles all the DTMF functionality,
 *          including sending and receiving phone numbers
 */
class CallRouter{
    public:
        /**
         * \brief	Constructor, sets the callback function when a number is detected
         * \param[in] Access to the COR for sending an receiving DTMF
         */
        CallRouter(DtmfMap*, ConfigParser*, SisComSip*);

    	/**
         * \brief	Destructor, Does nothing else but exist
         */
        ~CallRouter();

        /**
         * \brief	Polling function to pick up new DTMF's
         *          Forwards any DTMF found to handleDTMF(DTMF)
         */
        void poll();

        /**
         * \brief	Method for transmitting a Call init header
         * \param[in] The header to send
         */
        void sendHeader(SPhoneHeader);

        /**
         * \brief	Method for transmitting a ack "*" symbol for acknlowledging the sent header
         */
        void sendAck();

        /**
         * \brief	Method for transmitting a nack "#" symbol for telling the call has finished or not answered
         */
        void sendNack();

    private:
        /**
         * \brief	Method to parse the DTMF buffer for a "SIP header"
         * \param[out] Parsed Numbers and state
         * \return  The parser state
         */
        SPhoneHeader parseBufferForNumber();

        /**
         * \brief	Method to handle new DTMF's
         * \param[in] The new dtmf
         */
        void handleDTMF(SDtmf);

        /**
         * \brief	Method to handle the result of the parseBufferForNumber() method
         * \param[in] The result of the parsed header
         */
        void handleParserResult(SPhoneHeader);

        /**
         * \brief	This method is called as a callback when a new single DTMF can be received
         */
        void resetSingleDtmf();

        /**
         * \brief	This method clears the internal dtmf buffer
         */
        void clearBuffer();

        /**
         * \brief	Buffer for storing the currently incoming DTMF's
         *          Parse this for the phone number to call
         */
        std::vector<std::string> m_bufferDTMF;

        /**
         * \brief	Reference to the FPGA dtmf map.
         *          All DTMF IO operations are executed with this.
         */
        DtmfMap* m_dtmf;

        /**
         * \brief	Reference to the conf parser for getting phone numbers
         */
        ConfigParser* m_conf;

        /**
         * \brief	Reference to SisComSip for making calls
         */
        SisComSip* m_sip;

        Timer <std::function<void()>>dtmfTimer;

        bool m_singleDTMF = false;
        bool m_resetOnce = false;
};