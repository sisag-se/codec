/*
 * ErrorLogger.h
 *
 *  Created on: 25.04.2019
 *      Author: hen
 */

#ifndef SRC_HELPER_ERRORLOGGER_H_
#define SRC_HELPER_ERRORLOGGER_H_

#include <map>
#include <stddef.h>
#include "Result.h"
#include "ErrorId.h"
#include "ICorTel.h"

class ErrorLogger {
public:
	struct SLog {
		SLog()
		: res(NULL)
		, Origin(0)
		{};
		Result* res;
		int Origin;
	};

	/**
	 * \brief	Constructor
	 * \param[in]	pItf	Interface to the Tel data in the Cor
	 */
	ErrorLogger(ICorTel* pItf);

	/**
	 * \brief Destructor
	 */
	virtual ~ErrorLogger();

	/**
	 * \brief	evaluates all results and gets the most prioritized error ID (lowest Error ID)
	 * 			Writes the Error ID to the Cor.
	 * \param[in]	ParamLog
	 */
	Result ErrorLog(SLog& ParamLog);



private:

	Result setErrFlagId();

	enum EFlags{
		eGeneral	= ERR_ORIG_GENERAL		/ ERR_GROUP_SIZE,
		eConfig		= ERR_ORIG_CONF			/ ERR_GROUP_SIZE,
		eCsr		= ERR_ORIG_CSR			/ ERR_GROUP_SIZE,
		eFpga		= ERR_ORIG_FPGA			/ ERR_GROUP_SIZE,
		eComm		= ERR_ORIG_COM			/ ERR_GROUP_SIZE,
		eProt		= ERR_ORIG_PROTOCOL		/ ERR_GROUP_SIZE,
		eI2c		= ERR_ORIG_I2C			/ ERR_GROUP_SIZE,
		eHwIo		= ERR_ORIG_HW_IO		/ ERR_GROUP_SIZE,
		eIoModul	= ERR_ORIG_HW_IO_MODUL	/ ERR_GROUP_SIZE,
		ePlc		= ERR_ORIG_PLC			/ ERR_GROUP_SIZE,
		eWatchRtc	= ERR_ORIG_WATCHRTC		/ ERR_GROUP_SIZE,
		eLog		= ERR_ORIG_LOG			/ ERR_GROUP_SIZE,
		eTel		= ERR_ORIG_TEL			/ ERR_GROUP_SIZE,
		eCor		= ERR_ORIG_COR			/ ERR_GROUP_SIZE,
		eCodec		= ERR_ORIG_CODEC		/ ERR_GROUP_SIZE
	};

	struct SErrFlags {
		SErrFlags()
		: Flags(0)
		, LowErrId(0)
		{};
		short Flags;
		int LowErrId;
	};

	std::map<int, SErrFlags> m_ErrFlags;	/**< Flag-Speicher der einzelnen Threads */
	ICorTel* m_TelItf; 						/**< Interface for the COR memory access */
};

#endif /* SRC_HELPER_ERRORLOGGER_H_ */
