//Base: https://github.com/99x/timercpp
/** *
 * \file	Timer.h
 * \date	09.02.2021
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief header file for handling timer and interval like events
 */


#pragma once

#include <chrono>
#include <thread>
#include <condition_variable>
#include <mutex>


/**
 * \class	Timer
 * \brief	Implements a timer and interval based event handler. The class template defines the callback function type
 *          There is a optional paramter for defining a time factor (milliseconds, seconds, nanoseconds etc.)
 * \example Timer<std::function<void()>> tim;
 * \example Timer<std::function<void()>, std::chrono::seconds> tim;
 * \note    One instance only supports either a timer or an interval. But not both at the same time!
 *          Parameters should be possible but seem to not really work. Hmmmmm...
 */

template <class T, typename duration = std::chrono::milliseconds>
class Timer{
    public:

        /**
         * \brief	Calls the given function after the delay expires
         * \param[in] function to call back
         * \param[in] How long to wait till the function gets called
         * \param[in] Wheter the function gets called first and then delay or vice versa
         */
        void setTimeout(T function, unsigned int delay, bool first) {
            if(!isRunning){
                this->isRunning = true;
                m_func = function;

                waitThread = new std::thread(&Timer::timeout, this, delay, first);
                waitThread->detach();
            }
        }

        /**
         * \brief	Calls the given function every time after the delay expires
         * \param[in] function to call back
         * \param[in] How long to wait till the function gets called
         * \param[in] Wheter the function gets called first and then delay or vice versa
         */
        void setInterval(T function, unsigned int interval, bool first) {
            if(!isRunning){
                this->isRunning = true;
                m_func = function;

                intervalThread = new std::thread(&Timer::interval, this, interval, first);
                intervalThread->detach();
            }
        }

        void stop(){
            std::unique_lock<std::mutex> lck(mtx);
            this->isRunning = false;
            sleepCond.notify_all();
            threadDone.wait_for(lck, std::chrono::milliseconds(100));
            // Note: when the thead finished, the memory appears to be free'd
            // so when using a delete here, it causes a things to go apeshit!
        }

        bool running(){
            return this->isRunning;
        }

        ~Timer(){
            std::unique_lock<std::mutex> lck(mtx);
            this->isRunning = false;
            sleepCond.notify_all();
            
        }

    private:
        T m_func;
        bool isRunning = false;
        std::thread* waitThread;
        std::thread* intervalThread;
        std::condition_variable sleepCond;
        std::condition_variable threadDone;
        std::mutex mtx;


        void timeout(unsigned int time, bool first){
            wait(time, first);
            this->isRunning = false; // reset running at end of function if not stopped
            threadDone.notify_all();
        }

        void wait(unsigned int time, bool first){
            if(!this->isRunning){
                return;
            }

            if(first){
                m_func();
            }
            
            duration dur = duration(time);
            std::unique_lock<std::mutex> lck(mtx);
            sleepCond.wait_for(lck, dur); //Wait for duration or notify cause stopped

            if(!this->isRunning){
                return;
            }

            if(!first){
                m_func();
            }
        }

        void interval(unsigned int time, bool first){

            while(true){
                if(!this->isRunning){
                    break;
                }
                wait(time, first);
            }
            threadDone.notify_all();
        }
};