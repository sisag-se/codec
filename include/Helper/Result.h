/**
 * \file	Result.h
 * \date	03.02.2016
 * \author	mem, brr
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 */

#ifndef RESULT_H_
#define RESULT_H_

#include <list>
#include <string>
#include "ErrorId.h"

/**
 * \class	Result
 * \brief	Resultat Klasse mit Id und Message
 */
class Result {
public:
	/**
	 * \brief Result struct mit ID und Message
	 */
	struct SResultData {
		SResultData(const int code, const int id, const std::string& msg)
		: status(code)
		, errorId(id)
		, resultMsg(msg)
		{ };

		int status;				/**< individueller R�ckgabewert einer Funktion */
		int errorId;			/**< Fehler-ID des Resultats */
		std::string resultMsg;	/**< Message des Resultats */
	};

	/**
	 * \brief Constructor
	 */
	Result();

	/**
	 * \brief Copy Constructor
	 */
	Result(const Result& res);

	/**
	 * \brief Destructor
	 */
	virtual ~Result();

	void setResult(const SResultData& res);

	/**
	 * \brief setter Methode um die Error-ID und Message zu setzen
	 * \remark Wurde schon ein Resultat gesetzt, das neue  angeh�ngt.
	 * \param[in]	id		Error ID
	 * \param[in]	msg		Resultat Message
	 * \param[in]	file	file on which the result is set
	 * \param[in]	func	Function which sets the result
	 * \param[in]	line	line on which the result is set
	 */
	void setResult(const int id, const std::string& msg, const std::string file = "", const std::string func = "", const int line = 0);

	/**
	 * \brief Methode um die Error-ID und Message zu setzen, falls der Resultat-Code != 0 ist.
	 * \remark Wurde schon ein Resultat gesetzt, wird das neue angeh�ngt.
	 * \param[in]	id		Error ID
	 * \param[in]	msg		Resultat Message
	 * \param[in]	file	file on which the result is set
	 * \param[in]	func	Function which sets the result
	 * \param[in]	line	line on which the result is set
	 */
	void setResultIfFailed(const int id, const std::string& msg, const std::string file = "", const std::string func = "", const int line = 0);

	/**
	 * \brief getter Methode um die Daten des Resultats zu erhalten (letzter Eintrag)
	 * \return	Resultat Data
	 */
	SResultData& getResult();

	/**
	 * \brief getter Methode um die Message zu erhalten (letzter Eintrag)
	 * \return	Resultat Message
	 */
	std::string& getMsg();

	/**
	 * \brief getter Methode um die Error-ID zu erhalten (letzter Eintrag)
	 * \return	Error ID
	 */
	int getErrorId() const;

	/**
	 * \brief getter Methode um den Status-Code der aufgerufenen Methode zu erhalten (letzter Eintrag)
	 * \return	Status-Code
	 */
	int getStatus() const;

	/**
	 * \brief Print Error ID und Error Message (letzter Eintrag)
	 */
	void printResult();

	/**
	 * \brief Print alle Resultate
	 */
	void printAllResults() const;

	/**
	 * \brief Gibt den ganzen Resultat-Vector zur�ck
	 * \return Result vector
	 */
	std::list<SResultData> getAllResults() const;

	/**
	 * \brief Operator zur R�ckgabe des integer Wertes (ID)
	 */
	operator int () const;

	/**
	 * \brief Operator zur Zuweisung eines Resultats
	 * \remark Alle alten Resultate werden verworfen.
	 */
	Result& operator = (const Result& rhs);

	/**
	 * \brief Operator zur Zuweisung eines Integers
	 * \remark Alle alten Resultate werden verworfen.
	 */
	Result& operator = (const int& status);

	/**
	 * \brief Operator zum Anh�ngen eines neuen Resultats
	 */
	Result& operator += (const Result& rhs);

	/**
	 * \brief Gets the size of the stored result data.
	 * \return size of the stored result
	 */
	int getSize();

	/**
	 * \brief Removes all result data and resets states.
	 */
	void reset();

private:
	/**
	 * \brief L�scht alle Resultate
	 */
	void clearAll ();

	/**
	 * \brief Print einer Resultat-Zeile
	 * \param[in]	res 	message to print out
	 */
	void print(const SResultData& res) const;

	/**
	 * \brief Composes a fully message out if the parameters
	 * \remark 	The additional parameters must all be set to add to message otherwise
	 * 			the plain message will be given back
	 * \param[in]	file	file name
	 * \param[in]	func	function name
	 * \param[in]	line	line number
	 * \param[in]	msg		error message
	 * \return composed message from the input parameter
	 */
	std::string createMsg(const std::string file, const std::string func, const int line, const std::string& msg);

	int m_currentStatus;					/**< Aktueller Status-Code, wird immer wieder ueberschrieben*/
	std::list<SResultData> m_resultData;	/**< Resultat ID und Message Liste*/
};

std::string FFLtoStr(const std::string msg, const std::string file, const std::string function, const unsigned int line);

#endif /* RESULT_H_ */
