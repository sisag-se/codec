/** *
 * \file	ErrorId.h
 * \date	19.5.2016
 * \author	bem
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief 	Error Id list.
 */

#ifndef ERRORID_H_
#define ERRORID_H_

#define	NO_ERROR 				0							///< Kein Error
#define	ERR_GROUP_SIZE 			10							///< Groessse einer Gruppierungs-Einheiten
#define	ERR_MAX_GROUPS 			16							///< maximale Anzahl von Gruppierungs-Einheiten Nr 0-15 (Logger verwendet ein Short zum Speichern der Flags)

// General Briker
#define ERR_ORIG_GENERAL		0							///< Basis-Adresse Allgemein (darf nicht direkt als Error-ID verwendet werden)
#define ERR_SHELL				(ERR_ORIG_GENERAL + 1)		///< Linux shell error, IP oder Zeitzone kann nicht gesetzt werden
#define ERR_OPENFILE			(ERR_ORIG_GENERAL + 2)		///< Konfigurations-, Koeffizienten-, Error- oder Log-File kann nicht geoeffnet werden
#define ERR_CLOSEFILE			(ERR_ORIG_GENERAL + 3)		///< Konfigurations-, Koeffizienten-, Error- oder Log-File kann nicht geschlossen werden
#define ERR_CPU_CLOCK			(ERR_ORIG_GENERAL + 4)		///< CPU-Clock kann nicht gelesen werden
#define ERR_TIMER				(ERR_ORIG_GENERAL + 5)		///< Timer kann nicht gestartet oder gestoppt werden
#define ERR_FATAL_I2C			(ERR_ORIG_GENERAL + 6)		///< Fatal Error I2C Bus, Rtc zugriff funktioniert nicht mehr => I2C abgestuerzt, reboot notwendig

// Configuration
#define ERR_ORIG_CONF			10							///< Basis-Adresse Konfiguration
#define ERR_CONF_GEN			(ERR_ORIG_CONF + 1)			///< genereller Konfigurations-Fehler (Software-Fehler)
#define ERR_CONF_VALUE			(ERR_ORIG_CONF + 2)			///< falscher Wert im Konfigurations-File
#define ERR_CONF_NAME 			(ERR_ORIG_CONF + 3)			///< falscher Name im Konfigurations-File
#define	ERR_COEFF_VALUE			(ERR_ORIG_CONF + 4)			///< falscher Wert im Koeffizienten-File
#define ERR_COEFF_NAME 			(ERR_ORIG_CONF + 5)			///< falscher Name im Koeffizienten-File
#define ERR_DIV_VALUE			(ERR_ORIG_CONF + 6)			///< falscher Wert im DivConfig-File
#define ERR_DIV_NAME			(ERR_ORIG_CONF + 7)			///< falscher Name im DivConfig-File
#define	ERR_CONF_TZ				(ERR_ORIG_CONF + 8)			///< falscher Zeit-Zonen-Wert
#define ERR_STATION				(ERR_ORIG_CONF + 9)			///< falscher Stations-Wert (Dip Schalter c 7-)

//CSR
#define ERR_ORIG_CSR			20							///< Basis-Adresse Csr
#define	ERR_CSR					(ERR_ORIG_CSR + 1)			///< Daten koennen nicht aus CSR gelesen werden
#define ERR_BUFFER				(ERR_ORIG_CSR + 2)			///< Daten koennen nicht auf WEB-Server Memory geschrieben werden
#define ERR_CM_IDENTIFIER		(ERR_ORIG_CSR + 3)			///< Daten-Zugriff im Kommunikations-Interface nicht moeglich (Software-Fehler)
#define ERR_CSR_READ_ACCESS		(ERR_ORIG_CSR + 4)			///< CSR-Lese-Zugriff nicht moeglich
#define ERR_CSR_WRITE_ACCESS	(ERR_ORIG_CSR + 5)			///< CSR-Schreib-Zugriff nicht moeglich
#define ERR_DATA_SIZE_TX		(ERR_ORIG_CSR + 6)			///< Daten-Zugriff im Kommunikations-Interface nicht moeglich (Software-Fehler)
#define ERR_MEM_ACCESS			(ERR_ORIG_CSR + 7)			///< Daten koennen nicht im CSR geschrieben oder gelesen werden

// FPGA
#define ERR_ORIG_FPGA			30							///< Basis-Adresse Fpga
#define ERR_FPGA_CPU			(ERR_ORIG_FPGA + 1)			///< Keine oder zu sp�te CPU-Atwort
#define ERR_FPGA_TX				(ERR_ORIG_FPGA + 2)			///< Zu tiefer Sende-Pegel
#define ERR_FPGA_RX0_LESS		(ERR_ORIG_FPGA + 3)			///< Zu tiefes Sende-Echo (Sende-Mith�ren)
#define ERR_FPGA_RX0_MUCH		(ERR_ORIG_FPGA + 4)			///< Zu hohes Sende-Echo (Sende-Mith�ren)
#define ERR_FPGA_CODEC_TASK		(ERR_ORIG_FPGA + 5)			///< Absturz der codec Task

//COMMUNICATION
#define ERR_ORIG_COM			40							///< Basis-Adresse Kommunikation
#define ERR_COM					(ERR_ORIG_COM + 1)			///< Kommunikations-Thread oder -Objekt kann nicht erstellt werden
#define ERR_COMMUNICATOR		(ERR_ORIG_COM + 2)			///< Daten-Zugriff bei der Empfangsdaten-Auswertung nicht moeglich (Software-Fehler)
#define ERR_COM_RX1_CRC			(ERR_ORIG_COM + 3)			///< fehlerhaftes Protokoll (CRC-Fehler) von Slave1 oder Master empfangen
#define ERR_COM_RX2_CRC			(ERR_ORIG_COM + 4)			///< fehlerhaftes Protokoll (CRC-Fehler) von Slave2 empfangen
#define ERR_COM_RX3_CRC			(ERR_ORIG_COM + 5)			///< fehlerhaftes Protokoll (CRC-Fehler) von Slave3 empfangen
#define ERR_COM_RX1_NOPROT		(ERR_ORIG_COM + 6)			///< kein Protokoll von Slave1 oder Master empfangen
#define ERR_COM_RX2_NOPROT		(ERR_ORIG_COM + 7)			///< kein Protokoll von Slave2 empfangen
#define ERR_COM_RX3_NOPROT		(ERR_ORIG_COM + 8)			///< kein Protokoll von Slave3 empfangen

//internes seriell Protocol
#define ERR_ORIG_PROTOCOL		50							///< Basis-Adresse internes Protokoll
#define ERR_PROTOCOL			(ERR_ORIG_PROTOCOL + 1)		///< fehlerhafte "Service"-Daten (Master->Slave, CRC15-Fehler) empfangen
#define ERR_SP_SETDATA			(ERR_ORIG_PROTOCOL + 2)		///< Software-Fehler im internen seriellen Protokoll aufgetretten
#define ERR_SP_LENGTH			(ERR_ORIG_PROTOCOL + 3)		///< Software-Fehler im internen seriellen Protokoll aufgetretten
#define ERR_SP_CRCER			(ERR_ORIG_PROTOCOL + 4)		///< fehlerhaftes internes Protokoll (CRC5-Fehler) empfangen
#define ERR_SP_SEQER 			(ERR_ORIG_PROTOCOL + 5)		///< Synchronisations-Fehler des internen seriellen Protokolls
#define ERR_SP_GETRXDATA		(ERR_ORIG_PROTOCOL + 6)		///< Software-Fehler im internen seriellen Protokoll aufgetretten

// I2C
#define ERR_ORIG_I2C			60							///< Basis-Adresse I2c
#define ERR_I2C_OPEN			(ERR_ORIG_I2C + 1)			///< I2c kann nicht geoeffnet werden
#define ERR_I2C_SETADDR			(ERR_ORIG_I2C + 2)			///< Slave-Adresse kann bei I2c nicht gesetzt werden
#define	ERR_I2C_WRITE			(ERR_ORIG_I2C + 3)			///< auf I2c schreiben nicht moeglich
#define ERR_I2C_READ			(ERR_ORIG_I2C + 4)			///< von I2c lesen nicht moeglich
#define ERR_I2C_CLOSE			(ERR_ORIG_I2C + 5)			///< I2c kann nicht geschlossen werden

//Hw IO's general
#define ERR_ORIG_HW_IO			70							///< Basis-Adresse Hardware-IOs
#define ERR_HW_IO				(ERR_ORIG_HW_IO + 1)		///< HwIo-Thread oder -Objekt kann nicht erstellt werden
#define ERR_HW_IO_UPDATE		(ERR_ORIG_HW_IO + 2)		///< Eingaenge der IoModule via Generalcall nicht moeglich

//Hw IO's Einzel-Modul
#define ERR_ORIG_HW_IO_MODUL	80							///< Basis-Adresse fuer einzelne IoModule
#define ERR_HW_IO_MODUL1		(ERR_ORIG_HW_IO_MODUL + 1)	///< lesen oder schreiben bei 1.IoModul nicht moeglich
#define ERR_HW_IO_MODUL2		(ERR_ORIG_HW_IO_MODUL + 2)	///< lesen oder schreiben bei 2.IoModul nicht moeglich
#define ERR_HW_IO_MODUL3		(ERR_ORIG_HW_IO_MODUL + 3)	///< lesen oder schreiben bei 3.IoModul nicht moeglich
#define ERR_HW_IO_MODUL4		(ERR_ORIG_HW_IO_MODUL + 4)	///< lesen oder schreiben bei 4.IoModul nicht moeglich
#define ERR_HW_IO_MODUL5		(ERR_ORIG_HW_IO_MODUL + 5)	///< lesen oder schreiben bei 5.IoModul nicht moeglich
#define ERR_HW_IO_MODUL6		(ERR_ORIG_HW_IO_MODUL + 6)	///< lesen oder schreiben bei 6.IoModul nicht moeglich
#define ERR_HW_IO_MODUL7		(ERR_ORIG_HW_IO_MODUL + 7)	///< lesen oder schreiben bei 7.IoModul nicht moeglich
#define ERR_HW_IO_MODUL8		(ERR_ORIG_HW_IO_MODUL + 8)	///< lesen oder schreiben bei 8.IoModul nicht moeglich
#define ERR_HW_IO_MODUL9		(ERR_ORIG_HW_IO_MODUL + 9)	///< lesen oder schreiben bei 9.IoModul nicht moeglich

// PLC
#define ERR_ORIG_PLC			90							///< Basis-Adresse SPS
#define	ERR_PLC					(ERR_ORIG_PLC + 1)			///< Plc-Thread oder -Objekt kann nicht erstellt werden
#define ERR_PLC_CONNECT			(ERR_ORIG_PLC + 2)			///< keine Verbindung zur SPS moeglich
#define ERR_PLC_NOTRECV			(ERR_ORIG_PLC + 3)			///< keine Daten von der SPS erhalten
#define ERR_PLC_MUCH			(ERR_ORIG_PLC + 4)			///< zu viele SPS-Bytes von der SPS oder im SisCom-Speicher
#define ERR_PLC_LESS			(ERR_ORIG_PLC + 5)			///< zu wenig SPS-Bytes von der SPS oder im SisCom-Speicher

// WATCHDOG/RTC
#define ERR_ORIG_WATCHRTC		100							///< Basis-Adresse Watchdog and RTC
#define ERR_WATCHDOG			(ERR_ORIG_WATCHRTC + 1)		///< Watchdog kann nicht erstellt werden
#define	ERR_RTC_TIMESET			(ERR_ORIG_WATCHRTC + 2)		///< Master korrigiert die System-Zeit auf Slave

// Logger
#define ERR_ORIG_LOG			110							///< Basis-Adresse Logger
#define ERR_WRITE_LOGF			(ERR_ORIG_LOG + 1)			///< schreiben in Log-File nicht moeglich
#define ERR_CHECK_SPACE			(ERR_ORIG_LOG + 2)			///< Kontrollieren des freien Speicherplatzes nicht m�glich

// Phone
#define ERR_ORIG_TEL			120							///< Basis-Adresse Phone
#define	ERR_TEL					(ERR_ORIG_TEL + 1)			///< Phone-Thread oder -Objekt kann nicht erstellt werden
#define	ERR_TEL_SIZE			(ERR_ORIG_TEL + 2)			///< Groesse der Telefondaten ist falsch
#define	ERR_TEL_OVERFLOW		(ERR_ORIG_TEL + 3)			///< Telphone Buffer voll

// COR (Codec register)
#define ERR_ORIG_COR			130							///< Basis-Adresse Cor
#define	ERR_COR					(ERR_ORIG_COR + 1)			///< Genereller COR Error
#define ERR_COR_READ_ACCESS		(ERR_ORIG_COR + 2)			///< COR-Lese-Zugriff nicht moeglich
#define ERR_COR_WRITE_ACCESS	(ERR_ORIG_COR + 3)			///< COR-Schreib-Zugriff nicht moeglich
#define ERR_COR_ACCESS			(ERR_ORIG_COR + 4)			///< COR kann nicht geoefnet oder geschlossen werden
#define ERR_COR_IDENTIFIER		(ERR_ORIG_COR + 5)			///< Daten-Zugriff im Interface nicht moeglich (Software-Fehler)
#define ERR_DATA_SIZE			(ERR_ORIG_COR + 6)			///< Daten schreib fehler zu viele daten

// Codec
#define ERR_ORIG_CODEC			140							///< Basis-Adresse Codec
#define ERR_CODEC				(ERR_ORIG_CODEC + 1)		///< Codec Thread kann nicht erstellt werden
#define ERR_ENC_CREATE			(ERR_ORIG_CODEC + 2)		///< Der Encoder konnte nicht erstellt wurde
#define ERR_DEC_CREATE			(ERR_ORIG_CODEC + 3)		///< Der Decoder konnte nicht erstellt wurde
#define ERR_DECODING			(ERR_ORIG_CODEC + 4)		///< Error beim Decodieren
#define ERR_ENCODING			(ERR_ORIG_CODEC + 5)		///< Error beim Encodieren
#define ERR_SIP     			(ERR_ORIG_CODEC + 6)		///< SIP Fehler

#endif /* ERRORID_H_ */
