/**
 * \file	GetIP.h
 * \date	05.05.2021
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief This file contains a single function to get the local IP address
 */

std::string getIP();