/** *
 * \file	TelStruct.h
 * \date	13.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief 	Structs for better handling of the telephone data.
 */

#ifndef SRC_COR_TELSTRUCT_H_
#define SRC_COR_TELSTRUCT_H_

#include <string>
#include <vector>

/**
 * \brief	struct for the telephone configuration
 */
struct STelConfig {
	unsigned int Echo;				/**< Telephone Configuration Echo Canceling: 0: off, 1: on */
	unsigned int Source;			/**< Audio Source 0..2: analog 2-wire, 4: analog 4-wire, 5: digital */
	unsigned int Processing;		/**< Audio processing 0: analog, 1: digital remote, 2: digital local */
	unsigned int BytesPerFrame;		/**< Frame Length of coded Telephone Bytes (0�248) */
	unsigned int LocalAdjust;		/**< Adjustment of the local Telephone Rx signal (127�-128 [1/32]) */
	unsigned int RemoteGain;		/**< Rx Gain of SISCOM remote telephone signal (127�-128 [1/8]) */
	unsigned int MinLevel;			/**< minimal Level to detect telephone communication*/
	unsigned int SamplesPerFrame;	/**< Frame Length of Telephone Samples (160�1016) */
};

/**
 * \brief	struct for the DTMF
 */
struct SDtmf {
	unsigned int Det;				/**< DTMF detection 0: No DTMF, 1: DTMF detected */
	unsigned int Num;				/**< DTMF number detected DTMF number */
	bool		 isLocal;			/**< Is the DTMF local or remote. Is used bidirectional (RX & TX), true when local DTMF*/
};

enum ETelCommState {
       eNoPC316Comm = 0,  /**< keine analoge (PC316) Kommunikation */
       ePC316Rx,                  /**< analoge Empfangs-Kommunikation */
       ePC316Tx,                  /**< analoge Sende-Kommunikation */
       ePC316RxTx,               /**< analoge Empfangs- sowie Sende-Kommunikation */
       eNoSipComm,               /**< keine digitale (SIP) Kommunikation */
       eSipAsFz,                  /**< digitale Kommunikation zwischen AS und einem Fz */
       eSipFzFz,                  /**< digitale Kommunikation zwischen den Fz'en */
       eSipAsFzFz                 /**< digitale Kommunikation zwischen AS und beiden Fz'en */
};


/**
 * \brief	struct for the states
 */
struct SState {
	ETelCommState Comm;				/**< State of TEL communication 0: No, 1: TX, 2: RX, 3:TX&RX */
	SDtmf Dtmf;						/**< DTMF with detection and Number */
};

/**
 * \brief	struct for the controls
 */
struct SCtl {
	bool Ready;						/**< 1 ready to read Data, 0 not ready to read Data */
	unsigned int NumData;			/**< Number of Data (Number of Samples/ Number of coded Data) */
	SDtmf Dtmf;						/**< DTMF with detection and Number */
};

/**
 * \brief	Rssi struct
 */
struct STel {
	unsigned int RssiLocal	: 10;	/**< local telephone  RSSI: Bit 9..0 */
	unsigned int RssiRemote	: 10;	/**< remote telephone RSSI: Bit 19..10 */
};

/**
 * \brief	IDs for Requests
 */
enum ECorCtl {
	eReq_None = 0,	/**< No Request */
	eSamplesIn,		/**< read/write Data to SamplesIn */
	eSamplesOut,	/**< read/write Data to SamplesOut */
	eCodeRX,		/**< read/write Data to CodeRx */
	eCodeTX,		/**< read/write Data to CodeTx */
};

/**
 * \brief 	Enum for all possible SisCom locations
 */
enum EStation{
    eMaster = 0,
    eSlave1,
    eSlave2,
    eSlave3,
    eUndefined
};

enum EInterface{
	eAnalog,
	eDigital,
	eWeb
};

enum ECallType{
	eSIP,
	eIP,
	ePC316
};

enum ECallState{
    eInvalid = -1,  //There is something wrong
    eInactive,		//Neutral state
    eCalling,		//Call Someone
    eInCall,		//In a call
    eHangup,		//Hangup the current call
	eAccept,		//Accept the current call
	ePassive		//Indicates that the station was not called but others are in a call
};

typedef struct{
    std::string callee; //The number being called
    std::string caller; //The number calling
    ECallState state;   //If the header is calling, hangup or invalid
    bool isIpCall;      //Used for making the right callback
}SPhoneHeader;


/**
 * \brief 	Enum for all the audio sources
 */
enum ESource{
    eNoSource = 0,
    eSourceDecoder,
    eSourcePC316,
    eSourceSIP
};

/**
 * \brief 	Enum for all the audio sinks
 */
enum ESink{
    eNoSink = 0,
    eSinkEncoder,
    eSinkPC316,
    eSinkSIP
};

#endif /* SRC_COR_TELSTRUCT_H_ */
