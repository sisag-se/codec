/** *
 * \file	ICorTel.h
 * \date	13.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief	header file for virtual methods to access the cor
 */

#ifndef ICorTel_H_
#define ICorTel_H_

#include "TelStruct.h"
#include <vector>

class Result;

/**
 * \class	ICorTel
 * \brief 	ICorTel is the interface for the "Codec Register" (COR).
 */
class ICorTel
{
public:
	/**
	 * \brief Destructor
	 */
	 virtual ~ICorTel() {};

	/**
	 *\brief gets "Samples in" from the COR.
	 * \param[out]	data	vector for the samples
	 * \return result with error messages
	 */
	virtual Result getSamplesIn(std::vector<int>& data) = 0;

	/**
	 *\brief sets "Samples out" to the COR.
	 * \param[in]	data	vector for the samples
	 * \return result with error messages
	 */
	virtual Result setSamplesOut(std::vector<int>& data) = 0;

	/**
	 *\brief get "State" from the COR
	 * \param[in]	CorId	ID to read the state from: SamplesIn/SamplesOut/CodeRx/CodeTx
	 * \param[out]	State	state from the Cor
	 * \return result with error messages
	 */
	virtual Result getState(ECorCtl CorId, SState& State) = 0;

	/**
	 *\brief set "State" to the COR.
	 * \param[in]	CorId	ID to write the state to: SamplesIn/SamplesOut/CodeRx/CodeTx
	 * \param[in]	State	Tel state from the Cor
	 * \return result with error messages
	 */
	virtual Result setState(ECorCtl CorId, SState& State) = 0;

	/**
	 *\brief get "control" from the COR.
	 * \param[in]	CorId	ID to read the control from: SamplesIn/SamplesOut/CodeRx/CodeTx
	 * \param[out]	control	control from the Cor
	 * \return result with error messages
	 */
	virtual Result getCtl(ECorCtl CorId, SCtl& control) = 0;

	/**
	 *\brief set "control" to the COR.
	 * \param[in]	CorId	ID to write the control to: SamplesIn/SamplesOut/CodeRx/CodeTx
	 * \param[in]	control	control from the Cor
	 * \return result with error messages
	 */
	virtual Result setCtl(ECorCtl CorId, SCtl& control) = 0;

	/**
	 *\brief get the configuration.
	 * \param[out]	config	configuration from the Cor
	 * \return result with error messages
	 */
	virtual Result getTelConfig(STelConfig& config) = 0;

	/**
	 *\brief get the Rssi.
	 * \param[out]	Rssi	Rssi struct from the Cor
	 * \return result with error messages
	 */
	virtual Result getTelInRssi(STel& Rssi) = 0;

	/**
	 *\brief set the Rssi.
	 * \param[in]	Rssi	Rssi struct from the Cor
	 * \return result with error messages
	 */
	virtual Result setTelTxRssi(STel& Rssi) = 0;

	/**
	 *\brief gets "RXCode" from the COR.
	 * \param[out]	data	Data form the Cor
	 * \return result with error messages
	 */
	virtual Result getCodeRx(std::vector<int>& data) = 0;

	/**
	 *\brief sets "TXCode" to the COR.
	 * \param[in]	data	Data to write to Cor
	 * \return result with error messages
	 */
	virtual Result setCodeTx(std::vector<int>& data) = 0;

	/**
	 *\brief sets "CodeRxAck" to the COR.
	 * \param[in]	Ack		State to set the Acknowledge Flag
	 * \return result with error messages
	 */
	virtual Result setCodeRxAck(bool Ack) = 0;

	/**
	 *\brief sets "SamplesInAck" to the COR.
	 * \param[in]	Ack		State to set the Acknowledge Flag
	 * \return result with error messages
	 */
	virtual Result setSamplesInAck(bool Ack) = 0;

	/**
	 *\brief sets "Error" to the COR.
	 * \param[in]	ErrorID		set errorID to transmit to the SisCom Task
	 * \return result with error messages
	 */
	virtual Result setError(int ErrorID) = 0;

	/**
	 *\brief Gets the analog telephone rssi with web optimized filtering
	 * \param[out]	STel Struct containing local and remote rssi
	 * \return result with error messages
	 */
	virtual Result getWebRssi(STel&) = 0;

	/**
	 *\brief Gets the section number
	 * \param[out]	the section number
	 * \return result with error messages
	 */
	virtual Result getSection(unsigned int&) = 0;

	/**
	 *\brief Gets the station
	 * \param[out]	the section number
	 * \return result with error messages
	 */
	virtual Result getStation(EStation&) = 0;

	/**
	 *\brief Gets the interface the SisCom is Running on
	 * \param[out]	the Interface
	 * \return result with error messages
	 */
	virtual Result getInterface(EInterface&) = 0;
};

#endif /* ICorTel_H_ */
