/** *
 * \file	CorMapping.h
 * \date	13.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief 	Describes the mapping of the Cor (Codec Register).
 */

#ifndef SRC_COR_CORMAPPING_H_
#define SRC_COR_CORMAPPING_H_

/* Cor Memory Definition */
#define COR_SIZE sizeof(SMem)/sizeof(volatile unsigned int)
#define TEL_DATA_SIZE	1016	/// maximal size of the telData in the Cor
#define CODE_DATA_SIZE	 248	/// maximal size of the coded Data samples in the Cor

/**
 * COR Memory struct
 */
struct SMem {
// Samples In
	struct SConfig{
		volatile unsigned int					:	32;	// Bit 31..0
		volatile unsigned int					:	32;	// Bit 31..0
	} Config;										// memory address =  0..1
	struct STelInState{
		volatile unsigned int Comm				:	 3;	// Bit  2..0
		volatile unsigned int LocalDtmfDet		:	 1;	// Bit  3
		volatile unsigned int LocalDtmfNum		:	 4;	// Bit 7..4
		volatile unsigned int RemoteDtmfDet		:	 1; // Bit 8
		volatile unsigned int RemoteDtmfNum		:	 9; // Bit 17..9
		volatile unsigned int Ack				:	 1;	// Bit 18
		volatile unsigned int					:	13;	// Bit 31..19
	} TelInState;									// memory address =  2
	struct STelInControll{
		volatile unsigned int InRdy				:	 1;	// Bit  0
		volatile unsigned int NumInData			:	10;	// Bit 10..1
		volatile unsigned int					:	21;	// Bit 31..11
	} TelInControll;								// memory address =  3
	volatile unsigned int TelInData[TEL_DATA_SIZE];	// 1016 x (Bit 15..0 Local Sample(0) Bit 31..16 Remote Sample(0))
													// memory address = 4..1019
	struct STelInRssi{
		volatile unsigned int RssiLocal			:	10;	// Bit  9..0
		volatile unsigned int RssiRemote		:	10;	// Bit 19..10
		volatile unsigned int					:	12;	// Bit 31..20
	}TelInRssi;										// memory address = 1020
	struct STelInWebRssi{
		volatile unsigned int RssiLocal			:	10;	// Bit  9..0
		volatile unsigned int RssiRemote		:	10;	// Bit 19..10
		volatile unsigned int					:	12;	// Bit 31..20
	}TelInWebRssi;									// memory address = 1021
	volatile unsigned int						:	32;	// Bit 31..0
	volatile unsigned int						:	32;	// Bit 31..0
													// memory address = 1022..1023
// Samples Out
	volatile unsigned int						:	32;	// Bit 31..0
	volatile unsigned int						:	32;	// Bit 31..0
													// memory address = 1024..1025
	struct STelOutState{
		volatile unsigned int Fifo				:	 3;	// Bit  2..0
		volatile unsigned int					:	29;	// Bit 31..3
	} TelOutState;									// memory address = 1026
	struct STelOutControll{
		volatile unsigned int OutRdy			:	 1;	// Bit  0
		volatile unsigned int NumOutData		:	10;	// Bit 10..1
		volatile unsigned int DtmfSend			:	 2; // Bit 12..11
		volatile unsigned int DtmfNum			:	 5; // Bit 17..13
		volatile unsigned int					:	14;	// Bit 31..18
	} TelOutControll;								// memory address = 1027
	volatile unsigned int TelOutData[TEL_DATA_SIZE];	// 1016 x (Bit 15..0 Local Sample(0) Bit 31..16 Remote Sample(0))
													// memory address = 1028..2043
	volatile unsigned int						:	32;	// Bit 31..0
	volatile unsigned int						:	32;	// Bit 31..0
	volatile unsigned int						:	32;	// Bit 31..0
	volatile unsigned int						:	32;	// Bit 31..0
													// memory address = 2044..2047
// Code RX
	struct STelConfig{
		volatile unsigned int Echo				:	 2;	// Bit  1..0
		volatile unsigned int Source			:	 3;	// Bit  4..2
		volatile unsigned int Processing		:	 2;	// Bit  6..5
		volatile unsigned int BytesPerFrame		:	 8;	// Bit 14..7
		volatile unsigned int LocalAdjust		:	 8;	// Bit 22..15
		volatile unsigned int RemoteGain		:	 8;	// Bit 30..23
		volatile unsigned int					:	 1;	// Bit 31
		volatile unsigned int MinLevel			:	10;	// Bit  9..0
		volatile unsigned int SamplesPerFrame	:	10;	// Bit 19..10
		volatile unsigned int Section			:	 4; // Bit 23..20
		volatile unsigned int Station			:	 2; // Bit 25..24
		volatile unsigned int					:	 6;	// Bit 31..26
	} TelConfig;									// memory address = 2048..2049
	struct SCodRxState{
		volatile unsigned Ack					:	 1;	// Bit  0
		volatile unsigned int 					:	31;	// Bit 31..0
	} CodRxState;									// memory address = 2050
	struct SCodRxCtl{
		volatile unsigned int InRdy				:	 1;	// Bit  0
		volatile unsigned int NumInData			:	 8;	// Bit  8..1
		volatile unsigned int DtmfValid			:	 1;	// Bit  9
		volatile unsigned int DtmfNum			:	13;	// Bit 22..10
		volatile unsigned int Mode				:	 2;	// Bit 24..23
		volatile unsigned int TestToneValid		:	 2;	// Bit 26..25
		volatile unsigned int TestToneNum		:	 5; // Bit 31..27
	} CodRxCtl;										// memory address = 2051
	volatile unsigned int CodecRxData[CODE_DATA_SIZE];// 248 x (Bit 15..0)
													// memory address = 2052..2299
	struct SCodRxRssi{
		volatile unsigned int RssiRemote		:	10;	// Bit  9..0
		volatile unsigned int					:	22;	// Bit 31..10
	}CodRxRssi;										// memory address = 2300
	volatile unsigned int						:	32;	// Bit 31..0
	volatile unsigned int						:	32;	// Bit 31..0
	volatile unsigned int						:	32;	// Bit 31..0
													// memory address = 2301..2303
// Code TX
	struct STelConfInterface{
		volatile unsigned int Interface			:	3;	// Bit  2..0
		volatile unsigned int					:	29;	// Bit 31..3
	}TelConfInterface;								// memory address = 2304
	volatile unsigned int						:	32;	// Bit 31..0
													// memory address = 2305
	struct SCodTxState{
		volatile unsigned int Comm				:	 3;	// Bit  2..0
		volatile unsigned int DtmfValid			:	 1;	// Bit  3
		volatile unsigned int DtmfNum			:	14;	// Bit 17..4
		volatile unsigned int CodecError		:	 8;	// Bit 25..18
		volatile unsigned int					:	 6;	// Bit 31..26
	} CodTxState;									// memory address = 2306
	struct SCodTxCtl{
		volatile unsigned int OutRdy			:	 1;	// Bit  0
		volatile unsigned int NumOutData		:	 8;	// Bit  8..1
		volatile unsigned int					:	23;	// Bit 31..9
	} CodTxCtl;										// memory address = 2307
	volatile unsigned int CodecTxData[CODE_DATA_SIZE];// 248 x (Bit 15..0)
													// memory address = 2308..2555
	struct SCodTxRssi{
		volatile unsigned int RssiLocal			:	10;	// Bit  9..0
		volatile unsigned int					:	22;	// Bit 31..10
		volatile unsigned int WebRssiLocal		:	10; // Bit  9..0
		volatile unsigned int WebRssiRemote		:	10; // Bit 19..10
		volatile unsigned int 					:	12; // Bit 31..20
	}CodTxRssi;										// memory address = 2556..2557
	struct SCodWebDtmf{
		volatile unsigned int LocalValid		:	 1;	// Bit  0
		volatile unsigned int LocalNum			:	 4;	// Bit  4..1
		volatile unsigned int RemoteValid		:	 1; // Bit  5
		volatile unsigned int RemoteNum			:	 4; // Bit  9..6
		volatile unsigned int 					:	22;	// Bit 31..10
	}CodWebDtmf;									// memory address = 2558
	volatile unsigned int						:	32;	// Bit 31..0
													// memory address = 2559
};

/**
 * COR Memory Union access
 */
union Cor {
	SMem			name;
	unsigned int	addr[COR_SIZE];
};

/**
 * \brief COR Interrupt struct
 */
struct SInterrupt {
	unsigned int InRdy;
	unsigned int RxRdy;
};

#endif /* SRC_COR_CORMAPPING_H_ */
