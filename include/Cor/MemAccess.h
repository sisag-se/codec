/** *
 * \file	MemAccess.h
 * \date	11.4.2016
 * \author	bem, mad
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	2.0
 *
 * \brief Header file for the MemAccess class
 */

#ifndef MemAccess_H_
#define MemAccess_H_

#include <stdio.h>
#include <pthread.h>
#include <vector>
#include "Result.h"

class Result;

#define PAGE_SIZE 0x4000

/**
 * \class	MemAccess
 * \brief	MemAccess enables the access to "Memory Mapped Linux Devices", witch have to be available:
 *			for example as a Device "/dev/uio0".
 *			The Memory is Structured as "Page-units", witch in Linux are corresponding to units the size of 4096 Bytes
 *			(PAGE_SIZE = 0x1000). MemAccess enables only the access to one  "Page-Unit".
 *			On opening the Memory-access the Base Address (baseAddr) defines the Start Address of a Page Unit.
 */
class MemAccess {
public:

	/**
	* \brief Constructor
	*   \param[in] device  Name of the Linux Devices (z.B: "/dev/uio0")
	*/
	MemAccess(const char* device);

	/**
	* \brief Destructor
	*/
	virtual ~MemAccess();

	/**
	* \brief opens a Linux Device and Provides one Memory Page the size of PAGE_SIZE =
	*			0x1000 starting from the Base Address "baseAddr" including Read-/Write-access.
	* \param[in]	baseAddr	Base-Address on witch you access. It must be 0,
	*							or a multiple of the PAGE_SIZE=0x1000!
	* \return	Result		id: 0: no Error
	*							1: Error while opening the Device
	*							2: Error while opening the Memory-Page
	*/
	Result openMem(const off_t baseAddr);

	/**
	* \brief	writes a Value to the Memory-Address (baseAddr + offestAddr). The Offset-Address "offsetAddr"
	* 			must not be greater than one Page-unit it must be smaller than PAGE_SIZE.
	* \param[in]	offsetAddr	offset from the base address of the page (Address = baseAddr + offsetAddr).
	* \param[in]	value		Value to write at the address
	* \return		Result	id: 0: No Error
	*							1: Offset-Address is bigger than PAGE_SIZE-1
	*/
	Result writeMem(unsigned int offsetAddr, int value);

	/**
	* \brief	Writes values continuously from the Start-Address (baseAddr+offsetStartAddr) to the End-Address
	*			(baseAddr+offestStartAddr+vector.length-1). The End-Address can not be greater than one Page-unit.
	*			The End Address can not be bigger than PAGE_SIZE!
	* \param[in]	offsetStartAddr	Offset-Address to where the 1. Vector-Value. the other Vector-values are then written to the following addresses.
	* \param[out]	values			Vector containing the Values written to the Memory
	* \return		Result	id: 0: No Error
	*							1: offsetStartAddr + values.size() is bigger than PAGE_SIZE-1
	*/
	Result writeMem(unsigned int offsetStartAddr, std::vector<int> &values);

	/**
	* \brief	Reads the value from the Memory-Address (baseAddr + offestAddr). The Offset-Address "offsetAddr"
	*			must be Smaller than the Size of one Page PAGE_SIZE!
	* \param[in]	offsetAddr	Address to read the Value from (Address = baseAddr + offsetAddr)
	* \param[in]	value		Value read from the Memory
	* \return		Result	id: 0: No Error
	*							1: Offset-Address is bigger than PAGE_SIZE-1
	*/
	Result readMem(unsigned int offsetAddr, int value);

	/**
	* \brief	Reads values from the Start-Address (baseAddr+offsetStartAddr) to the End-Address
	*			(baseAddr+offestStartAddr+vector.length-1). The End-Address should not be larger than the PAGE_SIZE!
	* \param[in]	offsetStartAddr	Offset-Address to read the first value to the Vector. the other Vector-values are then read from the following addresses.
	* \param[out]	values			Values read from the Memory (Number of Values to read are according values.size())
	* \return		Result	id: 0: No Error
	*							1: Offset-Address + values.size() bigger than PAGE_SIZE-1
	*/
	Result readMem(unsigned int offsetStartAddr, std::vector<int> &values);

	/**
	* \brief closes the Linux Device
	* \return	Result	id: 0: No Error
	*						1: Error at closing the Memory-Page
	*/
	Result closeMem();

protected:
	unsigned int* m_PBaseMap;	/**<virtual base Address of the memory mapped access */

private:
	int				m_Fd;		/**<Filehandler*/
	const char*		m_PDevice;	/**<Linux Device Name*/
	const off_t		m_BaseAddr;	/**<physical access-address Pointing to the Basis-(Start-) Address of the Memory-Page*/
	pthread_mutex_t	m_mutex;	/**<prevents to race conditions */
};

#endif /* MemAccess_H_ */
