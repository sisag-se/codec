/** *
 * \file	CorAccess.h
 * \date	13.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief 	Header File for the CorAccess class
 */

#ifndef SRC_COR_CORACCESS_H_
#define SRC_COR_CORACCESS_H_

#include <queue>
#include "Result.h"
#include "CorMapping.h"
#include "MemAccess.h"

#define COR_DEVICE "/dev/uio2"		///< File Path of the Linux user space memory mapped device "COR"

/**
 * \class	CorAccess
 * \brief	CorAccess manages the access to the "Codec Register" (COR),
 *			CorAccess coordinates the memory mapping and the call of
 *			according Call-Back-Functions, if new COR-Data are available.
 */
class CorAccess : public MemAccess
{
public:
	/**
	 * \brief Constructor
	 */
	CorAccess();

	/**
	 * \brief Destructor
	 */
	virtual ~CorAccess();

	/**
	 * \brief returns a reference to the data in the COR memory
	 * \param[out]	res 	result of the function with error messages
	 * \return reference to the COR-Data
	 */
	Cor* getCorReadAccess(Result& res);

	/**
	 * \brief return a reference to the writable COR memory.
	 * \param[out]	res 	result of the Function with error messages
	 * \return reference to the writable memory
	 */
	Cor* getCorWriteAccess(Result& res);

	/**
	 * \brief closes the writing access to the COR memory.
	 * \return Result of the Function with error messages
	 */
	Result quitCorWriteAccess();

	/**
	 * \brief Updates the data in the internal COR Image.
	 * \return Result of the Function with error messages
	 */
	Result updateCorImage();

	/**
	 * \brief Check Interrupts and calls the evaluateInterrupts function
	 * \return Result of the Function with error messages
	 */
	Result checkInterrupts();

	/**
	 * \brief returns the the evaluated Interrupts
	 * \return SInterrupt Struct containing the front of the interrupt queue.
	 */
	SInterrupt getInterrupts();

	/**
	 * \brief deletes the the executed interrupts from the interrupt queue
	 * \param[in]	executedInterrupt	last executed Interrupts.
	 */
	void deleteOldestInterrupt(SInterrupt executedInterrupt);

	/**
	 * \brief deletes the whole interrupt queue
	 */
	void deleteAllInterrupts();

private:
	/**
	 * \brief Evaluated the interrupt struct of rising edges
	 * \param[out]	detectedInterrupt	Detected and evaluated interrupts
	 * \return returns the number of detected interrupts
	 */
	int evaluateInterrupts(SInterrupt& detectedInterrupt);

	Cor m_CorData; 									/**< Memory Data image, witch will be read cyclic */
	SInterrupt m_currentInterrupt;					/**< the interrupt flags read in this cycle */
	SInterrupt m_lastInterrupt;						/**< the interrupt flags from the previous cycle */
	std::queue<SInterrupt> m_detectedInterrupts;	/**< interrupt queue */
};
#endif /* SRC_COR_CORACCESS_H_ */
