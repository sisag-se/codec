/** *
 * \file	ICorDtmf.h
 * \date	08.02.2021
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief	header file for virtual methods to access DTMF's
 */

#pragma once

#include "TelStruct.h"
#include <vector>

class Result;

/**
 * \class	ICorDtmf
 * \brief 	ICorDtmf is used for sendig and Receiving DTMF's
 */
class ICorDtmf{
public:
	/**
	 * \brief Destructor
	 */
	 virtual ~ICorDtmf() {};

	/**
	 *\brief Send a DTMF
	 * \param[in]	dtmf the DTMF to send
	 * \return result with error messages
	 */
	virtual void sendDtmf(SDtmf) = 0;

	/**
	 *\brief Send multiple DTMF's
	 * \param[in]	dtmf list of DTMF's to send
	 * \return result with error messages
	 */
	virtual void sendDtmf(std::vector<SDtmf>) = 0;

	/**
	 *\brief Forward any remote DTMF to Local DTMF and vice versa
	 * \param[in]	The interface used, determines how the DTMF's are copied
	 * \return result with error messages
	 */
	virtual Result copyDtmf(EInterface) = 0;

	/**
	 *\brief Check wheter a local DTMF is present and fill its number into the reference
	 * \param[out] dtmf reference (det is false when no DTMF is present)
	 * \return result with error messages
	 */
	virtual Result receiveLocalDtmf(SDtmf&) = 0;

	/**
	 *\brief Check wheter a remote DTMF is present and fill its number into the reference
	 * \param[out] dtmf reference (det is false when no DTMF is present)
	 * \return result with error messages
	 */
	virtual Result receiveRemoteDtmf(SDtmf&) = 0;

private:
	/**
	 *\brief Check wheter a local DTMF is present
	 * \param[out] true when a local DTMF is present
	 * \return result with error messages
	 */
	virtual Result hasLocalDtmf(bool&) = 0;

	/**
	 *\brief Check wheter a remote DTMF is present
	 * \param[out] true when a remote DTMF is present
	 * \return result with error messages
	 */
	virtual Result hasRemoteDtmf(bool&) = 0;

	/**
	 * \brief Method for actually sending out a DTMF. Gets the latest in the queue and sends it
	 * \return result with error messages
	 * \note Stops the timer automatically when there are no more elements in the queue
	 */
	virtual void sendDtmfLiterally() = 0;

};