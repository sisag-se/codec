/** *
 * \file	DtmfMap.h
 * \date	08.02.2021
 * \author	mak
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief header file of the dtmfMap class
 */

#pragma once

#include <vector>
#include <queue>
#include <functional>
#include "ICorDtmf.h"
#include "TelStruct.h"
#include "Timer.h"

#define DTMF_SEND_TIME 200 //A DTMF is sent for XXXms

class CorAccess;

/*
Special cases of DTMFS
DTMF 10: "0"
DTMF 11: "*"
DTMF 12: "#"
*/


/**
 * \class	DtmfMap
 * \brief	DtmfMap implements Methods from ICorDtmf to access the DTMF Data from the Cor with
 *			the DtmfInterface.
 */
class DtmfMap : public ICorDtmf{
public:
	 /**
	 * \brief Constructor
	 * \param[in]	pCor	Interface to the Cor
	 */
	DtmfMap(CorAccess* pCor);

	/**
	* \brief Destructor
	*/
	virtual ~DtmfMap();

	/**
	 *\brief Adds a DTMF to a queue which caches all DTMF to be sent
	 * \param[in]	the DTMF to send
	 * \return result with error messages
	 */
	virtual void sendDtmf(SDtmf);

	/**
	 *\brief Adds a DTMF to a queue which caches all DTMF to be sent
	 * \param[in]	list of DTMF's to send
	 * \return result with error messages
	 */
	virtual void sendDtmf(std::vector<SDtmf>);

	/**
	 *\brief Forward any remote DTMF to Local DTMF and vice versa
	 * \param[in]	The interface used, determines how the DTMF's are copied
	 * \return result with error messages
	 */
	virtual Result copyDtmf(EInterface);

	/**
	 *\brief Check wheter a local DTMF is present and fill its number into the reference
	 * \param[out] dtmf reference (det is false when no DTMF is present)
	 * \return result with error messages
	 */
	virtual Result receiveLocalDtmf(SDtmf&);

	/**
	 *\brief Check wheter a remote DTMF is present and fill its number into the reference
	 * \param[out] dtmf reference (det is false when no DTMF is present)
	 * \return result with error messages
	 */
	virtual Result receiveRemoteDtmf(SDtmf&);

private:
	/**
	 *\brief Check wheter a local DTMF is present
	 * \param[out] true when a local DTMF is present
	 * \return result with error messages
	 */
	virtual Result hasLocalDtmf(bool&);

	/**
	 *\brief Check wheter a remote DTMF is present
	 * \param[out] true when a remote DTMF is present
	 * \return result with error messages
	 */
	virtual Result hasRemoteDtmf(bool&);

	/**
	 * \brief Method for actually sending out a DTMF. Gets the latest in the queue and sends it
	 * \return result with error messages
	 * \note Stops the timer automatically when there are no more elements in the queue
	 */
	virtual void sendDtmfLiterally();

	CorAccess* m_pCor;	/**< Pointer to the CorAccess */

	//Queueueueueueueue and timer for sending DTMF's 
	std::queue<SDtmf> dtmfQueue;
	Timer <std::function<void()>>dtmfTimer;
};
