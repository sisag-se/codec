/** *
 * \file	TelMap.h
 * \date	13.03.2019
 * \author	hen
 * \author	SISAG, Postfach, 6460 Altdorf
 * \version	1.0
 *
 * \brief herder file of the telMap class
 */

#ifndef SRC_COR_TELMAP_H_
#define SRC_COR_TELMAP_H_

#include <vector>
#include "ICorTel.h"
#include "TelStruct.h"

class CorAccess;

/**
 * \class	TelMap
 * \brief	TelMap implements Methods from ICorTel to access the Tel Data from the Cor with
 *			the TelInterface.
 */
class TelMap : public ICorTel {
public:
	 /**
	 * \brief Constructor
	 * \param[in]	pCor	Interface to the Cor
	 */
	TelMap(CorAccess* pCor);

	/**
	* \brief Destructor
	*/
	virtual ~TelMap();

	/**
	 * \brief get "Samples in" from the COR.
	 * \param[out]	data	vector for the Samples
	 * \return result with error messages
	 */
	virtual Result getSamplesIn(std::vector<int>& data);

	/**
	 * \brief set "Samples out" to the COR.
	 * \param[in]	data	vector for the Samples
	 * \return result with error messages
	 */
	virtual Result setSamplesOut(std::vector<int>& data);

	/**
	 * \brief get "State" from the COR
	 * \note The  SIP comm state is only valid on the master, slaves will always yield noSipComm
	 * \param[in]	CorId	ID to read the State from: SamplesIn/SamplesOut/CodeRx/CodeTx
	 * \param[out]	State	Tel State
	 * \return result with error messages
	 */
	virtual Result getState(ECorCtl CorId, SState& State);

	/**
	 * \brief set "State" to the COR.
	 * \param[in]	CorId	ID to write the State to: SamplesIn/SamplesOut/CodeRx/CodeTx
	 * \param[in]	State	Tel State
	 * \return result with error messages
	 */
	virtual Result setState(ECorCtl CorId, SState& State);

	/**
	 * \brief get "control" from the COR.
	 * \param[in]	CorId	ID to read the control from: SamplesIn/SamplesOut/CodeRx/CodeTx
	 * \param[out]	control	control
	 * \return result with error messages
	 */
	virtual Result getCtl(ECorCtl CorId, SCtl& control);

	/**
	 * \brief set "control" to the COR.
	 * \param[in]	CorId	ID to write the control to: SamplesIn/SamplesOut/CodeRx/CodeTx
	 * \param[in]	control	control
	 * \return result with error messages
	 */
	virtual Result setCtl(ECorCtl CorId, SCtl& control);

	/**
	 * \brief get the configuration.
	 * \param[out]	config	configuration
	 * \return result with error messages
	 */
	virtual Result getTelConfig(STelConfig& config);

	/**
	 * \brief get the Rssi.
	 * \param[out]	Rssi	Rssi
	 * \return result with error messages
	 */
	virtual Result getTelInRssi(STel& Rssi);

	/**
	 * \brief set the Rssi.
	 * \param[in]	Rssi	Rssi
	 * \return result with error messages
	 */
	virtual Result setTelTxRssi(STel& Rssi);

	/**
	 * \brief get "RXCode" from the COR.
	 * \param[out]	data	vector for the Code
	 * \return result with error messages
	 */
	virtual Result getCodeRx(std::vector<int>& data);

	/**
	 * \brief set "TXCode" to the COR.
	 * \param[in]	data	vector for the Code
	 * \return result with error messages
	 */
	virtual Result setCodeTx(std::vector<int>& data);

	/**
	 * \brief sets "CodeRxAck" to the COR.
	 * \param[in]	Ack		State to set the Acknowledge Flag
	 * \return result with error messages
	 */
	virtual Result setCodeRxAck(bool Ack);

	/**
	 * \brief sets "SamplesInAck" to the COR.
	 * \param[in]	Ack		State to set the Acknowledge Flag
	 * \return result with error messages
	 */
	virtual Result setSamplesInAck(bool Ack);

	/**
	 * \brief sets "Error" to the COR.
	 * \param[in]	ErrorID		set errorID to transmit to the SisCom Task
	 * \return result with error messages
	 */
	virtual Result setError(int ErrorID);

	/**
	 * \brief Gets the analog telephone rssi with web optimized filtering
	 * \param[out]	STel Struct containing local and remote rssi
	 * \return result with error messages
	 */
	virtual Result getWebRssi(STel&);

	/**
	 * \brief Sets the analog telephone rssi with web optimized filtering
	 * \param[out]	STel Struct containing local and remote rssi
	 * \return result with error messages
	 */
	virtual Result setWebRssi(STel&);

	/**
	 * \brief Gets the section number
	 * \param[out]	the section number
	 * \return result with error messages
	 */
	virtual Result getSection(unsigned int&);

	/**
	 * \brief Gets the station
	 * \param[out]	the section number
	 * \return result with error messages
	 */
	virtual Result getStation(EStation&);

	/**
	 * \brief Gets the interface the SisCom is Running on
	 * \param[out]	the Interface
	 * \return result with error messages
	 */
	virtual Result getInterface(EInterface&);

	/**
	 * \brief Sets the interface the SisCom is Running on
	 * \param[out]	the Interface
	 * \return result with error messages
	 */
	virtual Result setInterface(EInterface&);

private:

	CorAccess* m_pCor;	/**< Pointer to the CorAccess */

};
#endif /* SRC_COR_TELMAP_H_ */
